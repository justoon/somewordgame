﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class GameMap  {

	private static GameMap instance;
	private List<LevelRecord> gameLevels;

	private int index;

	public string currentLevel;

	public static GameMap GetInstance()
	{
		if(instance == null)
		{
			instance = new GameMap();
		}
		return instance;

	}

	private GameMap()
	{
		InitMap();
	}

	private void InitMap()
	{


		gameLevels = new List<LevelRecord>();
		//full game map
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,1,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(1,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(2,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(3,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,1,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(4,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,2,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(5,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(6,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,2,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(7,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(8,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(9,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,3,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,3,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(10,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(11,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(12,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,4,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(13,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(14,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,4,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(15,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,5,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(16,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(17,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(18,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,6,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(19,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,5,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(20,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(21,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,7,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(22,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(23,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(24,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,8,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,6,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.BOSS,25,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(26,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(27,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,9,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(28,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(29,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,7,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(30,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,10,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(31,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(32,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(33,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,11,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(34,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,8,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(35,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(36,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,12,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(37,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(38,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(39,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,13,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,9,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.BOSS,40,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(41,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(42,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,14,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(43,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(44,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,10,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(45,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,15,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(46,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(47,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(48,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,16,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(49,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,11,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.BOSS,50,0,0,gameLevels.Count));
		gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,12,0,0,gameLevels.Count));

	}

	public string GetLevelName(LevelRecord level)
	{
		StringBuilder b = new StringBuilder();
		switch(level.type)
		{
			case LevelRecord.LevelType.STORY: b.Append("Story").Append(level.level); break;
			case LevelRecord.LevelType.STANDARD: b.Append("Level").Append(level.level); break;
			case LevelRecord.LevelType.UPGRADE: b.Append("Upgrade").Append(level.level); break;
			case LevelRecord.LevelType.BOSS: b.Append("Boss").Append(level.level); break;
			default: break;
		}
		return b.ToString();
	}

	public int GetMaxLevel()
	{
		return gameLevels.Count-1;
	}

	public void SetIndex(int index)
	{
		if(index >= gameLevels.Count)
			index = gameLevels.Count - 1;
		this.index = index;
	}

	public LevelRecord GetLevel(int index)
	{
		if(index >= gameLevels.Count)
			index = gameLevels.Count - 1;
		return gameLevels[index];
	}

	//right now this is using scene names, we'll move to config header to load all relevant GameLevel settings
	public LevelRecord GetNextLevel()
	{

		return gameLevels[this.index++];

	}

	public int GetCurrentIndex()
	{
		return index;
	}

	public LevelRecord GetThisLevel()
	{
		if(index == 0)
			return new LevelRecord(-1,0,0,0);

		return gameLevels[index-1];
	}


	public bool GameComplete()
	{
		return (index >= gameLevels.Count-1);
	}

	public string GetSceneName(int index)
	{
		LevelRecord lr = GetLevel(index);
		return GetLevelName(lr);
	}

	//given a level's position in the gameMap (absolute index), return the scene index, Eg. index = 5 = Level4
	public int GetSceneIndex(int levelIndex)
	{
		LevelRecord lr = GetLevel(levelIndex);
		return lr.level;
	}


	public List<LevelRecord> GameLevels {
		get {
			return gameLevels;
		}
	}
}
