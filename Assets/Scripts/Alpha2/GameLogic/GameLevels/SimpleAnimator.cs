﻿using UnityEngine;
using System.Collections;

public class SimpleAnimator : MonoBehaviour {

	
	public Transform target;
	public Vector3 start;
	public Vector3 activePosition;
	public Vector3 finish;

	public float speed;

	//private Vector3 source;
	protected Vector3 destination;
	protected Vector3 currentPosition;
	protected bool animating;

	void Awake()
	{
		if(currentPosition == Vector3.zero)
			currentPosition = start;

		enabled = false;
	}

	void Update()
	{
		if(animating)
		{
			//when the game is paused allow GUI elements to still animate
			if(Time.timeScale == 0)
				target.transform.localPosition = Vector3.MoveTowards(currentPosition, destination, speed*0.1f);
			else
				target.transform.localPosition = Vector3.MoveTowards(currentPosition, destination, speed*Time.deltaTime);

			currentPosition = target.transform.localPosition;

			if(currentPosition == destination)
			{
				//currentPosition = destination;
				target.transform.localPosition = destination;

				animating = false;
				enabled = false;
				
			}
		}
	}

	public void MoveToActive()
	{
		destination = activePosition;
		animating = true;
		enabled = true;
	}

	public void MoveToEnd()
	{
		destination = finish;
		animating = true;
		enabled = true;
	}

	public virtual void Reset()
	{
		target.transform.localPosition = start;
		currentPosition = start;
		destination = activePosition;
	}

	public void SetToActive()
	{
		target.transform.localPosition = activePosition;
		currentPosition = activePosition;
	}
}
