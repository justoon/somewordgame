﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct WordSetting  {

	public int timeToGuess;
	public int wordLength;
	public WordScrambler.ScrambleDifficulty difficulty;

	public WordSetting(int wordLength, WordScrambler.ScrambleDifficulty difficulty, int timeToGuess)
	{
		this.wordLength = wordLength;
		this.difficulty = difficulty;
		this.timeToGuess = timeToGuess;
		if(this.timeToGuess == 0)
			this.timeToGuess = 10;
	}
}
