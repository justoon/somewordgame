using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameLevel : MonoBehaviour {
	
	public enum LengthVariable
	{
		STANDARD,
		ASCENDING,
		DESCENDING,
		RANDOM
	}

	public static int globalSpriteOrder = 1;

	public int level;
	//Alpha 2
	public int wordLength = 9;
	public LengthVariable wordSequenceType;

	public int variableLengthLimit = 9;

	
	
	public WordScrambler.ScrambleDifficulty scrambleDifficulty;
	//public WordScrambler.ScrambleZone scrambleZone;

	public float noiseLow = .9f;
	public float noiseHigh = 1.1f;
	public float thinkAdjustLow = 1f;
	public float thinkAdjustHigh = 1f;



	
	protected bool levelActive;
	
	protected GUITimer timer;
	
	protected SpriteRenderer failOverlayTitle;
	protected SpriteRenderer successOverlayTitle;

	
	protected bool gameOverTriggered;

	protected float startTime;
	protected float timeTaken;

	protected virtual void Awake()
	{


		
		


	}

	
	protected virtual GameLevel GetController()
	{
		return this;
	}



	void Start()
	{
		//switch off updates until we need them
		enabled = false;
	}

	public virtual void StartLevel () 
	{
	

		Score.score = 0;
		GUIManager.GetInstance().UpdateScore(0);
		//setup the weapon pool


		//timer = GameObject.Find("GUITimer").GetComponent<GUITimer>();
		GameObject.Find ("LevelNumber").GetComponent<TextMesh>().text = level.ToString();
	
		startTime = Time.time;

		//register for game events from this point on
		GameEventManager.OnEvent += ExecuteGameEvent;
		GUIEventManager.OnClicked += ExecuteGUIEvent;



		InitLevel();

		

		
	}
	
	//unload scene, unsubscribe to events
	protected virtual void OnDestroy()
	{
		GameEventManager.OnEvent -= ExecuteGameEvent;
		GUIEventManager.OnClicked -= ExecuteGUIEvent;
		
		WordManager.GetInstance().Reset();

	}

	//start the visual timers
	protected virtual void StartTimers ()
	{

		//comboTimer.StartTimer ();
		//timer.StartTimer ();
	}


	//called by GameController
	public virtual void ActivateLevel()
	{
		//GUIManager.GetInstance().InitLevel();
		//get a new word, the Word Manager will dispatch the GameWord event to all listeners

		WordManager wordManager  = WordManager.GetInstance();
		wordManager.GetNewWord();

		//if we have an even word, shift the letter tiles to center them
//		if(wordManager.GetCurrentWord().Length % 2 == 0)
//			GUIManager.GetInstance().GetLetterGrid().ShiftLetters();

		levelActive = true;

		//spawnController = GetComponent<SpawnController>();
		StartTimers ();

		
	}


	public virtual void InitLevel()
	{

		
		//set the word manager to use the specific rules for getting a new word
		//WordManager.GetInstance().InitSwitchOptions(switchOnGuess,switchOnLetters,maxGuesses);
		WordManager wordManager = WordManager.GetInstance();
		wordManager.Difficulty = PlayerProfile.GetInstance().GetWordDifficulty(); //PlayerProfile.GetInstance().GetWordDifficulty();;

		//limit or adjust the scramble difficulty based on the game difficulty limits
		short adjustedDifficulty = (short)(this.scrambleDifficulty + GameController.scrambleAdjustment);
		if(adjustedDifficulty > 3)
			adjustedDifficulty = (short)WordScrambler.ScrambleDifficulty.ALPHA;
		else if(adjustedDifficulty <0)
			adjustedDifficulty = (short)WordScrambler.ScrambleDifficulty.EASY;
		wordManager.ScrambleDifficulty = (WordScrambler.ScrambleDifficulty) adjustedDifficulty;




		wordManager.WordLength = wordLength;
		wordManager.WordSequenceType = wordSequenceType;
		wordManager.VariableLengthLimit = variableLengthLimit;



		

		noiseLow = GameController.noiseLow;
		noiseHigh = GameController.noiseHigh;
		thinkAdjustLow = GameController.thinkAdjustLow;
		thinkAdjustHigh = GameController.thinkAdjustHigh;


		//prevent multiple game over events from being processed, can happen with simultaneous moldies hitting the barrier and doing damage
		gameOverTriggered = false;



		//let the event listeners know the level has started	
		GameEvent gameEvent = new GameEvent(MoldicideEvent.LEVEL_START,0,level,this);
		GameEventManager.DispatchEvent(gameEvent);
		
		
		
		
	}
	
	
	public void SetActive(bool levelActive)
	{
		this.levelActive = levelActive;
	}
	
	public bool IsActive()
	{
		return levelActive;
	}
	

    public virtual void ExecuteGameEvent(GameEvent gameEvent)
	{

		MoldicideEvent eventType = gameEvent.GetName();

		 if(eventType == MoldicideEvent.GAME_OVER && !gameOverTriggered)
		{
			timeTaken = Time.time - startTime;
			GUIManager.GetInstance().ResetLetterGrid();
			GUIManager.GetInstance().LockGrid();
			gameOverTriggered = true;
			
		}
		
		
	}


	public virtual int GetWordsRemaining()
	{
		return -1;
	}



	protected virtual IEnumerator GameOver()
	{

		
		GUIManager.GetInstance().LevelComplete(2);
		GameController.GetInstance().SetDone();
		GUIManager.GetInstance().HideLetterGrid();
		GUIManager.GetInstance().LockGrid();
		ClearGameElements ();
		yield return new WaitForSeconds(1f);
		GUIManager.GetInstance().FadeOut();
		yield return new WaitForSeconds(1f);
		//allow subclasses to handle special gui cleanup
		ResetGUI();

		//GameObject.FindGameObjectWithTag("TargetingComputer").GetComponent<TargetingComputer>().ResetVisibility();
		//stop timers and spawncontrollers

		

		ResetTimers ();


		//WordManager.GetInstance().ClearUsed();
		WordManager.GetInstance().Reset();
		//if(level > -1)
		//	failOverlayTitle.sprite = SpriteCache.GetInstance().LevelFailTitles[level-1];
		//move camera to game over 

		GameObject.FindGameObjectWithTag("GameController").transform.position = GameConstants.OVERLAY_FAIL_POSITION;
		Camera.main.transform.position = GameConstants.CAMERA_FAIL_POSITION;

		Animator anim = GameObject.Find("LevFail_game").GetComponent<Animator>();
		anim.SetTrigger(GUIManager.GetInstance()._triggerFail);

	}
	
	protected void ResetTimers ()
	{
		//timer.StopTimer ();
		//comboTimer.StopAll ();
		//comboTimer.RestartTimer ();
	}
	
	
	protected virtual void ExecuteGUIEvent(GameEvent gameEvent)
	{
		
	}
	
	
	
	protected virtual void ClearGameElements ()
	{
		GUIManager.GetInstance ().HideGUIText ();
		GameObject foamBomb = GameObject.FindGameObjectWithTag("WeaponUpgrade");
		Destroy(foamBomb);
		
	}
	
	protected virtual void LevelComplete()
	{
		//ClearGameElements ();
//		if(Debug.isDebugBuild)
//		{
			GameController.GetInstance().SetDone();
			//DiagnosticManager.GetInstance().Level = level;

//			List<BaseEnemy> active = new List<BaseEnemy>();
//
//			GameObject[] moldies = GameObject.FindGameObjectsWithTag("Moldie");
//			GameObject moldieObj;
//			for(int i=0;i < moldies.Length;i++)
//			{
//				moldieObj = moldies[i];
//				//Debug.Log(moldieObj.name);
//				BaseEnemy moldie = moldieObj.GetComponent<BaseEnemy>();
//				if(moldie != null && moldie.currentState != EnemyState.PAUSED)
//					active.Add(moldie);
//			}
//			
//			GameEvent diagnosticEvent = new GameEvent(MoldicideEvent.D_MOLDIE_POPULATION, 1, 0, active);
//			GameEventManager.DispatchEvent(diagnosticEvent);
//
//			diagnosticEvent = new GameEvent(MoldicideEvent.DIAGNOSTICS_COMPLETE,1,0,null);
//			GameEventManager.DispatchEvent(diagnosticEvent);
//		}
		StartCoroutine(LevelComplete(LevelRecord.LevelType.STANDARD));

	}
	
	
	


	protected virtual void ResetGUI()
	{
	}


	protected virtual IEnumerator LevelComplete(LevelRecord.LevelType type)
	{
		GameEvent gameEvent = new GameEvent(MoldicideEvent.LEVEL_COMPLETE,0,0,null);
		GameEventManager.DispatchEvent(gameEvent);

		AudioDirector.Instance.SetTrackLevelSuccess();

		GUIManager.GetInstance().LevelComplete(1);
		GUIManager.GetInstance().HideLetterGrid();
		GUIManager.GetInstance().LockGrid();
		timeTaken = Time.time - startTime;

		yield return new WaitForSeconds(1.5f);
		GUIManager.GetInstance().FadeOut();

		GUIManager.GetInstance().ShowSuccess();

		ClearGameElements();
		
		WordManager.GetInstance().Reset();
		//WordManager.GetInstance().ClearUsed();
		//GUIManager.GetInstance().LevelComplete();
		
		ResetTimers();
		
		//ScoreManager.GetInstance().LevelEnd();
		PlayerProfile profile = PlayerProfile.GetInstance();
		Score tracker = GUIManager.GetInstance().Score;
	
		int baseScore = Score.score;

		yield return new WaitForSeconds(1f);

		//new UI
		Animator anim = GameObject.Find("LevSuc_game").GetComponent<Animator>();

		GameObject.Find("dtxt_dictionarybonus").GetComponent<Text>().text = tracker.NumWords.ToString();
		GameObject.Find ("dtxt_score").GetComponent<Text>().text = baseScore.ToString();
		GameObject.Find ("dtxt_chain").GetComponent<Text>().text = tracker.LongestChain.ToString();
		GameObject.Find ("dtxt_corrections").GetComponent<Text>().text = tracker.Corrections.ToString();
		GameObject.Find ("dtxt_ernesto").GetComponent<Text>().text = TextManager.GetInstance().GetErnestoWord();
		GameObject.Find ("dtxt_protip").GetComponent<Text>().text = TextManager.GetInstance().GetSuccessTip();

		

		tracker.TimeTaken = timeTaken;


		//GameObject.Find ("LC_Time").GetComponent<GUIText>().text = tracker.TimeTaken.ToString("#.##");
		string minutes = (tracker.TimeTaken/60f).ToString("#");
		if(minutes.Length == 0)
			minutes = "0";
		string secs = (tracker.TimeTaken %60f).ToString("##");
		if(secs.Length == 1)
			secs = "0"+secs;
		else if(secs.Length == 0)
			secs = "00";

		GameObject.Find ("dtxt_time").GetComponent<Text>().text = string.Format("{0}:{1}", minutes, secs);





		int bonuses = ApplyBonuses(tracker, baseScore);
		baseScore += bonuses;


		int badges = 0;
	
		if(profile.Difficulty == (short) GameDifficulty.INSANE)
		{
			int flawlessBonus = 1000;
			if(baseScore > 4000)
				flawlessBonus = baseScore / 3;
			
			baseScore += flawlessBonus;
			GameObject.Find ("dtxt_insanebonus").GetComponent<Text>().text = flawlessBonus.ToString(); 



			if(tracker.Corrections == 0)
			{
				
				GameObject.Find ("dtxt_flawless").GetComponent<Text>().text = "flawless!";
				badges = 3;
			}
			else
			{
				badges = 2;
			}

		}
		else if(tracker.Corrections == 0)
		{
			
			GameObject.Find ("dtxt_flawless").GetComponent<Text>().text = "flawless!";
			GameObject.Find ("dtxt_insanebonus").SetActive(false);
			badges = 1;
		}
		else
		{
			GameObject.Find ("dtxt_insanebonus").SetActive(false);
		}



		GameObject.Find ("dtxt_score").GetComponent<Text>().text = baseScore.ToString();

		

		LevelRecord previousBest = profile.GetPreviousBest();
		if(previousBest.score > 0)
		{
			GameObject.Find ("dtxt_bestscore").GetComponent<CanvasGroup>().alpha = 1f;
			GameObject.Find ("dtxt_bestscore").GetComponent<Text>().text = previousBest.score.ToString();
			if(previousBest.corrections == 0)
				GameObject.Find("txt_previousflawless").GetComponent<Text>().text = "flawless";
		}

		if(baseScore > previousBest.score)
			GameObject.Find ("dtxt_newbest").GetComponent<Text>().text = "new best!";

		//don't save score for the tutorial
		if(level == 1)
			baseScore = 0;




		profile.SaveLevelRecord(type,level, (int)baseScore, badges);
		profile.Save();


		//GameObject.Find ("dtxt_tscore").GetComponent<Text>().text = profile.TotalScore.ToString();

		Score.score = 0;
		GameObject.Find ("dtxt_level").GetComponent<Text>().text = "LEVEL " + level;
		//successOverlayTitle.sprite = SpriteCache.GetInstance().LevelSucessTitles[level-1];
		//Vector3 cameraPos = Camera.main.transform.position;

		//GUIManager.GetInstance().FadeIn();
		//GameObject[] test = GameObject.FindGameObjectsWithTag("GameController");

		GameObject.FindGameObjectWithTag("GameController").transform.position = GameConstants.OVERLAY_SUCCESS_POSITION;
		Camera.main.transform.position = GameConstants.CAMERA_SUCCESS_POSITION;

		anim.SetTrigger(GUIManager.GetInstance()._triggerSuccess);
		GUIManager.GetInstance().ShowLetterGrid();
		GUIManager.GetInstance().UnLockGrid();
	}
	
	protected virtual int ApplyBonuses(Score tracker, int baseScore)
	{
		if(tracker.Corrections > 3)
			return 0 ;

		int bonus = 0;
		
		int numWords = tracker.NumWords;
		int tier = numWords / 5;
		if(tier == 0) 
			tier = 1;

		switch(tracker.Corrections)
		{
		case 3:
			bonus = 5 * tier;
			break;
		case 2:
			if(tier < 6)
				bonus = 5 * (tier+1);
			else
				bonus = 5 * (tier *2 -4);
			break;
		case 1:
			if(tier < 4)
				bonus = 5 * (tier+2);
			else
				bonus = 5 * (tier*2-1);
			break;
		case 0:
			bonus = 10 * (tier +1);
			break;
		}

		float percent = (float)bonus/100f;


		bonus = (int) (baseScore * percent);


		if(bonus == 0)
			bonus = 4-tracker.Corrections;




		if(bonus > 0)
		{
			//GameObject.Find ("LC_CorrectionBonus").GetComponent<GUIText>().text = "correction bonus";
			//GameObject.Find ("LC_Bonus_Acc").GetComponent<GUIText>().text = bonus.ToString();
			GameObject.Find("txt_bonusearned").GetComponent<CanvasGroup>().alpha = 1f;

			GameObject.Find ("dtxt_chainbonus").GetComponent<Text> ().text = bonus.ToString ();
		}
		

		return( bonus );
	}


	
}
