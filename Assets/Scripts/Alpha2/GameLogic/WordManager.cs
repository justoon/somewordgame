using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WordManager 
{
	private static WordManager instance;

	//current word we're using
	private string currentWord;
	//tracking each letter in use
	private bool[] trackedWord;

	//current difficulty settings
	private WordScrambler.ScrambleDifficulty  scrambleDifficulty;
	private WordDifficulty difficulty;

	//list of guesses we've used for this current word
	private List<string> usedWords;
	private int wordLength;
	private int startingLength;
	private GameLevel.LengthVariable wordSequenceType;
	
	private int variableLengthLimit = 9;
	private bool firstWord;
	private int lastWordSize;



	private Dictionary dict;




	private WordManager()
	{
		dict = new Dictionary();
		usedWords = new List<string>();
		firstWord = true;
	}
	
	public static WordManager GetInstance()
	{
		if(instance == null)
		{
			instance = new WordManager();
		}
		return instance;
	}


	public int WordLength {
		get {
			return wordLength;
		}
		set {
			wordLength = value;
		}
	}

	public  WordScrambler.ScrambleDifficulty ScrambleDifficulty {
		get {
			return scrambleDifficulty;
		}
		set {
			scrambleDifficulty = value;
		}
	}

	public  WordDifficulty Difficulty {
		get {
			return difficulty;
		}
		set {
			difficulty = value;
		}
	}

	public bool IsValidWord(string word)
	{
		return ( this.dict.IsValidWord(word) );
	}
	
	public string GetCurrentWord()
	{
		return this.currentWord;
	}
	
	public string GetNPCWord(int len)
	{
		return dict.GenerateNPCWord(len,difficulty);
	}

	

	public string GetWord(int wordLength, WordDifficulty difficulty,   WordScrambler.ScrambleDifficulty scrambleDifficulty)
	{
		this.currentWord  = dict.GenerateRandomWord(wordLength,difficulty, scrambleDifficulty);
		//Debug.Log(dict.GetCurrentWord());
		return this.currentWord;
	}
	
	
	public string GetWord(int length)
	{
		string word = dict.GenerateRandomWord(length,difficulty);
		this.currentWord = word;
		return word;
	}
	
	
	public void ForceUsed(string word)
	{
		usedWords.Add(word);
	}

	public bool WordUsed(string word)
	{
		return usedWords.Contains(word);
	}
	
	public void Reset()
	{

		//dictionary was not resetting words used per level
		dict.ResetUsedWords(difficulty);
		usedWords.Clear();
		firstWord = true;
		lastWordSize = 0;
		startingLength = 0;
	}
	
	public void ClearUsed()
	{
		usedWords.Clear();
	}

	public void UpdateTracking(bool trackLetters, bool fullLength)
	{
		LetterGrid letterGrid = GUIManager.GetInstance().GetLetterGrid(); 
		letterGrid.LockTiles();
		string currentGuess = letterGrid.GetCurrentWord(); //GameObject.FindGameObjectWithTag("LetterGrid").GetComponent<LetterGrid>().GetCurrentWord();//letterGrid.currentWord.ToString();
		
		
//		if(Debug.isDebugBuild)
//		{
//			GameEvent diagnosticEvent = new GameEvent(MoldicideEvent.D_GUESS_TIMESCALE, 0,0, currentGuess);
//			GameEventManager.DispatchEvent(diagnosticEvent);
//		}

		usedWords.Add(currentGuess);
		if(trackLetters)
		{
			if(letterGrid.SetUsedTiles())
			{
				//if you've used all the tiles but didn't get the whole anagram, display it for the player so they can learn
				if(!fullLength)
				{
					GUIManager.GetInstance().AnimateSolution(dict.GetCurrentWord());
				}

				switch(wordSequenceType)
				{
					case GameLevel.LengthVariable.ASCENDING:
						wordLength++;
						if(wordLength > variableLengthLimit)
						{
							firstWord = true;
							wordLength = startingLength;
							letterGrid.ResetLetterPosition();
						}
					break;
				case GameLevel.LengthVariable.DESCENDING:
						wordLength--;
						if(wordLength < variableLengthLimit)
						{
							firstWord = true;
							wordLength = startingLength;
							letterGrid.ResetLetterPosition();
						}
					break;
				case GameLevel.LengthVariable.RANDOM:
					wordLength = Random.Range(startingLength, variableLengthLimit+1);
					break;
				default: break;
				}

				GetNewWord();
			}
		}
		GUIManager.GetInstance().HideTrigger(); //reset the weapon trigger
		GUIManager.GetInstance().ResetLetterGrid(); //reset the tiles
		letterGrid.UnLockTiles();
	}

	//updates internal tracking mechanisms for the current word to determine if we need to get a new one
	public void UpdateTracking()
	{
		UpdateTracking(true,false);
			
	}

	public void ChangeDictionary()
	{
		dict.ReloadFiles();
	}
	
	public string GetUnscrambled()
	{
		return dict.GetCurrentWord();
	}


	public void GetNewWord()
	{

		GetNewWord(this.difficulty, this.scrambleDifficulty, this.wordLength);
		
	}
	
	public void GetNewWord(WordDifficulty difficulty,  WordScrambler.ScrambleDifficulty scrambleDifficulty, int wordLength)
	{
		
		usedWords.Clear();
		
	
		string scrambledWord = GetWord(wordLength,difficulty, scrambleDifficulty);

		if(startingLength == 0)
			startingLength = wordLength;

//		if(Debug.isDebugBuild)
//		{
//			GameEvent diagnosticEvent = new GameEvent(MoldicideEvent.D_WORD_TIMESCALE, 0, 0, scrambledWord + " = " + GetUnscrambled());
//			GameEventManager.DispatchEvent(diagnosticEvent);
//		}
		

		SetLetterGrid (scrambledWord);
		
		
		
	}


	public string Scramble()
	{
		string updatedScramble = new string(dict.Scramble(currentWord));
		return updatedScramble;
	}

	public void SetLetterGrid (string scrambledWord)
	{
		int counter = 0;
		LetterGrid grid = GUIManager.GetInstance ().GetLetterGrid ();
		grid.ClearLetterTiles();

		//pad the empty tiles - need to calculate x padding here and pass it to the setlettersprites method

		if (scrambledWord.Length < 9) {
			counter = (9 - scrambledWord.Length) / 2;
		}
		char c ;
		//Debug.Log("this is the scrambled word: " + scrambledWord);

		for(int i=0;i<scrambledWord.Length;i++)
		{
			c = scrambledWord[i];
			grid.SetLetterSprites (counter, c);
			counter++;
		}


		int len = scrambledWord.Length;
		if(firstWord)
		{
			if(len % 2 == 0)
				grid.ShiftLetters();
			
			firstWord = false;
			
			lastWordSize = len;
		}
		else if(wordSequenceType != GameLevel.LengthVariable.STANDARD)
		{
			if(len % 2 == 0 && lastWordSize % 2 > 0)
			{
				////Debug.Log("was odd, now even");
				grid.ShiftLetters();
			}
			else if(len % 2 > 0 && lastWordSize % 2 == 0)
			{
				grid.ResetLetterPosition();
			}
			else if(len % 2 == 0 && len != lastWordSize)
			{
				grid.ResetLetterPosition();
				grid.ShiftLetters();
			}
			lastWordSize = len;
		}





	}
	
	public void ShuffleWord()
	{

		char[] scrambledWord = dict.Scramble(this.currentWord);
		SetLetterGrid(new string(scrambledWord));
	}
	
	
	public string GetSolutionFor(int s)
	{
		return dict.GetSolutionFor(s);
	}

	public void SetCurrentWord(string word)
	{
		this.currentWord = word;
	}


	public GameLevel.LengthVariable WordSequenceType {
		get {
			return wordSequenceType;
		}
		set {
			wordSequenceType = value;
		}
	}

	public int VariableLengthLimit {
		get {
			return variableLengthLimit;
		}
		set {
			variableLengthLimit = value;
		}
	}
}
