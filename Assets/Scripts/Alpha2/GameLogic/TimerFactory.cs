﻿using UnityEngine;
using System.Collections;

public class TimerFactory {

	private static TimerFactory instance;

	public static TimerFactory GetInstance()
	{
		if(instance == null)
		{
			instance = new TimerFactory();
		}
		return instance;
	}

	private TimerFactory()
	{
	}

	public Timer CreateTimer(float countDownFrom)
	{
		return new Timer(countDownFrom);
	}
}