﻿using UnityEngine;
using System.Collections;
using System.Text;
[System.Serializable]
public class SpawnPhase  {

	private string spawn;

	public string Spawn {
		get {
			return spawn;
		}
		set {
			spawn = value;
		}
	}

	private string phasePattern;

	public string PhasePattern {
		get {
			return phasePattern;
		}
		set {
			phasePattern = value;
		}
	}

	private bool multiSpawn;

	public bool MultiSpawn {
		get {
			return multiSpawn;
		}
		set {
			multiSpawn = value;
		}
	}

	private float percentChance;

	public float PercentChance {
		get {
			return percentChance;
		}
		set {
			percentChance = value;
		}
	}

	private bool triggered;

	public SpawnPhase()
	{}


	public SpawnPhase(string pattern, bool multiSpawn, float percentChance)
	{
		this.phasePattern = pattern;
		this.multiSpawn = multiSpawn;
		this.percentChance = percentChance;
	}

	public bool Triggered {
		get {
			return triggered;
		}
		set {
			triggered = value;
		}
	}

	public override string ToString()
	{
		StringBuilder spawnPattern = 
			new StringBuilder()
				.Append("Phase pattern: " + phasePattern)
				.Append(" multiSpawn: " + multiSpawn)
				.Append(" percentChance: " + percentChance);
				
		return spawnPattern.ToString();
	}


}
