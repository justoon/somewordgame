﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager {

	public string poolName;

	private Dictionary<string, Pool> pools;

	public PoolManager()
	{
		pools = new Dictionary<string, Pool>();
		this.poolName = "undefined";
	}

	public PoolManager(string poolName)
	{
		this.poolName = poolName;
		pools = new Dictionary<string, Pool>();
	}

	public virtual bool HasCapacity(string prefabName)
	{
		Pool p = pools[prefabName];
		return p.HasCapacity();
	}

	public virtual PooledObject CheckOut(string prefabName)
	{
		//string id = prefix+prefabName;
		Pool p = null;
		if(!pools.ContainsKey(prefabName))
		{

			p = new Pool(prefabName);

			pools.Add(prefabName, p);
		}
		else
		{
			p = pools[prefabName];
		}
		PooledObject pooledbject = p.GetObject();
		pooledbject.identifier = prefabName;
		return pooledbject;
	}

	public virtual void CheckIn(PooledObject pooledObject)
	{
		if(!pools.ContainsKey(pooledObject.identifier))
			return;

		Pool p = pools[pooledObject.identifier];
		p.ReleaseObject(pooledObject);
	}

	public virtual PooledObject Prime(string prefabName)
	{
		Pool p = null;
		if(!pools.ContainsKey(prefabName))
		{
			p = new Pool(prefabName);
			pools.Add(prefabName, p);
		}
		else
		{
			p = pools[prefabName];
		}
		return p.AddNew(prefabName);

	}


	public virtual void Clear()
	{


		pools.Clear();
	}


}
