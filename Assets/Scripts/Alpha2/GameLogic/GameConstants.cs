﻿using UnityEngine;
using System.Collections;

public class GameConstants
{

	public static int BOMB_REQUIREMENT = 25;

	public static int TOTAL_LEVELS = 77;
	public static string LEVEL_MAIN = "Main";
	public static string LEVEL_SELECT = "LevelSelect";

	public static string LEVEL_TUTORIAL = "Tutorial";
	public static string LEVEL_SURVIVAL1 = "Survival_V2";
	public static string LEVEL_GAMEOVER = "GameOver";
	public static string LEVEL_GAMECOMPLETE = "GameComplete";
	public static string LEVEL_LEADERBOARD = "Leaderboard";
	public static string LEVEL_LEADERBOARD_ANDROID = "Leaderboard-Android";
	public static string LEVEL_LEADERBOARD_AMAZON = "Leaderboard-Amazon";
	public static string LEVEL_LOAD = "Loading";


	public static Vector3 CAMERA_GAME_POSITION = new Vector3(0f, 0f, -10f);
	public static Vector3 CAMERA_SUCCESS_POSITION = new Vector3(-0f, -6.558439f, -10f);
	public static Vector3 CAMERA_FAIL_POSITION = new Vector3(0f, -13.17977f,-10f);
	public static Vector3 CAMERA_SETTING_POSITION = new Vector3(0f, -19.77473f,-10f);

	public static Vector3 OVERLAY_SUCCESS_POSITION = new Vector3(-0f, -6.558439f, 0f);
	public static Vector3 OVERLAY_FAIL_POSITION = new Vector3(0f, -13.17977f, 0f);
	public static Vector3 OVERLAY_RESET_POSITION = new Vector3(0f, 0f, 0f);
	public static Vector3 OVERLAY_SETTING_POSITION = new Vector3(0f, -19.77473f,0f);


	public static string SDELIMITER = "#";

	public static string DELIMITER = ",";
	

	
	//public static string WEAPON_PREFAB_PATH = "Prefabs/Weapons/";
	public static string WEAPON_PREFAB_PATH = "Prefabs/Alpha2/Weapon/";
	public static string ENEMY_PREFAB_PATH = "Prefabs/Alpha2/Enemy/";
	public static string GUI_PREFAB_PATH = "Prefabs/GUI/";
	public static string LEVEL_SELECT_PREFAB_PATH = "Prefabs/Alpha2/GUI/LevelSelect/";
	public static char[] alpha = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

	//sprite names mapped to sprite sheet index
	//SPRITE_<OBJECT>_<VARIANT> eg. SPRITE_WEAPON_BLANK

	
	public static int SPRITE_WEAPON_CANNON_FIXED = 11;
	public static int SPRITE_WEAPON_CANNON_MOVEABLE = 12;
	public static int SPRITE_WEAPON_FLAME_FIXED = 24;
	public static int SPRITE_WEAPON_FLAME_MOVEABLE = 25;
	public static int SPRITE_WEAPON_ROCKET_FIXED = 37;
	public static int SPRITE_WEAPON_ROCKET_MOVEABLE = 38;
	public static int SPRITE_WEAPON_ROCKET_PLATE = 39;

	public static int SPRITE_WEAPON_GUI_USED = 13;
	public static int SPRITE_WEAPON_GUI_BLANK = 12;
	public static int SPRITE_WEAPON_GUI_BULLET = 0;
	public static int SPRITE_WEAPON_GUI_BULLET2 = 1;
	public static int SPRITE_WEAPON_GUI_BULLET_TAP = 2;
	public static int SPRITE_WEAPON_GUI_FLAME = 3;
	public static int SPRITE_WEAPON_GUI_FLAME2 = 4;
	public static int SPRITE_WEAPON_GUI_FLAME_TAP = 5;
	public static int SPRITE_WEAPON_GUI_ROCKET = 6;
	public static int SPRITE_WEAPON_GUI_ROCKET2 = 7;
	public static int SPRITE_WEAPON_GUI_ROCKET_TAP = 8;
	public static int SPRITE_WEAPON_GUI_LASER = 9;
	public static int SPRITE_WEAPON_GUI_LASER_TAP = 11;
	public static int SPRITE_WEAPON_GUI_CANCEL = 10;
	public static int SPRITE_WEAPON_GUI_CANCEL_TAP = 14;
	public static int SPRITE_WEAPON_GUI_CHALLENGE = 17;
	public static int SPRITE_WEAPON_GUI_CHALLENGE_TAP = 19;
	
	
	public const string SPRITE_PATH_BLOB = "Alpha2/puppet tests/blob";

	public static string WEAPON_PREFAB_ROCKET_PROJECTILE = "Prefabs/Alpha2/Weapon/Rocket";
	public static string WEAPON_PREFAB_ROCKET_PROJECTILE_2 = "Prefabs/Weapons/Rocket";

	public static string WEAPON_PREFAB_CANNON = "WeaponCannon"; 
	public static string WEAPON_PREFAB_CANNON_2 = "WeaponCannon_2";
	public static string WEAPON_PREFAB_FLAME = "WeaponFlamer";
	public static string WEAPON_PREFAB_FLAME_2 = "WeaponFlamer_2";
	public static string WEAPON_PREFAB_ROCKET = "WeaponRocket";
	public static string WEAPON_PREFAB_ROCKET_2 = "WeaponRocket_2";
	public static string WEAPON_PREFAB_LASER = "lasergrid";
	
//	public static string ENEMY_DRONE = "Drone";
//	public static string ENEMY_BEAST = "Beast";
//	public static string ENEMY_SPITTER = "Spitter";
//	public static string ENEMY_TWIN = "Twin";
//	public static string ENEMY_BLOB = "Blob";
	public static string ENEMY_DRONE = "Prefabs/Alpha2/enemy/drone_z";
	public static string ENEMY_BEAST = "Prefabs/Alpha2/enemy/beast_z";
	public static string ENEMY_SPITTER = "Prefabs/Alpha2/enemy/spitter_z";
	public static string ENEMY_TWIN = "Prefabs/Alpha2/enemy/twin_z";
	public static string ENEMY_BLOB = "Prefabs/Alpha2/enemy/blob_z";
	public static string ENEMY_CODE_DRONE = "D";
	public static string ENEMY_CODE_BEAST = "B";
	public static string ENEMY_CODE_SPITTER = "S";
	public static string ENEMY_CODE_TWIN = "T";
	public static string ENEMY_CODE_BLOB = "L";
	
	public static float PHASE_DELAY_DEFAULT = 1.0f;
	

	public static string PREFS_PLAYER_MUSIC = "PREFS_PLAYER_MUSIC";
	public static string PREFS_PLAYER_SFX = "PREFS_PLAYER_SFX";
	public static string PREFS_PLAYER_DIFFICULTY = "PREFS_PLAYER_DIFFICULTY";
	public static string PREFS_PLAYER_RATEASK = "PREFS_RATE_ASK";
	public static string PREFS_PLAYER_DICTIONARY = "PREFS_PLAYER_DICTIONARY";


	
	public static Color COLOR_SELECTION = new Color(0.2196f, .2196f, .2196f, 1);

	

	public static bool VowelC(char letter)
	{
		return letter == 'E' ||
			letter == 'A' ||
				letter == 'O' ||
				letter == 'I' ||
				letter == 'U';
	}

	//Alpha2 Additions
	public static string SPRITE_PATH_FLAWLESS_BADGE = "Alpha2/gui/lev_flawless";
	//public static string SPRITE_PATH_BARRIER_DAMAGE = "Beta/Sprites/barrier_beta";
	public static string SPRITE_PATH_LETTER_TILES = "Alpha2/ui/X_UI_letterTiles";
	public static string SPRITE_PATH_TRIGGER = "Alpha2/ui/ui_trigger";



	public static string SPRITE_PATH_BOSS_TILES = "Alpha2/enemy/boss_letters";
	public static string SPRITE_PATH_BOSS_SCRAMBLE_TILE = "Alpha2/enemy/boss_letter_cycle";

	public static string SPRITE_PATH_WEAPON_QUEUE_CANNON= "Alpha2/gui/widget_queue_cannon";
	public static string SPRITE_PATH_WEAPON_QUEUE_FLAMER= "Alpha2/gui/widget_queue_flamer";
	public static string SPRITE_PATH_WEAPON_QUEUE_ROCKET= "Alpha2/gui/widget_queue_rocket";

	public static string SPRITE_PATH_SPITTER_TILE_GOO = "Alpha2/gui/tile_spit"; //now located in Alpha2/ui/ui_trigger (slice_name: ui_trigger_slime; slice_index:17)



	//padding
	public static short SPRITE_USED = 27;
	public static short SPRITE_INDEX_CANNON = 1;
	public static short SPRITE_INDEX_CANNON_2 = 2;
	public static short SPRITE_INDEX_FLAMER = 3;
	public static short SPRITE_INDEX_FLAMER_2 = 4;
	public static short SPRITE_INDEX_ROCKET = 5;
	public static short SPRITE_INDEX_ROCKET_2 = 6;
	public static short SPRITE_INDEX_LASER = 7;
	public static short SPRITE_INDEX_CHALLENGE = 10;

	public static string TAG_UI_MANAGER = "GameController";







}
