﻿using UnityEngine;
using System.Collections;

public class GUIEventManager : MonoBehaviour {

	public delegate void ClickAction(GameEvent gameEvent);
	public static event ClickAction OnClicked;
	
	
	public static void DispatchEvent(GameEvent gameEvent)
	{
		if(OnClicked != null)
		{
			OnClicked(gameEvent);
		}
		
	}
}
