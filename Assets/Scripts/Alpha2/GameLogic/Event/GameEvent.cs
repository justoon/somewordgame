﻿using UnityEngine;
using System.Collections;

public struct GameEvent  {

	private MoldicideEvent name;
	private int value;
	private int id;
	private float timeStamp;

	private object action;

	public GameEvent(MoldicideEvent name)
	{
		this.name = name;
		this.value = -1;
		this.id = -1;
		this.action = null;
		this.timeStamp = Time.time;
	}

	public GameEvent(MoldicideEvent name, int value, int id, object action)
	{
		this.name = name;
		this.value = value;
		this.id = id;
		this.action = action;
		this.timeStamp = Time.time;
	}
	
	public int GetID()
	{
		return id;
	}
	
	public object GetAction()
	{
		return action;
	}
	
	public MoldicideEvent GetName() {
		return name;
	}
	
	public int GetValue()
	{
		return value;
	}

	public float TimeStamp {
		get {
			return timeStamp;
		}
	}
}
