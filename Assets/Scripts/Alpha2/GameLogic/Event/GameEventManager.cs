﻿	using UnityEngine;
using System.Collections;

public class GameEventManager : MonoBehaviour {

	public delegate void EventFired(GameEvent gameEvent);
	public static event EventFired OnEvent;
	
	
	public static void DispatchEvent(GameEvent gameEvent)
	{
		if(OnEvent != null)
		{
			OnEvent(gameEvent);
		}
		
	}
}
