﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class SpawnWave {

	public float triggerAt;
	public float phaseDelay;
	public float noiseLow = .8f;
	public float noiseHigh = 1.2f;

	public int totalEnemies;
	public SpawnPhase[] phases;

	public int minMoldies;
	public int totalWaveTime;

	private Timer waveTimer;
	private int currentPhase;
	private SpawnPhase activePhase;


	//private float[] spawnEvents;

	public enum WaveState
	{
			QUEUED,
			READY,
			STARTED,
			FINISHED
	}

	public WaveState currentState;

	public SpawnWave()
	{
		currentState = WaveState.QUEUED;
		currentPhase = 0;
	}

	public int GetTotalEnemies()
	{
		return phases.Length;
	}

	public void StartWave()
	{
		if(phaseDelay == 0f)
			phaseDelay = GameConstants.PHASE_DELAY_DEFAULT;

		if(noiseLow == 0)
			noiseLow = .9f;

		if(noiseHigh == 0)
			noiseHigh = 1.1f;

		float phaseNoise = Random.Range(phaseDelay*noiseLow, phaseDelay*noiseHigh);

		//Debug.Log("using waveTimer: " + phaseNoise);

		waveTimer = TimerFactory.GetInstance().CreateTimer(phaseNoise);

		currentState = WaveState.STARTED;



		activePhase = phases[currentPhase++];



	}

	public WaveState GetState()
	{
		return currentState;
	}

	public SpawnPhase[] Phases {
		get {
			return phases;
		}
		set {
			phases = value;
		}
	}

	public void Update()
	{
		if(!activePhase.Triggered)
		{
			activePhase.Triggered = true;
			//replace with pooled checkout
			//GameObject enemy = (GameObject) GameObject.Instantiate(activePhase.Spawn);

			GameEvent gameEvent = new GameEvent(MoldicideEvent.MOLDIE_SPAWN,0,0,activePhase.Spawn);
			//create spawn event so the game controller will pick it up and allocate a new moldie
			GameEventManager.DispatchEvent(gameEvent);
			waveTimer.StartTimer();
		}
		else
		{
			if(waveTimer.HasStarted())
			{
				//if we've reached the end of the phase delay/spacing
				if(waveTimer.Update())
				{
					//if we have any more phases to spawn
					if(currentPhase < phases.Length)
					{
						activePhase = phases[currentPhase++];
						waveTimer.StopTimer();
						float phaseNoise = Random.Range(phaseDelay*noiseLow, phaseDelay*noiseHigh);
						//Debug.Log("using waveTimer: " + phaseNoise);
						waveTimer.UseNewTime(phaseNoise);
					}
					else
					{
						currentState = WaveState.FINISHED;	
						waveTimer.StopTimer();
					}
				}
			}
		}
			
	}


}
