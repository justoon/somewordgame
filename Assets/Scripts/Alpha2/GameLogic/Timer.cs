﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class Timer  {

	private float countDownFrom;
	private float currentCounter;
	private bool timerStarted;

	public Timer(float countDownFrom)
	{
		this.countDownFrom = countDownFrom;
	}

	public void UseNewTime(float newTime)
	{
		countDownFrom = newTime;
	}

	public bool HasStarted()
	{
		return this.timerStarted;
	}

	public void StartTimer(bool triggerImmediately)                  
	{
		if(triggerImmediately)
		{
			this.currentCounter = 0;
		}
		else
		{
			this.currentCounter = countDownFrom;
		}
		this.timerStarted = true;
	}


	public void StartTimer()
	{
		this.currentCounter = countDownFrom;
		this.timerStarted = true;
	}

	public void StopTimer()
	{
		this.timerStarted = false;
	}

	public float GetCurrent()
	{
		return currentCounter;
	}

	public void ModifyTimer(float amt)
	{
		//suspend while we make the adjustment to avoid any contention
		timerStarted = false;
		this.currentCounter -= amt;
		timerStarted = true;
	}

	public bool Update()
	{

		bool trigger = false;
		if(timerStarted)
		{
			this.currentCounter -= Time.deltaTime;
			if(this.currentCounter <= 0)
			{
				trigger = true;
				StartTimer();
			}
		}
		return trigger;
	}
}
