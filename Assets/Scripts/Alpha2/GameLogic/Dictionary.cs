﻿/* Copyright Sprouted Interactive, LLC. 2013 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;


public class Dictionary  {



	//contains a list of all the words in the dictionary
	private string[] masterWordList;
	private HashSet<string> checkList;
	private List<int>[] filteredWordList; //0-5:easy 6-11:hard, contains indeces to masterWordList
	private List<int> usedWords;
	private int[] challengeWordsEasy; //[ masterWordList index]  = matches[]
	private int[] challengeWordsHard;

	private int[][] challengeMatchesEasy;
	private int[][] challengeMatchesHard;


	private WordScrambler scrambler;

	private string currentWord;


	public Dictionary()
	{
		scrambler = new WordScrambler();
		LoadFiles();
	}


	public void ReloadFiles()
	{
		LoadFiles();
	}


	private void LoadFiles()
	{
//		System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();


		//TextAsset masterDictionary = Resources.Load("btmDictionary") as TextAsset;

		TextAsset masterDictionary = Resources.Load("btmDictionary2") as TextAsset;

		TextAsset obviousDictionary = null; 
		TextAsset obnoxiousDictionary = null; 

		if(PlayerProfile.GetInstance().UsDictionary)
		{
			//obviousDictionary = Resources.Load("filteredWordList2") as TextAsset;
			obviousDictionary = Resources.Load("filteredWordList3") as TextAsset;
			//obnoxiousDictionary = Resources.Load("filteredWordList2h") as TextAsset;
			obnoxiousDictionary = Resources.Load("filteredWordList3h") as TextAsset;
		}
		else
		{
			//obviousDictionary = Resources.Load("filteredWordListUK") as TextAsset;
			//obnoxiousDictionary = Resources.Load("filteredWordListUK_H") as TextAsset;

			obviousDictionary = Resources.Load("filteredWordList3UK") as TextAsset;
			obnoxiousDictionary = Resources.Load("filteredWordList3hUK") as TextAsset;
		}


		string text = masterDictionary.text;
		this.masterWordList = text.Split(new string[] { GameConstants.DELIMITER }, System.StringSplitOptions.None);

		this.checkList = new HashSet<string>(masterWordList);

		text = obviousDictionary.text;

		string[] easyWords = text.Split(new string[] { GameConstants.DELIMITER }, System.StringSplitOptions.None);

		text = obnoxiousDictionary.text;

		string[] hardWords = text.Split(new string[] { GameConstants.DELIMITER }, System.StringSplitOptions.None);


		this.usedWords = new List<int>();
		this.filteredWordList = new List<int>[12];
		

		int counter = 0;

		List<int> tempChallengeWords = new List<int>();
		int[] matches;
		List<int[]> matchCache = new List<int[]>();
		string wordBundle;
		for(int i=0;i<easyWords.Length;i++)
		{
			wordBundle = easyWords[i];
			string word = wordBundle;
			int w = wordBundle.IndexOf(":");
			if(wordBundle.Contains(":"))
			{
				word = wordBundle.Substring(0,w);
				string sub = wordBundle.Substring(w+1, wordBundle.Length-w-1);
				////Debug.Log(sub);

				string[] indeces = sub.Split(new char[]{';'});
				matches = new int[indeces.Length];
				string index;

				for(int j=0;j<indeces.Length;j++)
				{
					index = indeces[j];
					matches[counter++] = System.Convert.ToInt32(index);
				}
				matchCache.Add (matches);
				tempChallengeWords.Add(System.Convert.ToInt32(word));
			}

			//Debug.Log("looking up " + word);

			int wordLength = masterWordList[System.Convert.ToInt32(word)].Length;

			List<int> wordList = filteredWordList[wordLength-4];
			if(wordList == null)
			{
				wordList = new List<int>();
			}

			wordList.Add(System.Convert.ToInt32(word));
			filteredWordList[wordLength-4] = wordList;
			counter = 0;
		}

		challengeWordsEasy = new int[tempChallengeWords.Count];
		challengeMatchesEasy = new int[tempChallengeWords.Count][];
		for(int x=0;x<tempChallengeWords.Count;x++)
		{
			challengeWordsEasy[x] = tempChallengeWords[x];
			challengeMatchesEasy[x] = matchCache[x];
		}

		tempChallengeWords.Clear();
		matchCache.Clear();

		counter = 0;
		for(int i=0;i<hardWords.Length;i++)
		{
			wordBundle = hardWords[i];
			string word = wordBundle;
			int w = wordBundle.IndexOf(":");
			if(wordBundle.Contains(":"))
			{
				word = wordBundle.Substring(0,w);

				string[] indeces = wordBundle.Substring(w+1, wordBundle.Length-w-1).Split(new char[]{';'});
				matches = new int[indeces.Length];
				
				string index;
				
				for(int j=0;j<indeces.Length;j++)
				{
					index = indeces[j];
					matches[counter++] = System.Convert.ToInt32(index);
					matchCache.Add (matches);
				}

				tempChallengeWords.Add(System.Convert.ToInt32(word));
			}


			int wordLength = masterWordList[System.Convert.ToInt32(word)].Length;




			List<int> wordList = filteredWordList[wordLength+2];
			if(wordList == null)
			{
				wordList = new List<int>();
			}
			
			wordList.Add(System.Convert.ToInt32(word));
			filteredWordList[wordLength+2] = wordList;
			counter = 0;
		}

		challengeWordsHard = new int[tempChallengeWords.Count];
		challengeMatchesHard = new int[tempChallengeWords.Count][];
		
		for(int x=0;x<tempChallengeWords.Count;x++)
		{
			challengeWordsHard[x] = tempChallengeWords[x];
			challengeMatchesHard[x] = matchCache[x];
		}

//		sw.Stop();
//		System.TimeSpan elapsedTime = sw.Elapsed;
//		//Debug.Log(elapsedTime.Milliseconds);

		Resources.UnloadAsset(masterDictionary);
		Resources.UnloadAsset(obnoxiousDictionary);
		Resources.UnloadAsset(obviousDictionary);
		                                   
	}





	

	public string GenerateNPCWord(int length,WordDifficulty difficulty)
	{
		string word = null;

		List<int> words;

		if(difficulty == WordDifficulty.OBVIOUS)
			words = filteredWordList[length-4];
		else
			words = filteredWordList[length+2];
		
		int r = Random.Range(0,words.Count);
		
		int wordIndex = words[r];
		word = masterWordList[wordIndex];

		usedWords.Add(wordIndex);
		words.Remove(wordIndex);
		if(difficulty == WordDifficulty.OBVIOUS)
			filteredWordList[length-4] = words;
		else
			filteredWordList[length+2] = words;

		currentWord = word;
		return word;
	}

	public string GenerateRandomWord(int length, WordDifficulty difficulty)
	{
		string word = null;
		List<int> words;
		
		if(difficulty == WordDifficulty.OBVIOUS)
			words = filteredWordList[length-4];
		else
			words = filteredWordList[length+2];
		
		int r = Random.Range(0,words.Count);

		int wordIndex = words[r];
		word = masterWordList[wordIndex];
		usedWords.Add(wordIndex);
		words.Remove(wordIndex);
		if(difficulty == WordDifficulty.OBVIOUS)
			filteredWordList[length-4] = words;
		else
			filteredWordList[length+2] = words;
		currentWord = word;

		//return the list of characters 
		return word;
	}


	//check if this words exists in the dictionary
	public bool IsValidWord(string word)
	{
		//return ( System.Array.IndexOf(this.masterWordList, word) != -1 );
		return checkList.Contains(word);
	}


	public string GenerateRandomWord(int length,WordDifficulty difficulty,  WordScrambler.ScrambleDifficulty scrambleDifficulty)
	{
		string word = null;
		int indexOffset = 0;
		
		switch(difficulty)
		{
		case WordDifficulty.OBVIOUS:
			indexOffset = length -4;
			break;
		case WordDifficulty.OBNOXIOUS:
			indexOffset = length + 2;
			break;
		default:break;
		}
		
		
		
		List<int> words = filteredWordList[indexOffset];//(List<string>)wordsByLength[length];
		
		int r = Random.Range(0,words.Count);
		
		int wordIndex = words[r];

		word = masterWordList[wordIndex];
		usedWords.Add(wordIndex);
		words.Remove(wordIndex);
		filteredWordList[indexOffset] = words;
		currentWord = word;

		//Debug.Log("generated: " + word);


		char[] scrambled = scrambler.ScrambleWord (word, scrambleDifficulty,word.Length);
		word = new string(scrambled);
		
		return word;
	}


	//refactor this to use sets - available words and used words, move from one to the other on use and then move all used back
	//to the original sets
	//when changing difficulty this needs to be reset before confirming difficulty change
	public void ResetUsedWords(WordDifficulty difficulty)
	{
		int indexOffset = 0;
		
		switch(difficulty)
		{
		case WordDifficulty.OBVIOUS:
			indexOffset = -4;
			break;
		case WordDifficulty.OBNOXIOUS:
			indexOffset = 2;
			break;
		default:break;
		}

		int word;
		for(int i=0;i<usedWords.Count;i++)
		{
			word = usedWords[i];
			int len = masterWordList[word].Length;
			filteredWordList[len+indexOffset].Add(word);
		}
		usedWords.Clear();
	}

	public string GetCurrentWord()
	{
		return currentWord;
	}


	public char[] Scramble(string word, WordScrambler.ScrambleDifficulty difficulty)
	{
		return scrambler.ScrambleWord(word, difficulty,word.Length);
		
	}

	public char[] Scramble(string word)
	{
		return scrambler.ScrambleWord(word,  WordScrambler.ScrambleDifficulty.HARD,word.Length);
		
	}

	public string GetSolutionFor(int index)
	{

		string solution = masterWordList[index];

		return solution;
	}
}


