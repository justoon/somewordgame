﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool  {


	private List<PooledObject> _available = new List<PooledObject>();
	private List<PooledObject> _inUse = new List<PooledObject>();

	private string prefabPath;
	

	public Pool(string prefabPath)
	{
		this.prefabPath = prefabPath;
	}

	public bool HasCapacity()
	{
		return _available.Count != 0;
	}


	public void Reset()
	{
		_available.Clear();
		_inUse.Clear();
	}

	public PooledObject GetObject()
	{
		lock(_available)
		{
			if (_available.Count != 0)
			{
				PooledObject po = _available[0];
				_inUse.Add(po);
				_available.RemoveAt(0);
				return po;
			}
			else
			{
				PooledObject po = ((GameObject)GameObject.Instantiate(Resources.Load(prefabPath))).GetComponent<PooledObject>();
				_inUse.Add(po);
				return po;
			}
		}
	}


	public void AddPrefab(GameObject prefab)
	{
		PooledObject po = prefab.GetComponent<PooledObject>();
		po.identifier = prefab.name;
		_available.Add(po);
	}


	public PooledObject AddNew(string prefabPath)
	{
		PooledObject po = ((GameObject)GameObject.Instantiate(Resources.Load(prefabPath))).GetComponent<PooledObject>();
		po.identifier = prefabPath;
		_available.Add(po);

		return po;
	}
	
	public void ReleaseObject(PooledObject po)
	{
		CleanUp(po);
		
		lock (_available)
		{
			_available.Add(po);
			_inUse.Remove(po);
		}
		po.ResetComplete();
	}
	
	private void CleanUp(PooledObject po)
	{
		po.Reset();
	}
}
