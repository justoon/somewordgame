﻿using UnityEngine;
using System.Text;
using System.Collections;

public class WordScrambler  {


	public enum ScrambleDifficulty
	{
		EASY=0,
		MEDIUM=1,
		HARD=2,
		ALPHA=3
	}

	public enum ScrambleZone
	{
		START,
		MIDDLE,
		END
	}


	public char[] ScrambleWord(string word, WordScrambler.ScrambleDifficulty difficulty, int length)
	{

		return ScrambleWord(word, difficulty, length, ScrambleZone.END);
	}

	public char[] ScrambleWord(string word, WordScrambler.ScrambleDifficulty difficulty, int length, ScrambleZone zone)
	{
		char[] scrambled;
		string substring;
		string original = word;


		if(difficulty == ScrambleDifficulty.ALPHA)
		{
			scrambled = word.ToCharArray();
			char temp;
			
			for(int i=0;i<scrambled.Length-1;i++)
			{
				for(int j=i+1;j<scrambled.Length;j++)
				{
					if(scrambled[i] > scrambled[j])
					{
						temp = scrambled[j];
						scrambled[j] = scrambled[i];
						scrambled[i] = temp;
					}
				}
			}
			word = new string(scrambled);;
		}
		else
		{
			int x = 0;

			switch(difficulty)
			{
			case ScrambleDifficulty.EASY:
				x = length / 2 + length % 2;
				break;
			case ScrambleDifficulty.MEDIUM:
				x = length / 2 + length % 2 + 1;
				break;
			case ScrambleDifficulty.HARD:
				x = length;
				break;
			default:
				break;
			}

//			//Debug.Log("x: " + x);

			string leftover;

			int startOrEnd = Random.Range(0,2);
			if(startOrEnd == 0)
				zone = ScrambleZone.START;
			else
				zone = ScrambleZone.END;


			switch(zone)
			{
			case ScrambleZone.START:
				substring = word.Substring(0,x);
				leftover = word.Substring(x, word.Length-x);

				////Debug.Log("substring: " + substring);
				////Debug.Log("leftover: " + leftover);

				////Debug.Log("scrambling: " + substring);
				scrambled = Scramble(substring);
				////Debug.Log("scrambled: " + new string(scrambled));
				if(new string(scrambled).Equals(substring))
				{
					System.Array.Reverse(scrambled);			
				}
				word = new string(scrambled) + leftover;


				//word.Insert(0, new string(scrambled));
				////Debug.Log("scrambled: " + new string(word));

				break;
			case ScrambleZone.END:
				substring = word.Substring(word.Length-x, x);
				leftover = word.Substring(0,word.Length-x);


				/////Debug.Log("substring: " + substring);
				////Debug.Log("leftover: " + leftover);

				////Debug.Log("scrambling: " + substring);
				scrambled = Scramble(substring);
				////Debug.Log("scrambled: " + new string(scrambled));

				word =  leftover +new string(scrambled);
				////Debug.Log("scrambled: " + new string(word));
				//word.Insert(word.Length-x, new string(scrambled));

				break;

			default: break;
			}





		}

		//simply reverse the word if we've scrambled the word and it = our original word  or 'this' or 'hits' is scrambled as 'shit'
		if(word.Equals(original) || word.Equals("SHIT"))
		{
			char[] chars = original.ToCharArray();
			System.Array.Reverse(chars);
			word = new string(chars);
		}

		return word.ToCharArray();


	}

	public char[] Scramble(string word)
	{
		char[] scrambled = word.ToCharArray();
		int x = 0;
		//uses Fisher-Yates shuffle  
		int n = word.Length;  

		while (n > x) {  
			n--;  
			int k = Random.Range(x,n + 1);  
			char value = scrambled[k];  
			scrambled[k] = scrambled[n];  
			scrambled[n] = value;  
		} 
		
		
		//don't accept the word if it's spelled out exactly
		if(new string(scrambled).Equals(word))
		{
			System.Array.Reverse(scrambled);
		}
		
		return scrambled;
	}



}
