using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public bool useAds;
	public int retryCount;
	//score settings
	public static int baseScore = 1;
	public static float baseMultiplier = 1f;
	public static float factor = 2f;

	//difficulty adjustments
	public static short scrambleAdjustment = -1;
	public static short maxEnemyAdjustment = -1;
	public static float noiseLow = 1f;
	public static float noiseHigh = 1f;
	public static float thinkAdjustLow = .8f;
	public static float thinkAdjustHigh = 1.2f;


	private static GameController instance = null;
	public static GameController Instance {
		get { return instance; }
	}

	private bool levelDone;
	private TouchManager touchManager;
	private GameMap gameMap;
	private WaitForSeconds pauseTime = new WaitForSeconds(2f);


	protected float timeOffset;

	void Awake() 
	{
		if (instance != null && instance != this) 
		{
			Destroy(this.gameObject);
			return;
		} 
		else 
		{
			
			InitGame();
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}




	private void InitGame()
	{
		//Debug.Log("GameController.InitGame()");
		//Workaround for Galaxy Note 10.1/2014 Issues
		if(SystemInfo.deviceName.Contains("Galaxy Note 10.1 2014"))
		{
			Handheld.ClearShaderCache();
		}


	/*
	 * Initial Load:
	 * 1. Word Lists
	 * 2. Game Map
	 * 3. Profile
	 * 4. Sprite Cache
	 * 5. Register for GUI and GameEvents
	 * 6. On done - > move to Main
	 */
		WordManager.GetInstance();
		gameMap = GameMap.GetInstance();
		PlayerProfile profile = PlayerProfile.GetInstance();
		SpriteCache.GetInstance();
		TextManager.GetInstance();
		//setup initial score settings
		UpdateDifficultySettings((GameDifficulty)profile.Difficulty);





		GUIEventManager.OnClicked += ExecuteGUIEvent;
		//register the controller to receive Game Events (events triggered by game object interactions)
		GameEventManager.OnEvent += ExecuteGameEvent;
		
		if(Application.loadedLevelName.Equals(GameConstants.LEVEL_LOAD))
			StartCoroutine(Pause());
		else if(!Application.loadedLevelName.Equals(GameConstants.LEVEL_MAIN))
			LoadLevel(GameConstants.LEVEL_MAIN);


	}

	void OnLevelWasLoaded(int level)
	{
		if(Application.loadedLevelName.Equals(GameConstants.LEVEL_MAIN))
			StartCoroutine(PauseOnMain());
	}


	public void UpdateDifficultySettings(GameDifficulty difficulty)
	{
		switch(difficulty)
		{
		case GameDifficulty.EASY: 
			GameController.baseScore = 1;  
			GameController.baseMultiplier = 1f; 
			GameController.factor = 3f;
			GameController.scrambleAdjustment= -1;
			GameController.maxEnemyAdjustment = -1;
			GameController.noiseLow = .5f;
			GameController.noiseHigh = .8f;
//			GameController.thinkAdjustLow = 4f;
//			GameController.thinkAdjustHigh = 5.5f;
			
			break; //1* 2^wordLength-3
		case GameDifficulty.NORMAL: 
			GameController.baseScore = 2; 
			GameController.baseMultiplier = 1f; 
			GameController.factor = 2f;
			GameController.scrambleAdjustment= 0;
			GameController.maxEnemyAdjustment = 0;
			GameController.noiseLow = .8f;
			GameController.noiseHigh = 1.2f;
//			GameController.thinkAdjustLow = .8f;
//			GameController.thinkAdjustHigh = 1.2f;
			break; //1* 2^wordLength-2
		case GameDifficulty.HARD: 
			GameController.baseScore = 3; 
			GameController.baseMultiplier = 3f; 
			GameController.factor = 3f; 
			GameController.scrambleAdjustment= 1;
			GameController.maxEnemyAdjustment = 1;
			GameController.noiseLow = 1.0f;
			GameController.noiseHigh = 1.3f;
//			GameController.thinkAdjustLow = .75f;
//			GameController.thinkAdjustHigh = 1f;
			break; //3* 2^wordLength-3
		case GameDifficulty.INSANE: 
			GameController.baseScore = 4; 
			GameController.baseMultiplier = 4f; 
			GameController.factor = 3f; 
			GameController.scrambleAdjustment= 3;
			GameController.maxEnemyAdjustment = 2;
			GameController.noiseLow = 1.0f;
			GameController.noiseHigh = 1.3f;
//			GameController.thinkAdjustLow = .5f;
//			GameController.thinkAdjustHigh = .75f;
			break; //4 * 2^wordLength-3
		default: break;		
		}
	}


	private IEnumerator Pause() 
	{
		yield return pauseTime;
		LoadLevel(GameConstants.LEVEL_MAIN);
	}

	private IEnumerator PauseOnMain()
	{
		yield return pauseTime;
        GameMap.GetInstance().SetIndex(3);
        GameMap.GetInstance().currentLevel = "Level2";
        GUIManager.GetInstance().LevelFadeOut();
        //if(PlayerProfile.GetInstance().CurrentLevel > 1)
        //{
        //	GameController.GetInstance().LoadLevel("LevelSelect");
        //}
        //else if(PlayerProfile.GetInstance().CurrentLevel > 0)
        //{
        //	GameMap.GetInstance().SetIndex(2);
        //	GameMap.GetInstance().currentLevel = "Level1";
        //	GUIManager.GetInstance().LevelFadeOut();
        //}
        //else
        //{
        //	GameMap.GetInstance().SetIndex(1);
        //	GameController.GetInstance().LoadLevel("StoryTemplate");
        //}
    }


    void OnDestroy()
	{
		GUIEventManager.OnClicked -= ExecuteGUIEvent;
		//register the controller to receive Game Events (events triggered by game object interactions)
		GameEventManager.OnEvent -= ExecuteGameEvent;
	}

	public static GameController GetInstance()
	{
		return instance;
	}

	public void LoadLevel(int levelId)
	{
		//GUIManager.GetInstance().ShowLoadTransition();
		Application.LoadLevel(levelId);
	}

	public void LoadLevel(string level)
	{

		//show the loading screen before executing the load level
		//GUIManager.GetInstance().ShowLoadTransition();
		Application.LoadLevel(level);
		//GUIManager.GetInstance().FadeIn();
	}

	public virtual void DisableGUI()
	{
		touchManager.acceptInput = false;
	}

	public virtual void EnableGUI()
	{
		touchManager.acceptInput = false;
	}
	
	public virtual void PauseGame()
	{

		Time.timeScale=0f;
		if(touchManager)
			touchManager.acceptInput = false;
	}
	
	public virtual void ResumeGame()
	{
		if(touchManager)
			touchManager.acceptInput = true;
		Time.timeScale=1f;
	}

	public void LevelStart()
	{
		touchManager = GameObject.FindGameObjectWithTag(GameConstants.TAG_UI_MANAGER).GetComponent<TouchManager>();
	}

	//public void LoadNextStoryLevel()
	//{
	//	string newLevel = gameMap.GetLevelName(gameMap.GetNextLevel());
	//	retryCount = 0;
	//	levelDone = false;
	//	//use template
	//	if(newLevel.Contains("Story"))
	//	{
	//		newLevel = "StoryTemplate";
	//		gameMap.currentLevel = newLevel;
	//		LoadLevel(newLevel);
	//	}
	//	//cut to upgrade
	//	else if(newLevel.Contains("Upgrade"))
	//	{
	//		newLevel = "UpgradeTemplate";
	//		gameMap.currentLevel = newLevel;
	//		LoadLevel(newLevel);
	//	}
	//	//show loading screen
	//	else
	//	{
	//		gameMap.currentLevel = newLevel;

	//		//FadeOut the Level, when the Fade completes, the GUI Manager will request the Transition Level is called
	//		GUIManager.GetInstance().LevelFadeOut();
	//	}
	//}

	public void LoadSurvival()
	{
		GameMap.GetInstance().currentLevel = GameConstants.LEVEL_SURVIVAL1;
		GUIManager.GetInstance().LevelFadeOut();
	}



	public void GameOver()
	{
		WordManager.GetInstance().Reset();
	}

	public void LevelComplete()
	{
		LevelComplete(LevelRecord.LevelType.STANDARD);
	}
	
	public void LevelComplete(LevelRecord.LevelType type)
	{
		DisableGUI();
		//Reset();
	}

	private void ExecuteGameEvent(GameEvent gameEvent)
	{
		if(gameEvent.GetName() == MoldicideEvent.LEVEL_START)
		{
			GameLevel level = ( GameLevel ) gameEvent.GetAction();
			timeOffset = Time.time;
			level.ActivateLevel();
			//GUIManager.GetInstance().LevelLoaded();
		}


	}

	public void ResetDone()
	{
		levelDone = false;
	}


	public void SetDone()
	{
		levelDone = true;
		GUIManager.GetInstance().DisableQuit();
	}

	public bool IsDone()
	{
		return levelDone;
	}


	private void ExecuteGUIEvent(GameEvent gameEvent)
	{
	}

	public int IncreaseRetry()
	{
		retryCount++;
		return retryCount;
	}

}
