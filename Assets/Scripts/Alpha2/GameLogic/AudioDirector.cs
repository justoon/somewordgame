﻿using UnityEngine;
using System.Collections;

public class AudioDirector : MonoBehaviour {

	public bool sfxOn;
	public bool musicOn;

	public AudioSource music;
	public AudioSource guiSfx;

	public AudioClip currentTrack;

	public AudioClip mainTitle;
	public AudioClip[] urgency1;
	public AudioClip[] urgency2;
	public AudioClip[] urgency3;
	public AudioClip[] urgency4;
	public AudioClip bossTrack;

	public AudioClip[] guiClips;

	private int currentUrgency;

	private static AudioDirector instance;


	private float clipLength = 0f;
	private float timer = 0f;

	private int[] playList;
	private int trackPointer = 0;

	private enum MusicState 
	{
		START,
		MAIN_TITLE,
		START_LEVEL,
		BOSS_LEVEL,
		PLAYING,
		STOPPED
	}

	private MusicState currentState;
	private MusicState levelCache;

	public static AudioDirector Instance {
		get { return instance; }
	}

	void Awake() 
	{
		if (instance != null && instance != this) 
		{
			Destroy(this.gameObject);
			return;
		} 
		else 
		{
			

			sfxOn = PlayerProfile.GetInstance().SfxOn;
			musicOn = PlayerProfile.GetInstance().MusicOn;
			SetTrackToMain();
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}

	public void SfxOff()
	{
		sfxOn = false;
	}
	
	public void SfxOn()
	{
		sfxOn = true;
	}

	public void On()
	{
		switch(levelCache)
		{
		case MusicState.MAIN_TITLE:
			SetTrackToMain();
			break;
		case MusicState.BOSS_LEVEL:
			PlayBossTrack();
			break;		
		case MusicState.PLAYING:
			PlayNextTrack();
			break;
		default:
			SetTrackToMain();
			break;
		}

	}
	
	public void Off()
	{
		PlayerProfile.GetInstance().MusicOn = false;
		currentState = MusicState.STOPPED;
		music.Stop();
	}


	public void SetTrackToMain() {


		if(currentState != MusicState.MAIN_TITLE) {
			music.Stop();
		}

		currentState = levelCache = MusicState.MAIN_TITLE;
		music.clip = currentTrack = mainTitle;
		music.loop = true;
		if(musicOn) 
		{
			music.Play();
		}
	}

	public void PlayGUIClip(int index)
	{
		if(sfxOn)
		{
			guiSfx.Stop();
			guiSfx.clip = guiClips[index];
			guiSfx.Play();
		}
	}

	public void PlayBossTrack()
	{
		if(currentState != MusicState.BOSS_LEVEL) {
			music.Stop();
		}

		music.clip = currentTrack = bossTrack;
		music.loop = true;
		currentState = levelCache = MusicState.BOSS_LEVEL;
		if(musicOn)
		{
			music.Play();
		}
	}

	public void StartPlayList(int[] trackList) 
	{
		if(currentState != MusicState.START_LEVEL) {
			music.Stop();
		}
		playList = trackList;

		currentState = MusicState.START_LEVEL;
		music.loop = false;

		currentUrgency = playList[0];
		trackPointer = 0;
		timer = 0f;

		PlayNextTrack();


	}

	private void PlayNextTrack()
	{
		int track = Random.Range(0,3);
		
		switch(currentUrgency)
		{
		case 0:
			currentTrack = urgency1[track];
			break;
		case 1: 
			currentTrack = urgency2[track];
			break;
		case 2:
			currentTrack = urgency3[track];
			break;
		case 3:
			currentTrack = urgency4[track];
			break;
		default: break;
		}

		music.clip = currentTrack;
		clipLength = currentTrack.length;
		
		currentState = levelCache = MusicState.PLAYING;
		if(musicOn)
		{
			music.Play();
		}
	}

	void Update()
	{
		if(currentState == MusicState.PLAYING && !music.loop) 
		{
			timer += Time.deltaTime;
			if(timer > clipLength)
			{
				trackPointer++;
				if(trackPointer >= playList.Length)
				{
					trackPointer--;
				}
				currentUrgency = playList[trackPointer];
				timer = 0;
				PlayNextTrack();

			}
		}
	}


	public void IncreaseUrgency()
	{
		Debug.Log("increasing urgency to max");

		if(currentState != MusicState.BOSS_LEVEL) 
		{

			currentState = MusicState.STOPPED;

			if(PlayerProfile.GetInstance().SfxOn)
			{
				guiSfx.clip = guiClips[3];
				guiSfx.Play();
			}

			music.loop = true;
			currentUrgency = 3;
			timer = 0;
			PlayNextTrack();
		}
	}

	public void SetTrackLevelSuccess()
	{
		music.Stop();
		currentState = MusicState.STOPPED;

		if(PlayerProfile.GetInstance().SfxOn)
		{
			int r = Random.Range(5,8);
			guiSfx.clip = guiClips[r];
			guiSfx.Play();
		}
		
	}

	public void SetTrackGameOver()
	{
		currentState = MusicState.STOPPED;
 		music.Stop();

		if(PlayerProfile.GetInstance().SfxOn)
		{
			guiSfx.clip = guiClips[4];
			guiSfx.Play();
		}
	}

	public void StopGameTracks()
	{
		currentState = MusicState.STOPPED;
		music.Stop();
	}

}
