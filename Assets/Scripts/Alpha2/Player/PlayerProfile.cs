﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;

public class PlayerProfile  {

	public bool rateAsk;

	public bool haveReset;

	private int bombCounter;

	private int currentLevel;
	private int highestLevel;
	private int points;

	private Hashtable weaponUpgradesStory;
	private Hashtable weaponUpgradesSurvival;

	private int maxSpawns;
	private int maxSpawnsSurvival;

	private int totalScore;
	private int survivalScore;

	private bool musicOn;
	private bool sfxOn;
	private short difficulty;
	private bool usDictionary;


	private bool scoreSubmitted;


	private List<LevelRecord> scoreMatrix;


	private static PlayerProfile instance;



	public static PlayerProfile GetInstance()
	{
		if(instance == null)
		{
			instance = new PlayerProfile();
		}
		return instance;
	}

	private PlayerProfile()
	{
		//default to musicOn/sfxOn, meaning if the value is not set or 0, flag to 1 to switch off
		musicOn = PlayerPrefs.GetInt(GameConstants.PREFS_PLAYER_MUSIC) == 1 ? false:true;
		sfxOn = PlayerPrefs.GetInt(GameConstants.PREFS_PLAYER_SFX) == 1 ? false:true;
		difficulty = (short)PlayerPrefs.GetInt(GameConstants.PREFS_PLAYER_DIFFICULTY);
		rateAsk = PlayerPrefs.GetInt(GameConstants.PREFS_PLAYER_RATEASK,0) == 1 ? true:false;
	
		usDictionary = PlayerPrefs.GetInt(GameConstants.PREFS_PLAYER_DICTIONARY,1) == 1 ? true:false;

		//PlayFab Register/Authenticate silently

		//Load Saved game
		Load();





	}

	private void LoadScores(string encoded)
	{
		//replace with getting from file payload
		scoreMatrix = new List<LevelRecord>();
		if(!string.IsNullOrEmpty(encoded))
		{
			string[] encodedLevelRecords  = encoded.Split('|');
			string encodedRecord;
			for(int i=0;i<encodedLevelRecords.Length;i++)
			{
				encodedRecord = encodedLevelRecords[i];
				LevelRecord lr = LevelRecord.Decode(encodedRecord);
				lr.index = scoreMatrix.Count;
				scoreMatrix.Add(lr);
			}
		}
	}

		private void LoadUpgrades(string encoded, string encodedSurvival)
	{
		weaponUpgradesStory = new Hashtable();
		weaponUpgradesSurvival = new Hashtable();
		LoadUpgrade(encoded, weaponUpgradesStory);
		LoadUpgrade(encodedSurvival, weaponUpgradesSurvival);
	}

	private void LoadUpgrade(string encodedUpgrades, Hashtable store)
	{

		if(!string.IsNullOrEmpty(encodedUpgrades))
		{
//			BinaryFormatter bf = new BinaryFormatter();
//			MemoryStream ms = new MemoryStream(Convert.FromBase64String(encoded));
//			weaponUpgrades = bf.Deserialize(ms) as Hashtable;
		

			string[] upgrades  = encodedUpgrades.Split('[');
			string encodedUpgrade;
			for(int i=0;i<upgrades.Length;i++)
			{
				encodedUpgrade = upgrades[i];
				//weapon format: ammo power speed targeting active type
				//	targeting, 0=dumb(default),1=smart active, 1=earned apply to story mode, 0=unlocked, apply to survival only
				//	[21101000
				if(!string.IsNullOrEmpty(encodedUpgrade))
				{
				
					////Debug.Log(encodedUpgrade);
					char[] attribs = encodedUpgrade.ToCharArray();
					int ammo = (int) char.GetNumericValue(attribs[0]);
					int power = (int) char.GetNumericValue(attribs[1]);
					int speed = (int) char.GetNumericValue(attribs[2]);
					int targeting  = (int) char.GetNumericValue(attribs[3]);
					int active = (int) char.GetNumericValue(attribs[4]);
					int strength  = (int) char.GetNumericValue(attribs[5]);
					int reflection  = (int) char.GetNumericValue(attribs[6]);

					UpgradeInstance.UpgradeCategory type = (UpgradeInstance.UpgradeCategory) char.GetNumericValue(attribs[7]);

					PlayerUpgrade u = new PlayerUpgrade(ammo, power, speed, targeting, active, strength, reflection, type);
					store.Add(type,u);
				}
			}
		}
	}


	public void SetAsk()
	{
		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_RATEASK, 1);
		rateAsk = true;
	}

	public void SavePreferences()
	{

		if(musicOn)
			PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_MUSIC,0);
		else
			PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_MUSIC,1);

		if(sfxOn)
			PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_SFX,0);
		else
			PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_SFX,1);

		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_DIFFICULTY,difficulty);

		if(usDictionary)
			PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_DICTIONARY, 1);
		else
			PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_DICTIONARY, 0);

		PlayerPrefs.Save();
	}

	//replace with getting from file payload
	private void SaveCurrentLevel()
	{
		//PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_LEVEL, currentLevel);

		if(currentLevel >= highestLevel)
		{
			highestLevel = currentLevel;
			//PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_HIGHEST_LEVEL, highestLevel);

		}

		if(currentLevel >= points)
		{
			points = currentLevel;
			//PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_POINTS, points);
		}

		//PlayerPrefs.Save();
	}

	//replace with getting from file payload
//	public void SavePoints()
//	{
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_POINTS, points);
//	}

	public LevelRecord GetPreviousBest()
	{
		LevelRecord lr = new LevelRecord();
		if(currentLevel < scoreMatrix.Count)
		{
			lr = scoreMatrix[currentLevel];
		}
		return lr;
	}


	public void SaveLevelRecord(LevelRecord.LevelType type, int level, int score, int corrections)
	{

		//LevelRecord lr = scoreMatrix[level-1];
		//if we haven't recorded this level yet, add it
		if(scoreMatrix.Count <= currentLevel)
		{
			LevelRecord lr = new LevelRecord(type, level, score, corrections);
			scoreMatrix.Add (lr);
			totalScore += score;
			scoreSubmitted = false;
		}
		//replace if we improved the score or corrections since last time we played this
		else 
		{
			LevelRecord lr = scoreMatrix[currentLevel];
			if(corrections > lr.corrections)
				lr.corrections = corrections;
			if(lr.score < score)
			{
				totalScore -= lr.score;
				totalScore += score;
				lr.score = score;
			}
			scoreMatrix[currentLevel] = lr;
			scoreSubmitted = false;
		}

		CurrentLevel += 1;
		SaveCurrentLevel();

	}
	//replace with getting from file payload
	public void SaveSurvivalScore(int newScore)
	{
		if(newScore> survivalScore)
		{
			scoreSubmitted = false;
			survivalScore = newScore;
			Save();
			//PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_SURVIVAL_SCORE, survivalScore);
			//PlayerPrefs.Save();
		}
	}
	//replace with getting from file payload
	public void ClearLevelProgress()
	{
		currentLevel = 0;
		highestLevel = 0;
		scoreMatrix.Clear();
		totalScore = 0;
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_LEVEL, currentLevel);
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_HIGHEST_LEVEL, highestLevel);
//		PlayerPrefs.SetString(GameConstants.PREFS_PLAYER_SCORE_MATRIX, 
//		                     "");
//		//Convert.ToBase64String(m.GetBuffer()));
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_SCORE, totalScore);
//		PlayerPrefs.Save();
	}
	//replace with getting from file payload
	public void ClearUpgrades()
	{
		maxSpawns = 3;
		weaponUpgradesStory.Clear();
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_MAX_SPAWNS, maxSpawns);
//		PlayerPrefs.SetString(GameConstants.PREFS_PLAYER_UPGRADES, "");
//		PlayerPrefs.Save();
	}
	//replace with getting from file payload
	private string SaveLevelRecords()
	{
		StringBuilder encoded = new StringBuilder();
		LevelRecord lr;
		for(int i=0;i<scoreMatrix.Count;i++)
		{
			lr = scoreMatrix[i];
			encoded.Append(lr.ToString());
			encoded.Append ("|");
		}

		if(encoded.Length > 0)
		{
			encoded.Remove(encoded.Length-1,1);
		
		}	

		return encoded.ToString();
		
	}

	private string EncodedLevelRecord()
	{
		StringBuilder encoded = new StringBuilder();
		LevelRecord lr;
		for(int i=0;i<scoreMatrix.Count;i++)
		{
			lr = scoreMatrix[i];
			encoded.Append(lr.ToString());
			encoded.Append ("|");
		}
		
		if(encoded.Length > 0)
		{
			encoded.Remove(encoded.Length-1,1);
		}

		return encoded.ToString();
	}

	//replace with getting from file payload
	private string SaveUpgrades()
	{
		IEnumerator e = weaponUpgradesStory.GetEnumerator();
		DictionaryEntry dentry;

		StringBuilder encoded = new StringBuilder();

		
		
		while(e.MoveNext())
		{
			encoded.Append("[");
			dentry = (DictionaryEntry) e.Current;
			PlayerUpgrade u = (PlayerUpgrade) dentry.Value;

			encoded.Append(u.ToString());


		}
		return encoded.ToString();
	}
	//replace with getting from file payload
	private string SaveUpgradesSurvival()
	{
		IEnumerator e = weaponUpgradesSurvival.GetEnumerator();
		DictionaryEntry dentry;
		
		StringBuilder encoded = new StringBuilder();

		while(e.MoveNext())
		{
			encoded.Append("[");
			dentry = (DictionaryEntry) e.Current;
			PlayerUpgrade u = (PlayerUpgrade) dentry.Value;
			
			encoded.Append(u.ToString());
			
		}
		return encoded.ToString();
	}

	//replace with getting from file payload
	public void IncreaseMaxSpawns()
	{
		maxSpawns++;
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_MAX_SPAWNS, maxSpawns);
//		PlayerPrefs.Save();
	}
	//replace with getting from file payload
	public void IncreaseMaxSpawnsSurvival()
	{
		maxSpawnsSurvival++;
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_MAX_SPAWNS_SURVIVAL, maxSpawnsSurvival);
//		PlayerPrefs.Save();
	}


	public WordDifficulty GetWordDifficulty()
	{
		if(difficulty == (short)GameDifficulty.INSANE)
			return WordDifficulty.OBNOXIOUS;
		else
			return WordDifficulty.OBVIOUS;

	}


	public short Difficulty {
		get {
			return difficulty;
		}
		set {
			difficulty = value;
		}
	}

	public bool MusicOn {
		get {
			return musicOn;
		}
		set {
			musicOn = value;
		}
	}

	public bool SfxOn {
		get {
			return sfxOn;
		}
		set {
			sfxOn = value;
		}
	}

	public int GetMaxSpawns()
	{
		return maxSpawns;
	}

	public int Points {
		get {
			return points;
		}
		set {
			points = value;
		}
	}

	public Hashtable WeaponUpgrades {
		get {
			return weaponUpgradesStory;
		}
		set {
			weaponUpgradesStory = value;
		}
	}

	public Hashtable WeaponUpgradesSurvival {
		get {
			return weaponUpgradesSurvival;
		}
		set {
			weaponUpgradesSurvival = value;
		}
	}
	
	public int CurrentLevel {
		get {
			return currentLevel;
		}
		set {
			currentLevel = value;
		}
	}


	public int HighestLevel {
		get {
			return highestLevel;
		}
		set {
			highestLevel = value;
		}
	}


	public List<LevelRecord> ScoreMatrix {
		get {
			return scoreMatrix;
		}
	}

	public int TotalScore {
		get {
			return totalScore;
		}
	}

	public int SurvivalScore {
		get {
			return survivalScore;
		}
	}

	public int MaxSpawnsSurvival {
		get {
			return maxSpawnsSurvival;
		}
	}

	public int BombCounter {
		get {
			return bombCounter;
		}
		set {
			bombCounter = value;
		}
	}

	public bool ScoreSubmitted {
		get {
			return scoreSubmitted;
		}
		set {
			scoreSubmitted = value;
		}
	}
	//replace with getting from file payload
	public void UnlockAll()
	{
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_MAX_SPAWNS_SURVIVAL, 6);
//		PlayerPrefs.SetInt(GameConstants.PREFS_PLAYER_MAX_SPAWNS, 6);
		maxSpawns  = 6;
		maxSpawnsSurvival = 6;
		currentLevel = 0;
		List<LevelRecord> levels = GameMap.GetInstance().GameLevels;
		for(int i=0;i<levels.Count;i++)
		{
			LevelRecord level = levels[i];
			SaveLevelRecord(level.type, level.level, 0, 0);
			currentLevel++;
		}

		SaveLevelRecords();

		highestLevel = levels.Count;
		points = levels.Count;
		LoadUpgrades("[22111001[22101232[22101033[10000004[00001215","[22111001[22101232[22101033[10000004[00001215");

		Save();

	}

	public void UpdateFoamBomb()
	{
		string path = Application.persistentDataPath + "/moldicide";
		
		StringBuilder sb = new StringBuilder();
		
		sb.Append (currentLevel.ToString())
			.Append(GameConstants.SDELIMITER)
				.Append(points.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(highestLevel.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(totalScore.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(survivalScore.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(maxSpawns.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(maxSpawnsSurvival.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(EncodedLevelRecord())
				.Append(GameConstants.SDELIMITER)
				.Append(SaveUpgrades())
				.Append(GameConstants.SDELIMITER)
				.Append(SaveUpgradesSurvival())
				.Append(GameConstants.SDELIMITER)
				.Append(bombCounter.ToString())
				.Append(GameConstants.SDELIMITER);
		
		int d = 67854;
		StreamWriter fout = new StreamWriter(path);
		StringBuilder inSb = new StringBuilder(sb.ToString());
		StringBuilder outSb = new StringBuilder(sb.Length);
		char c;
		for (int i = 0; i < sb.Length; i++)
		{
			c = inSb[i];
			c = (char)(c ^ d);
			outSb.Append(c);
		}
		
		fout.Write(outSb.ToString());
		fout.Close();
	}

	public void Save()
	{
		string path = Application.persistentDataPath + "/moldicide";

		StringBuilder sb = new StringBuilder();

		sb.Append (currentLevel.ToString())
			.Append(GameConstants.SDELIMITER)
				.Append(points.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(highestLevel.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(totalScore.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(survivalScore.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(maxSpawns.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(maxSpawnsSurvival.ToString())
				.Append(GameConstants.SDELIMITER)
				.Append(SaveLevelRecords())
				.Append(GameConstants.SDELIMITER)
				.Append(SaveUpgrades())
				.Append(GameConstants.SDELIMITER)
				.Append(SaveUpgradesSurvival())
				.Append(GameConstants.SDELIMITER)
				.Append(bombCounter.ToString())
				.Append(GameConstants.SDELIMITER);

		int d = 67854;
		StreamWriter fout = new StreamWriter(path);
		StringBuilder inSb = new StringBuilder(sb.ToString());
		StringBuilder outSb = new StringBuilder(sb.Length);
		char c;
		for (int i = 0; i < sb.Length; i++)
		{
			c = inSb[i];
			c = (char)(c ^ d);
			outSb.Append(c);
		}

		fout.Write(outSb.ToString());
		fout.Close();
		
	}
	
	private void Load()
	{
		//Debug.Log(Application.persistentDataPath);

		string path = Application.persistentDataPath + "/moldicide";
		if(File.Exists(path))
		{
			StreamReader streamReader = new StreamReader(path);
			string text = streamReader.ReadToEnd();
			streamReader.Close();

			//d
			int d = 67854;

			StringBuilder inSb = new StringBuilder(text);
			StringBuilder outSb = new StringBuilder(text.Length);
			char c;
			for (int i = 0; i < text.Length; i++)
			{
				c = inSb[i];
				c = (char)(c ^ d);
				outSb.Append(c);
			}

			//Debug.Log("decoded: " + outSb.ToString());

			string[] rawData = outSb.ToString().Split(new string[] { GameConstants.SDELIMITER }, StringSplitOptions.None );
			try{

			currentLevel = int.Parse(rawData[0]);
			points = int.Parse(rawData[1]);
			highestLevel = int.Parse(rawData[2]);
			totalScore = int.Parse(rawData[3]);
			survivalScore = int.Parse(rawData[4]);
			maxSpawns = int.Parse(rawData[5]);
			maxSpawnsSurvival = int.Parse(rawData[6]);
			string encoded = rawData[7];
			LoadScores(encoded);
			string upgrades = rawData[8];
			string supgrades = rawData[9];
			LoadUpgrades(upgrades,supgrades);
			bombCounter = int.Parse(rawData[10]);
			}
			catch(FormatException fe)
			{
				Debug.Log("issue with save file");
				SetInitialValues();
			}

		}
		//no save file exists
		else
		{
			SetInitialValues();
			Save();
		}

	}

	private void SetInitialValues()
	{
		currentLevel = 0;
		points = 0;
		highestLevel = 0;
		totalScore = 0;
		survivalScore = 0;
		maxSpawns = 3;
		maxSpawnsSurvival = 3;
		bombCounter = 0;
		scoreMatrix = new List<LevelRecord>();
		weaponUpgradesStory = new Hashtable();
		weaponUpgradesSurvival = new Hashtable();
	}

	public bool UsDictionary {
		get {
			return usDictionary;
		}
		set {
			usDictionary = value;
		}
	}
}
