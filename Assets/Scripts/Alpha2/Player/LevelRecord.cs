

public struct LevelRecord
{
	public enum LevelType
	{
		STORY=0,
		STANDARD=1,
		BOSS=2,
		UPGRADE=3
	}

	public int level;
	public int score;
	public int corrections; //0=flawless+insane, 1=flawless, 2=insane
	public LevelType type;
	public int index; //absolute index in the game map

	public LevelRecord(LevelType type, int level, int score, int corrections, int index)
	{
		this.level = level;
		this.score = score;
		this.corrections = corrections;
		this.type = type;
		this.index = index;
	}

	public LevelRecord(LevelType type, int level, int score, int corrections)
	{
		this.level = level;
		this.score = score;
		this.corrections = corrections;
		this.type = type;
		this.index = -1;

	}

	public LevelRecord(int level, int score, int corrections, int index)
	{
		this.level = level;
		this.score = score;
		this.corrections = corrections;
		this.index = index;
		this.type = LevelType.STANDARD;
	
	}

	public override string ToString ()
	{
		return string.Format ("{0},{1},{2},{3}",(int)type,level,score,corrections);
	}

	public static LevelRecord Decode(string encoded)
	{
		string[] elements = encoded.Split(new char[] {','});
		LevelType type = (LevelType) System.Convert.ToInt16(elements[0]);
		int level = System.Convert.ToInt16(elements[1]);
		int score = System.Convert.ToInt32(elements[2]);
		int corrections = System.Convert.ToInt16(elements[3]);

		return new LevelRecord(type,level,score,corrections);

	}
}


