﻿using UnityEngine;
using System.Collections;

public struct PlayerUpgrade 
{
	public int ammo;
	public int power;
	public int speed;
	public int targeting;
	public int active;
	public int strength;
	public int reflection;
	public UpgradeInstance.UpgradeCategory type;

	public PlayerUpgrade(int active)
	{
		this.ammo = 0;
		this.power = 0;
		this.speed = 0;
		this.targeting = 0;
		this.strength = 0;
		this.reflection = 0;
		this.type = UpgradeInstance.UpgradeCategory.Barrier;
		this.active = -1;
	}

	public PlayerUpgrade(int ammo, int power, int speed, int targeting, int active, int strength, int reflection,  UpgradeInstance.UpgradeCategory type)
	{
		this.ammo = ammo;
		this.power = power;
		this.speed = speed;
		this.targeting = targeting;
		this.strength = strength;
		this.reflection = reflection;
		this.type = type;
		this.active = active;
	}

	public override string ToString ()
	{
		return string.Format ("{0}{1}{2}{3}{4}{5}{6}{7}",ammo,power,speed,targeting,active,strength,reflection,(int)type);
	}




}