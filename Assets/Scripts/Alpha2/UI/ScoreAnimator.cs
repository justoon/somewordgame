﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;


public class ScoreAnimator : MonoBehaviour {

	public bool down;
	public float totalMove = .3f;
	public float step = .008f;
	float startY = 0;

	public float startTime;
	float FadeStartTime;
	float FadeTime;
	
	public float playTime;
	public float FadeSpeed;
	
	public Color TimerColor = Color.yellow;
	
	
	bool move = false;
	public bool fadeIn = false;
	public bool fadeOut = false;	
	private Text display;

	// Use this for initialization
	void Start () {
		startY = transform.position.y;


		if(FadeSpeed == 0)
		{
			FadeSpeed = .2f;
		}
		Color x = new Color(TimerColor.r,TimerColor.g,TimerColor.b,0);
		display = GameObject.Find("dtxt_timeBonus").GetComponent<Text>();

		display.color = x;

	}


	public void SetText(string text)
	{
		display.text = text;
	}

	public void Show()
	{
		fadeIn = true;
	}

	public void Reset()
	{
		transform.position = new Vector2(transform.position.x,startY);
		Color x = new Color(TimerColor.r,TimerColor.g,TimerColor.b,0);
		display.color = x;
		fadeIn = false;
		fadeOut = false;
	}
	// Update is called once per frame
	void Update () 
	{
		if(fadeIn)
		{
			FadeIn();
		}
		
		else if(fadeOut)
		{
			FadeOut();
		}
		else if(move)
		{
			transform.position = new Vector2(transform.position.x, transform.position.y+step);

			if(down)
			{
				if(transform.position.y <= (startY+totalMove))
				{
					fadeOut = true;
					move = false;
				}
			}
			else
			{
				if(transform.position.y >= startY+totalMove)
				{
					fadeOut = true;
					move = false;
				}
			}
		}
	
	}


	
	void FadeIn()
	{
		//FadeTime = (Time.time - FadeStartTime) * FadeSpeed;
		FadeTime = Time.time/FadeSpeed;
		TimerColor.a = Mathf.SmoothStep(0, 1, FadeTime );
		display.color = TimerColor;
		if(TimerColor.a == 1)
		{
			fadeIn = false;
			move = true;
			//FadeTime = 0;
		}
	}
	
	void FadeOut()
	{
		//FadeTime = (Time.time - FadeStartTime) * FadeSpeed;
		FadeTime = Time.time/FadeSpeed;
		TimerColor.a = Mathf.SmoothStep(1, 0, FadeTime );
		display.color = TimerColor;
		if(TimerColor.a == 0)
		{
			//Destroy(this.gameObject);
			Reset();
		}
	}
}
