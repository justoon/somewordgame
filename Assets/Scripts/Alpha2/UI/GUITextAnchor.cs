﻿using UnityEngine;
using System.Collections;

public class GUITextAnchor : MonoBehaviour {

	public float offset = -.03f;
	public Transform target;
	
	//GameObject levelSelect;
	
	// Use this for initialization
	void Start () {
		transform.position = new Vector2(Camera.main.WorldToViewportPoint(target.position).x+offset, transform.position.y);

	}

}
