﻿using UnityEngine;
using System.Collections;

public class TouchManager : MonoBehaviour {

	public float x;
	public float y;

	public bool acceptInput;
	public bool useMouse;


	/// <summary>
	/// Touch Manager for single tap/drag
	/// </summary>
	private int currentFinger = -1;

	private Vector2 touchPoint;

	private TouchZone currentZone;


	void Start()
	{
		touchPoint = Vector2.zero;
		//acceptInput = true;
		//useMouse = false;
	}

	private void EvaluateEscape()
	{
		int level = Application.loadedLevel;
		if(level <=1)
		{
			Application.Quit();
		}
		else if(level == 4)
		{
			if(Camera.main.transform.position == GameConstants.CAMERA_SETTING_POSITION)
			{
				Camera.main.transform.position = GameConstants.CAMERA_GAME_POSITION;
			}
			else
			{
				Camera.main.transform.position = GameConstants.CAMERA_SETTING_POSITION;
			}
		}
		else if((level > 1 && level <=5) || Application.loadedLevelName.Contains("Story") || Application.loadedLevelName.Contains("Upgrade") )
		{
			AudioDirector.Instance.StopGameTracks();
			AudioDirector.Instance.SetTrackToMain();
			Application.LoadLevel(GameConstants.LEVEL_SELECT);
		}
		else if(GameMap.GetInstance().GetThisLevel().level == 1)
		{
			return;
		}
		else
		{
			if(!GameController.GetInstance().IsDone())
			{
				if(Time.timeScale == 0)
				{
					Camera.main.transform.position = GameConstants.CAMERA_GAME_POSITION;
					GameController.GetInstance().ResumeGame();
				}
				else
				{
					Camera.main.transform.position = GameConstants.CAMERA_SETTING_POSITION;
					GameController.GetInstance().PauseGame();
				}
			}
			else
			{
				if(Time.timeScale == 0)
				{
					if(GUIManager.GetInstance().GetLevelOutcome() == 1)
						Camera.main.transform.position = GameConstants.CAMERA_SUCCESS_POSITION;
					else if (GUIManager.GetInstance().GetLevelOutcome() == 2)
						Camera.main.transform.position = GameConstants.CAMERA_FAIL_POSITION;

					GameController.GetInstance().ResumeGame();
				}
				else
				{
					Camera.main.transform.position = GameConstants.CAMERA_SETTING_POSITION;
					GameController.GetInstance().PauseGame();
				}
			}
		}
	}

	private void TryDispatchKey(char c)
	{
		int level = Application.loadedLevel;
	    if((level > 5) && !Application.loadedLevelName.Contains("Story") && !Application.loadedLevelName.Contains("Upgrade") )
		{
			LetterGrid grid = GUIManager.GetInstance().GetLetterGrid();
			grid.ActivateTileByLetter(c);
		}
	}

	private void TryDispatchEnter()
	{
		int level = Application.loadedLevel;
		if((level > 5) && !Application.loadedLevelName.Contains("Story") && !Application.loadedLevelName.Contains("Upgrade") )
		{
			WeaponClickAction wca = GUIManager.GetInstance().GetWeaponButton();
			wca.Trigger();

		}
	}

	

	private void TryDispatchBack()
	{
		int level = Application.loadedLevel;
		if((level > 5) && !Application.loadedLevelName.Contains("Story") && !Application.loadedLevelName.Contains("Upgrade") )
		{
			LetterGrid grid = GUIManager.GetInstance().GetLetterGrid();
			grid.RemoveLastTile();
		}
	}

	protected virtual void Update () {


		if(Input.GetKeyDown(KeyCode.Escape))
		{
			EvaluateEscape();
			return;
		}
		else if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
		{
			TryDispatchEnter();
		}
		else if (Input.GetKeyDown(KeyCode.Backspace))
		{
			TryDispatchBack();
		}
		
		else if(!string.IsNullOrEmpty(Input.inputString))
		{
			string input = Input.inputString.ToUpper();
			char c = input[0];
			TryDispatchKey(c);
			//Debug.Log("Input: " + c);
		}

		//check for touches
		if(Input.touchCount > 0)
		{
			//Touch currentTouch = Input.GetTouch(0);
			Touch currentTouch = Input.touches[0];
			int fingerID = currentTouch.fingerId;
			if(currentFinger == -1)
			{
				currentFinger = fingerID;
			}
			
			if( fingerID != currentFinger)
			{
				//not sure if this is an issue since we just want the first finger that hits the screen
				//Debug.LogWarning("I was given a different finger for touches[0]!");
				
			}
			EvaluateTouch(currentTouch);
		}

		#if UNITY_EDITOR || UNITY_STANDALONE
		if(useMouse)
		{
			if(Input.GetMouseButtonDown(0))
			{
				DoOnMouseDown();
			}
			else if(Input.GetMouseButtonUp(0))
			{
				DoOnMouseUp();
			}
			else if(Input.GetMouseButton(0))
			{
				DoOnMouseDrag();
			}
		}
#endif

	}


	protected virtual void EvaluateTouch(Touch currentTouch)
	{
		touchPoint = currentTouch.position;
		switch(currentTouch.phase)
		{
		case TouchPhase.Began:


			//Using Colliders to define specific zone controllers
			int mask = 1 << LayerMask.NameToLayer("GUI");
			RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touchPoint),Vector2.zero,0,mask);
			if(hit)
			{
				////Debug.Log("hit: " + hit.collider.gameObject.name);
				currentZone = hit.collider.gameObject.GetComponent<TouchZone>();

				if(currentZone)
				{

//					if(hit.collider.gameObject.name.Equals("x_bt_options") ||
//					   hit.collider.gameObject.name.Equals("NextLevelButton") ||
//					   hit.collider.gameObject.tag.Equals("Settings") ||
//					   acceptInput)
					if(acceptInput)
					{
						currentZone.TouchStart(currentTouch);
					}


				}
				//dispatch the touch to the handler

			}
			else
			{
				//Debug.Log("hit nothing");
			}


			//Circe collider:
			/*int mask = 1 << LayerMask.NameToLayer("GUI");
		
			Collider2D hit = Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(touchPoint), .075f);
			if(hit)
			{
				//Debug.Log("hit: " + hit.gameObject.name);
			}
			else
			{
				//Debug.Log("hit nothing");
			}*/


			break;
		case TouchPhase.Ended:
			if(currentZone)
			{
				currentZone.TouchEnd(currentTouch);
				currentZone = null;
			}
			break;
		case TouchPhase.Moved:
			if(currentZone)
			{
				currentZone.TouchMoved(currentTouch);
			}
			break;
		default:break;
		}
	}

	#if UNITY_EDITOR || UNITY_STANDALONE
	void DoOnMouseDown()
		
	{
		Vector2 hitPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		int mask = 1 << LayerMask.NameToLayer("GUI");
		RaycastHit2D hit = Physics2D.Raycast(hitPoint,Vector2.zero,0,mask);
		if(hit)
		{
			////Debug.Log("hit: " + hit.collider.gameObject.name);
			currentZone = hit.collider.gameObject.GetComponent<TouchZone>();
			if(currentZone)
			{
				//quick hack to only allow the pause/option button to accept input no matter what
				//not sure if needed as camera moves away from game screen on settings or next level
//				if(hit.collider.gameObject.name.Equals("x_bt_options") ||
//				   hit.collider.gameObject.name.Equals("NextLevelButton") ||
//				   hit.collider.gameObject.tag.Equals("Settings") ||
//				   acceptInput)
				if(acceptInput)
				{
					currentZone.FakeTouchStart(hitPoint);
				}

			}
		}
	}
		#endif

	#if UNITY_EDITOR || UNITY_STANDALONE
		void DoOnMouseDrag()
			
		{
			Vector2 hitPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			if(currentZone)
			{
			currentZone.FakeTouchMoved(hitPoint);
			}
	}
			#endif

	#if UNITY_EDITOR || UNITY_STANDALONE
	void DoOnMouseUp()
		
	{
		Vector2 hitPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		if(currentZone)
		{
			currentZone.FakeTouchEnd(hitPoint);
			currentZone = null;
		}
	}
	#endif
	
	/*
	void OnGUI()
	{
		//GUI.Box(new Rect(x,y, 20,20), "This is a title");
		if(touchPoint != Vector2.zero)
		{
			////Debug.Log(string.Format("drawing box @ {0}", touchPoint));

			GUI.Box(new Rect(touchPoint.x-10,  Screen.height - touchPoint.y-10, 20,20), "This is a title");


		}
	}
*/

}
