using UnityEngine;
using System.Collections;

public class BaseZoneController : TouchZone {

	protected GameObject[] posTracker;
	protected LetterGrid grid;

	protected virtual void Start()
	{
		posTracker = GameObject.FindGameObjectsWithTag("LetterTile");
		grid = GameObject.FindGameObjectWithTag("LetterGrid").GetComponent<LetterGrid>();

	}



	public virtual GameObject CheckTouchPoint(Vector3 point, TileTouchState targetState)
	{
		//Debug.Log("Checking touch points " + point);

		//float xpos = point.x;
		float minDistance = .175f;
		GameObject closestHit = null;
		for(int i=0;i<posTracker.Length;i++)
		{
			GameObject o = posTracker[i];
			LetterTile tile = o.GetComponent<LetterTile>();
			if(tile.IsPlayable() && tile.touchState == targetState)
			{
				Bounds b =  tile.background.GetComponent<Renderer>().bounds;

				float dist = b.SqrDistance(point);
				
				//Debug.Log(string.Format("checking {0} bounds: {1}", o.name, b));
				
				////Debug.Log(string.Format("Checking {0} with xmin {1} and xmax {2} vs {3}", tile.name, tile.xMin, tile.xMax, xpos));
				//if(xpos >= tile.xMin && xpos <= tile.xMax)
				if(b.Contains(point))
				{
					//Debug.Log("returning " + o);
					
					return o;
				}
				else if(dist < minDistance)
				{
					//Debug.Log(string.Format("this has the smallest distance {0} {1}", o,dist));
					minDistance = dist;
					closestHit = o;
				}
			}
		}
		
		if(closestHit != null)
			return closestHit;
		
		return null;
	}
}
