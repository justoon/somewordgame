﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CurrentLevelItem : LevelSelectItem {

	public SpriteRenderer specialBadge;
	public Text levelText;
}
