﻿using UnityEngine;
using System.Collections;

public class ClearProgressButton : MonoBehaviour {

	public void DoAction ()
	{
		AudioDirector.Instance.PlayGUIClip(0);
		PlayerProfile profile = PlayerProfile.GetInstance();
		Score.score = 0;
		//clear scores, reset highest and current level to 0
		profile.ClearLevelProgress();
		//reset story upgrades
		profile.ClearUpgrades();
		//reload the scene
		profile.Save();
		GameController.GetInstance().LoadLevel(GameConstants.LEVEL_MAIN);

	}
}
