﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectItem : OneClickButton {

	public int level;
	public Image container;


	public override void DoAction ()
	{
		base.DoAction();
		GameController gameController = GameObject.Find("GameLogic").GetComponent<GameController>();
		//string scene = GameMap.GetInstance().GetSceneName(level);
		////Debug.Log("LevelSelect Load: " + scene);
		PlayerProfile.GetInstance().CurrentLevel = level;
		GameMap.GetInstance().SetIndex(level);
		//gameController.LoadNextStoryLevel();
	}

}
