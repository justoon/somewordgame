﻿using UnityEngine;
using System.Collections;

public class ToggleButton : TouchableButton {

	public bool on;
	public bool reset;

	private SpriteRenderer onRenderer;
	private SpriteRenderer offRenderer;

	public Sprite onButtonOn;
	public Sprite onButtonOff;
	public Sprite offButtonOn;
	public Sprite offButtonOff;

	protected override void Start ()
	{
		base.acceptInput = true;
		onRenderer = transform.Find("onButton").GetComponent<SpriteRenderer>();
		offRenderer = transform.Find("offButton").GetComponent<SpriteRenderer>();
		if(on)
		{
			onRenderer.sprite = onButtonOn;
			offRenderer.sprite = offButtonOff;
		}
		else
		{
			onRenderer.sprite = onButtonOff;
			offRenderer.sprite = offButtonOn;
		}
		reset = on;

	}
	protected override void FireEvent ()
	{

		if(on)
		{
			onRenderer.sprite = onButtonOff;
			offRenderer.sprite = offButtonOn;
		}
		else
		{
			onRenderer.sprite = onButtonOn;
			offRenderer.sprite = offButtonOff;
		}
		on = !on;
	}

	public virtual void Reset()
	{
		on = reset;
		if(!on)
		{
			onRenderer.sprite = onButtonOff;
			offRenderer.sprite = offButtonOn;
		}
		else
		{
			onRenderer.sprite = onButtonOn;
			offRenderer.sprite = offButtonOff;
		}
	}
	

}
