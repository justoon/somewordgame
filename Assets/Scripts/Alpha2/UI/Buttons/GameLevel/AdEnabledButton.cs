﻿using UnityEngine;
using System.Collections;

public abstract class AdEnabledButton : OneClickButton {

	public bool interstitialAd;
	protected abstract void DoLoadingAction();
	

	protected void AdCompleted()
	{
		AdManager.OnAdComplete -= AdCompleted;
		DoLoadingAction();	

	}


	public override void DoAction ()
	{
		base.DoAction();
		if(GameController.Instance.useAds)
		{
			if((interstitialAd && !AdManager.Instance.interstitialAdReady) || 
			   (!interstitialAd && !AdManager.Instance.videoAdReady))
			{
				Debug.LogWarning("Ad Manager not ready - skipping ad");
				DoLoadingAction();
			}
			else
			{
				AdManager.OnAdComplete += AdCompleted;
				if(interstitialAd)
					AdManager.Instance.ShowInterstitialAd();
				else
					AdManager.Instance.ShowVideoAd();
			}
		}
		else
		{
			DoLoadingAction();
		}
		
	}


}
