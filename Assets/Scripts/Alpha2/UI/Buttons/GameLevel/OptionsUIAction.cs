using UnityEngine;
using System.Collections;

public class OptionsUIAction : MonoBehaviour {

	public bool pauseGame = true;
	public bool reloadLevel = true;
	public SettingsConfirmButton confirmAction;
	public SettingsCancelButton cancelAction;
	public QuitLevel quitAction;

	public void DoAction ()
	{
		Camera.main.transform.position = GameConstants.CAMERA_SETTING_POSITION;//new Vector3(-11.67f, cameraPos.y,cameraPos.z);
		if(pauseGame)
			GameController.GetInstance().PauseGame();
	}


}
