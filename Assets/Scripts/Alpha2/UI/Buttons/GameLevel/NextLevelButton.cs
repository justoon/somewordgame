using UnityEngine;
using System.Collections;

public class NextLevelButton : AdEnabledButton {

	public SimpleAnimator askPrompt;



	protected override void DoLoadingAction ()
	{		
		//base.DoAction();
		
		if((GameMap.GetInstance().GetThisLevel().level % 9 == 0)
		   && !PlayerProfile.GetInstance().rateAsk)
		{
			askPrompt.MoveToActive();
		}
		else
		{
			//GameController.GetInstance().LoadNextStoryLevel();
		}
	}
}
