using UnityEngine;
using System.Collections;

public class WeaponClickAction : EventButton {

	public SpriteRenderer disable;

	public bool tapEnabled = true;

	public bool trackLetters = true;

	public string weaponPrefab;
	public int wordLength;
	public bool fullLength;
	public string currentWord;

	protected MoldicideEvent eventName;
	protected SpriteRenderer currentSprite;

	void Awake()
	{
		currentSprite = GetComponent<SpriteRenderer>();
	}
	protected override void Start ()
	{
		base.Start();
		acceptInput = false;
	}
	public void SetPrefab(string weaponPrefab)
	{
		this.weaponPrefab = weaponPrefab;
	}

	public void SetWordLength(int wordLength)
	{
		this.wordLength = wordLength;
	}


	protected override void DoAction ()
	{
		//update the total number of words of this length we have used

		//GUIManager.GetInstance().UpdateAlerts("");

		if(!string.IsNullOrEmpty(weaponPrefab))
		{
			//if valid word, update the letter tracking
			WordManager.GetInstance().UpdateTracking(trackLetters,fullLength);
			eventName = MoldicideEvent.WEAPON_SPAWN;

		}
		//if we have no prefab assigned, assume we've hit the cancel button
		else
		{
			eventName=MoldicideEvent.CANCEL_WORD;
			GUIManager.GetInstance().HideTrigger(); //reset the weapon trigger
			GUIManager.GetInstance().ResetLetterGrid(); //reset the tiles
		}

		base.DoAction ();

		//LetterGrid letterGrid = GUIManager.GetInstance().GetLetterGrid();
		//letterGrid.ResetLetterTiles();


		acceptInput = false; //reset the flag 
	}

	//event action executes and broadcasts data to other listeners
	protected override int GetValue ()
	{
		return wordLength;
	}
	protected override MoldicideEvent GetName ()
	{
		return eventName;
	}

	protected override object GetAction ()
	{
		return this;
	}

	public void Reset()
	{
		currentSprite.sprite = null; //weapons[GameConstants.SPRITE_WEAPON_GUI_BLANK];
	}

	public void Disable()
	{
		tapEnabled = false;
		acceptInput = false;
		disable.sprite = SpriteCache.GetInstance().Triggers[20];
	}

	public void Enable()
	{
		tapEnabled = true;
		acceptInput = true;
		disable.sprite = null;
	}

	public bool IsVisible()
	{
		if(currentSprite.sprite==null)
			return false;

		return !currentSprite.sprite.name.Equals("ui_trigger_12");
	}

	public void Trigger()
	{
		DoAction();
	}


}
