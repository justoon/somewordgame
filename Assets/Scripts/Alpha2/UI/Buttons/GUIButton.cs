﻿using UnityEngine;
using System.Collections;

public class GUIButton : TouchableButton {

	//standard implementation of a guibutton is to show a highlighted sprite on tap
	public Sprite hitSprite;
	public Sprite originalSprite;

	protected override void Start ()
	{
		base.Start ();
		originalSprite = spriteRenderer.sprite;
	}

	public override void FakeTouchStart (Vector2 v)
	{
		if(acceptInput && hitSprite)
			spriteRenderer.sprite = hitSprite;
	}

	public override void TouchStart (Touch t)
	{
		if(acceptInput && hitSprite)
			spriteRenderer.sprite = hitSprite;
	}

	public override void FakeTouchEnd (Vector2 v)
	{
		if(acceptInput)
		{
			spriteRenderer.sprite = originalSprite;
			DoAction();
		}
	}
	
	public override void TouchEnd (Touch t)
	{
		if(acceptInput)
		{
			spriteRenderer.sprite = originalSprite;
			DoAction();
		}
	}

	protected virtual void DoAction()
	{
	}
}
