using UnityEngine;
using System.Collections;

public class EventButton : GUIButton {

	//event button dispatches an event for multiple listeners
	protected override void DoAction ()
	{
		if(acceptInput)
		{
			GameEvent ge = new GameEvent(GetName(), GetValue(), GetID(), GetAction());
			GUIEventManager.DispatchEvent(ge);
		}
	}

	protected virtual int GetID()
	{
		return 0;
	}

	protected virtual int GetValue()
	{
		return 0;
	}

	protected virtual MoldicideEvent GetName()
	{
		return MoldicideEvent.NO_EVENT;
	}

	protected virtual object GetAction()
	{
		return null;
	}
}
