﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct UpgradeInstance  {

	public enum UpgradeCategory
	{
		Cannon=1,
		Flamer=2,
		Rocket=3,
		Laser=4,
		Barrier=5
		
	}
	
	public enum UpgradeBoost
	{
		Ammo=0,
		Power=1,
		Speed=2,
		Targeting=3,
		Strength=4,  //for weapons, strength = longer dot
		Reflection=5 //for weapons reflection = bigger aoe
	}

	public UpgradeCategory category;
	public UpgradeBoost boost;
	public int value;
	

}
