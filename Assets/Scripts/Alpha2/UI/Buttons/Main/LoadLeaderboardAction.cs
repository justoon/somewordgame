﻿using UnityEngine;
using System.Collections;

public class LoadLeaderboardAction : MonoBehaviour {

	public static bool GOOGLEPLAY= true;

	public  void ShowLeaderboard ()
	{
		AudioDirector.Instance.PlayGUIClip(0);
		#if UNITY_EDITOR
		GameController.GetInstance().LoadLevel(GameConstants.LEVEL_LEADERBOARD_AMAZON);
		#elif UNITY_ANDROID
		if(GOOGLEPLAY)
			GameController.GetInstance().LoadLevel(GameConstants.LEVEL_LEADERBOARD_ANDROID);
		else
			GameController.GetInstance().LoadLevel(GameConstants.LEVEL_LEADERBOARD_AMAZON);
		#else
		GameController.GetInstance().LoadLevel(GameConstants.LEVEL_LEADERBOARD);
		#endif


	}
}
