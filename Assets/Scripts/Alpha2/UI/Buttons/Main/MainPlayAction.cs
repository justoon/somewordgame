﻿using UnityEngine;
using System.Collections;

public class MainPlayAction : GUIButton {

	protected override void DoAction ()
	{
		AudioDirector.Instance.PlayGUIClip(2);
		if(PlayerProfile.GetInstance().CurrentLevel > 1)
		{
			GameController.GetInstance().LoadLevel("LevelSelect");
		}
		else if(PlayerProfile.GetInstance().CurrentLevel > 0)
		{
			GameMap.GetInstance().SetIndex(2);
			GameMap.GetInstance().currentLevel = "Level1";
			GUIManager.GetInstance().LevelFadeOut();
			//GameController.GetInstance().LoadLevel("Level1");
		}
		else
		{
			GameMap.GetInstance().SetIndex(1);
			GameController.GetInstance().LoadLevel("StoryTemplate");
		}
		
		
	}
}
