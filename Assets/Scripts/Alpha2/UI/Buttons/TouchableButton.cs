﻿using UnityEngine;
using System.Collections;

public class TouchableButton : TouchZone {

	public bool acceptInput;

	protected SpriteRenderer spriteRenderer;

	public override void TouchStart (Touch t)
	{
		if(acceptInput)
			FireEvent();
	}
	
	public override void FakeTouchStart (Vector2 v)
	{
		if(acceptInput)
			FireEvent();
	}

	protected virtual void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		acceptInput = true;
	}


	protected virtual void FireEvent()
	{}

}
