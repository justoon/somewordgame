using UnityEngine;
using System.Collections;

public class WordZoneController : BaseZoneController {


	private float touchStartTime;

	private float tapThreshold = .75f;
	//private float dragThreshold = .1f;

	private LetterTile currentTile;
	private int startZone;

	private bool dragged;
	private float offsetY;
	private float offsetX;
	//private Vector3 offsetPosition;
	Vector3 screenPoint;
	private Vector2 lastPosition;





	public override void TouchStart(Touch currentTouch)
	{
		Vector3 touchPosition = Camera.main.ScreenToWorldPoint(currentTouch.position);
		touchPosition = new Vector3(touchPosition.x, touchPosition.y, 0);
		
		GameObject touched = CheckTouchPoint(touchPosition, TileTouchState.INWORD);
		if(touched)
		{

			currentTile = touched.GetComponent<LetterTile>();
			currentTile.ShowActive();

			touchStartTime = Time.time;
			dragged = false;
			Vector2 offsetPosition = currentTouch.position;
			

			screenPoint = Camera.main.WorldToScreenPoint(touched.transform.position);
			offsetY = offsetPosition.y - screenPoint.y;
			offsetX = offsetPosition.x - screenPoint.x;
			startZone = currentTile.dropZoneId;

			//currentTile.MoveToWord();
			//currentTile.SetActive(currentTouch.position);
		}
		else
		{
			//Debug.Log(string.Format("touch detected, but nothing hit @ position {0}", touchPosition));
		}
	}

	public override void TouchEnd (Touch t)
	{
		if(currentTile)
		{
			float touchDuration = Time.time - touchStartTime;
			//if((touchDuration < tapThreshold && !dragged) || (dragged && touchDuration < dragThreshold))
			if((touchDuration < tapThreshold && !dragged))
			{
				//Debug.Log(string.Format("current tile touchDuration: {0}, marking as tapped", touchDuration));
				currentTile.MoveToSelection();
				//ScoreManager.GetInstance().GuessCorrected();

				GameEvent ge = new GameEvent(MoldicideEvent.GUESS_CORRECTION);
			
				//implemented by subclasses
				GUIEventManager.DispatchEvent(ge);
			}
			else
			{

				if(grid.InSelectionZone(currentTile.transform.position))
				{
					currentTile.MoveToSelection();
				}
				else
				{
					currentTile.StopActive();
					currentTile.animating = true;
				}
				//this is not working properly
				if(currentTile.dropZoneId != startZone)
				{
					//ScoreManager.GetInstance().GuessCorrected();

					GameEvent ge = new GameEvent(MoldicideEvent.GUESS_CORRECTION);
					//implemented by subclasses
					GUIEventManager.DispatchEvent(ge);
				}

			}
			currentTile = null;
		}
	}

	public override void TouchMoved (Touch t)
	{
		//consider the tile tapped if we haven't moved that much
		//if(currentTile && !Mathf.Approximately(lastPosition.x,v.x) && !Mathf.Approximately(lastPosition.y,v.y))
		//lastPosition = Vector2.zero; //reset this so subsequent dragging doesn't compare the original click position
		if(currentTile)
		{
			dragged = true;
			//currentTile.touchState = TileTouchState.DRAGGING;

//			Vector3 touchPosition = Camera.main.ScreenToWorldPoint(t.position);
//			Vector2 offsetPosition = new Vector2(touchPosition.x + currentTile.transform.position.x, touchPosition.y + currentTile.transform.position.y);
//
//			currentTile.transform.position = offsetPosition;
			Vector2 offsetPosition = t.position;

			Vector3 currentScreenPt = new Vector3(offsetPosition.x - offsetX, offsetPosition.y - offsetY,screenPoint.z);
			Vector3 currentPos = Camera.main.ScreenToWorldPoint(currentScreenPt);
			currentTile.gameObject.transform.position = currentPos;
			grid.HitOccupiedZone(currentTile);
		}
	}

	public override void FakeTouchStart (Vector2 v)
	{
		GameObject touched = CheckTouchPoint(v, TileTouchState.INWORD);
		if(touched)
		{
			lastPosition = v;
			currentTile = touched.GetComponent<LetterTile>();
			currentTile.ShowActive();
			
			touchStartTime = Time.time;
			dragged = false;
			Vector2 offsetPosition = Camera.main.WorldToScreenPoint(v);//currentTouch.position;
			
			
			screenPoint = Camera.main.WorldToScreenPoint(touched.transform.position);
			offsetY = offsetPosition.y - screenPoint.y;
			offsetX = offsetPosition.x - screenPoint.x;

			
			//currentTile.MoveToWord();
			//currentTile.SetActive(currentTouch.position);
		}
		else
		{
			//Debug.Log(string.Format("touch detected, but nothing hit @ position {0}", v));
		}
	}

	public override void FakeTouchEnd (Vector2 v)
	{
		if(currentTile)
		{
			float touchDuration = Time.time - touchStartTime;
			if((touchDuration < tapThreshold) && !dragged)
			{
				//Debug.Log(string.Format("current tile touchDuration: {0}, marking as tapped", v));
				currentTile.MoveToSelection();
				//ScoreManager.GetInstance().GuessCorrected();
				GameEvent ge = new GameEvent(MoldicideEvent.GUESS_CORRECTION);
				//implemented by subclasses
				GUIEventManager.DispatchEvent(ge);
			}
			else
			{
				if(grid.InSelectionZone(currentTile.transform.position))
				{
					currentTile.MoveToSelection();
				}
				else
				{
					currentTile.StopActive();
					currentTile.animating = true;
				}
				//currentTile.StopActive();
				if(currentTile.dropZoneId != startZone)
				{
				//	ScoreManager.GetInstance().GuessCorrected();
					GameEvent ge = new GameEvent(MoldicideEvent.GUESS_CORRECTION);
					//implemented by subclasses
					GUIEventManager.DispatchEvent(ge);
				}
				
			}
			currentTile = null;
		}
	}

	public override void FakeTouchMoved (Vector2 v)
	{
		//modify: see if we've moved more than just a tiny amount to count as a drag, Mathf.approximately is too small a number


		if(currentTile && !Mathf.Approximately(lastPosition.x,v.x) && !Mathf.Approximately(lastPosition.y,v.y))
		{
			dragged = true;
			lastPosition = Vector2.zero; //reset this so subsequent dragging doesn't compare the original click position
			Vector2 offsetPosition = Camera.main.WorldToScreenPoint(v);

			Vector3 currentScreenPt = new Vector3(offsetPosition.x - offsetX, offsetPosition.y - offsetY,screenPoint.z);
			Vector3 currentPos = Camera.main.ScreenToWorldPoint(currentScreenPt);

			currentTile.gameObject.transform.position = currentPos;
			grid.HitOccupiedZone(currentTile);
		}
	}
	

}
