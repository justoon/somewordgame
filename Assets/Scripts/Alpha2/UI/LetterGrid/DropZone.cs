﻿using UnityEngine;
using System.Collections;

public class DropZone : MonoBehaviour {

	//public static float bounds = .395f;
	//public static float bounds = .180f;
	public static float bounds = .3f;
	public float xMin;
	public float xMax;
	public LetterTile currentResident;


	void Start()
	{
		float currentX = transform.position.x;
		xMin = currentX - bounds;
		xMax = currentX + bounds;
	}
}
