using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class LetterGrid : MonoBehaviour {

	//check if this word has been blocked by the boss
	public bool checkBlock;
	public bool phasedLevel;

	public bool bossLevel;
	
	public int minLength;
	
	private bool locked;
	//private bool tutorialLevel;
	
	//private static Vector2 snapTo = new Vector2(0f, -1.5f);
	
	//this is the dynamic list of letter tiles in the word state
	private List<LetterTile> currentGrid;


	
	private int[][] patterns;
	private GameObject[] dropZones;
	
	private Vector2 moveDirection;
	
	private bool ignoreShift;
	
	private bool lettersShifted;

	//these are all the tiles in the scene, 
	private LetterTile[] tiles;
	private SpriteRenderer[] tileSprites;
	protected Bounds colliderBounds;
	
	
	protected void Awake()
	{
		currentGrid = new List<LetterTile>(9);
		tiles = new LetterTile[transform.childCount];
		tileSprites = new SpriteRenderer[transform.childCount];
		for(int i=0;i<transform.childCount;i++)
		{
			Transform t = transform.GetChild(i);
			tiles[i] = t.GetComponent<LetterTile>();
			tileSprites[i] = t.GetComponent<SpriteRenderer>();
		}

		colliderBounds = GetComponent<Collider2D>().bounds;
		
		patterns = new int[9][];
		patterns[0] = new int[] { 5 };
		patterns[1] = new int[] { 4,5 };
		patterns[2] = new int[] { 4,5,6};
		patterns[3] = new int[] { 3,4,5,6};
		patterns[4] = new int[] { 3,4,5,6,7 };
		patterns[5] = new int[] { 2,3,4,5,6,7 };
		patterns[6] = new int[] { 2,3,4,5,6,7,8 };
		patterns[7] = new int[] { 1,2,3,4,5,6,7,8 };
		patterns[8] = new int[] { 1,2,3,4,5,6,7,8,9 };
		
		dropZones = new GameObject[17];
		
		StringBuilder dzName = new StringBuilder();
		
		
		for(int i=0;i<dropZones.Length;i++)
		{
			if(i > 8)
			{
				dropZones[i] = GameObject.Find(dzName.Append("ZoneEven")
				                               .Append (i-8).ToString());
			}
			else
			{
				dropZones[i] = GameObject.Find(dzName.Append("Zone")
				                               .Append (i+1).ToString());
			}
			
			dzName.Remove(0,dzName.Length);
			
		}
		
		
		minLength = -1;
		
		
		
		ignoreShift = false;
		
	}
	
	public bool InSelectionZone(Vector3 point)
	{
		return colliderBounds.Contains(point);
	}
	
	//CheckGuess must be optimized 100%
	public void CheckGuess()
	{

		//GUIManager.GetInstance().GetChaoAnimator().SetBool("isTyping",true);
		//GUIManager.GetInstance().Type();

		string currentGuess = GetCurrentWord();
	
		//string letter = tile.currentLetter;
		//Sprite[] letters = GUIManager.GetInstance().GetLetterSprites();
		
		//int i = System.Array.IndexOf(GameConstants.alpha,letter.ToCharArray()[0]);
		
		
		GameObject weapon = GUIManager.GetInstance().GetWeaponGUI();
		
		//cache this
		WeaponClickAction weaponClickAction = weapon.GetComponent<WeaponClickAction>();
		
		
		////Debug.Log("trying current guess: " + currentGuess);
		//cace these
		Sprite[] weapons = SpriteCache.GetInstance().Triggers;
		SpriteRenderer weaponRenderer = weapon.GetComponent<SpriteRenderer>();
		
		//we'll just use a basic weapon for now
		string weaponPrefab = null;
		//GUIManager.GetInstance().UpdateAlerts("");
		//if we have used this word before
		//Color c = Color.white;
		int padding = 0;

		
		
		if(WordManager.GetInstance().WordUsed(currentGuess))
		{
			//GUIManager.GetInstance().UpdateAlerts("word used");
			weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_USED];
			weaponClickAction.SetPrefab(null);
			weaponClickAction.hitSprite = null;
			if(weaponClickAction.tapEnabled)
				weaponClickAction.acceptInput = true;
		}
		else if(currentGuess.Length == 0)
		{
			weaponRenderer.sprite = null;//weapons[GameConstants.SPRITE_WEAPON_GUI_BLANK];
			weaponClickAction.hitSprite = null;
			weaponClickAction.SetPrefab(null);
			weaponClickAction.acceptInput = false;
		}
		//hack to only enable the trigger button if you get the full length word
		else if((bossLevel && currentGuess.Length != WordManager.GetInstance().GetCurrentWord().Length) || (minLength > -1 && currentGuess.Length < minLength))
		{
			weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_CANCEL];
			weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_CANCEL_TAP];
			weaponClickAction.SetPrefab(null);
			if(weaponClickAction.tapEnabled)
				weaponClickAction.acceptInput = true;
		}
		else if(currentGuess.Length > 2 && WordManager.GetInstance().IsValidWord(currentGuess))
		{
			if(phasedLevel)
			{
				weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_CHALLENGE];
				weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_CHALLENGE_TAP];
				padding = GameConstants.SPRITE_INDEX_CHALLENGE;
				weaponPrefab = "Set";
			}
			else if(bossLevel)
			{
				weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_LASER];
				weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_LASER_TAP];
				padding = GameConstants.SPRITE_INDEX_LASER;
				weaponPrefab = "Set";
			}

			else
			{
				
				
				//need to make the GUI manager parse sprites and provide named index, instead of using hard-coded array indeces
				/*eg: case 3: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_CANNON];
				 *    weaponPrefab = WeaponController.GetInstance().GetFromPool(GameConstants.WEAPON_CANNON);
				 * */
				switch(currentGuess.Length)
				{
				case 3: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_BULLET];
					weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_BULLET_TAP];
					weaponPrefab = GameConstants.WEAPON_PREFAB_CANNON;
					padding = GameConstants.SPRITE_INDEX_CANNON;
					//c = GameConstants.COLOR_WEAPON_CANNON_1;
					break;
				case 4: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_BULLET2];
					weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_BULLET_TAP];
					weaponPrefab = GameConstants.WEAPON_PREFAB_CANNON_2; 
					padding = GameConstants.SPRITE_INDEX_CANNON_2;
					//c = GameConstants.COLOR_WEAPON_CANNON_2;
					break;
				case 5: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_FLAME];
					weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_FLAME_TAP];
					weaponPrefab = GameConstants.WEAPON_PREFAB_FLAME; 
					padding = GameConstants.SPRITE_INDEX_FLAMER;
					//c = GameConstants.COLOR_WEAPON_FLAME;
					break;
				case 6: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_FLAME2];
					weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_FLAME_TAP];
					weaponPrefab = GameConstants.WEAPON_PREFAB_FLAME_2; 
					padding = GameConstants.SPRITE_INDEX_FLAMER_2;
					//c = GameConstants.COLOR_WEAPON_FLAME_2;
					break;
				case 7: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_ROCKET];
					weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_ROCKET_TAP];
					weaponPrefab = GameConstants.WEAPON_PREFAB_ROCKET; 
					padding = GameConstants.SPRITE_INDEX_ROCKET;
					//c = GameConstants.COLOR_WEAPON_ROCKET;
					break;
				case 8: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_ROCKET2];
					weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_ROCKET_TAP];
					weaponPrefab = GameConstants.WEAPON_PREFAB_ROCKET_2; 
					padding = GameConstants.SPRITE_INDEX_ROCKET_2;
					//c = GameConstants.COLOR_WEAPON_ROCKET_2;
					break;
				case 9: weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_LASER];
					weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_LASER_TAP];
					weaponPrefab = GameConstants.WEAPON_PREFAB_LASER ; 
					padding = GameConstants.SPRITE_INDEX_LASER;
					//c = GameConstants.COLOR_WEAPON_LASER;
					break;
				default:break;
				}
			}
			
			bool fullLength = (currentGuess.Length == WordManager.GetInstance().GetCurrentWord().Length);
			
			weaponClickAction.fullLength = fullLength;
			weaponClickAction.SetPrefab(weaponPrefab);
			weaponClickAction.SetWordLength(currentGuess.Length);
			if(weaponClickAction.tapEnabled)
				weaponClickAction.acceptInput = true;
			weaponClickAction.currentWord = currentGuess;
			
			
			////Debug.Log("setting weapon strength to : " + weaponRenderer.sprite.name);
			
		}
		else
		{
			////Debug.Log("deactivate weapon strength");
			weaponClickAction.SetPrefab(null);
			weaponRenderer.sprite = weapons[GameConstants.SPRITE_WEAPON_GUI_CANCEL];
			weaponClickAction.hitSprite = weapons[GameConstants.SPRITE_WEAPON_GUI_CANCEL_TAP];
			if(weaponClickAction.tapEnabled)
				weaponClickAction.acceptInput = true;
		}
		
		LetterTile tile;
		for(int i=0;i<currentGrid.Count;i++)
		{
			tile = currentGrid[i];
			LetterTile touchTile = tile.GetComponent<LetterTile>();

			touchTile.background.sprite = SpriteCache.GetInstance().LetterTiles[padding];
			if(weaponPrefab != null)
			{
				touchTile.display.color = Color.white;
				if(touchTile.used)
				{
					touchTile.ticked.enabled = true;
					touchTile.ticked.sprite = SpriteCache.GetInstance().LetterTiles[9];
				}
			}
			else
			{
				touchTile.display.color = GameConstants.COLOR_SELECTION;
				if(touchTile.used)
				{
					touchTile.ticked.enabled = true;
					touchTile.ticked.sprite = SpriteCache.GetInstance().LetterTiles[8];
				}
			}
		}
	}

	public void SetLetterSprites(int tileIndex, char c)
	{

		tiles[tileIndex].background.sprite = SpriteCache.GetInstance().LetterTiles[0];
		tiles[tileIndex].display.text = tiles[tileIndex].currentLetter = c.ToString();		
		tiles[tileIndex].used = false;
	}

	public void ActivateTileByLetter(char c)
	{
		LetterTile tile = null;

		for(int i=0;i<tiles.Length;i++)
		{
			if(!string.IsNullOrEmpty(tiles[i].currentLetter) && tiles[i].currentLetter[0] == c && tiles[i].touchState != TileTouchState.INWORD)
			{
				if(tile == null)
					tile = tiles[i];
				else if(tile.used && !tiles[i].used)
					tile = tiles[i];
			}
		}

		if(tile != null)
			tile.MoveToWord();
	}

	public void RemoveLastTile()
	{
		if(currentGrid.Count > 0)
		{
			LetterTile tile = currentGrid[currentGrid.Count-1];
			tile.MoveToSelection();
		}
	}

	
	public void LockTiles()
	{
		locked = true;
		for(int i=0;i<tiles.Length;i++)
		{
			tiles[i].disabled =true;
		}
	}
	
	public void UnLockTiles()
	{
		locked = false;
		for(int i=0;i<tiles.Length;i++)
		{
			tiles[i].disabled =false;
		}
	}
	
	public void ClearLetterTiles()
	{
		for(int i=0;i<tiles.Length;i++)
		{
			//tileSprites[i].color = Color.white;
			//tileSprites[i].sprite = null;//SpriteCache.GetInstance().LetterTiles[26]; //blank
			tiles[i].used = false;

			tiles[i].currentLetter = "";
			tiles[i].posInCurrentWord = -1;
			tiles[i].ticked.enabled = false;
			tiles[i].background.sprite = null;
			tiles[i].display.text = "";
		}
		
	}
	
	public void AddLetter(LetterTile tile, bool forceEnd)
	{

		
		int len = currentGrid.Count;
		
		if(len > 0 && !forceEnd)
		{
			
			//GameObject selectedTile = GameObject.Find(tile.name);
			////Debug.Log("Selected Tile: " + tile.transform.localPosition);
			////Debug.Log("Leftmost tile's " + currentGrid[0].name + "  position: " + currentGrid[0].transform.localPosition);
			if(tile.transform.localPosition.x < currentGrid[0].transform.localPosition.x)
			{
				////Debug.Log("Letter on left of start");
				currentGrid.Insert(0,tile);
			}
			else
			{
				currentGrid.Add(tile);
			}
		}
		else
		{
			currentGrid.Add(tile);
		}
		len = currentGrid.Count;
		
		DrawGrid(null,len);
		
		
	}
	
	
	private void DrawGrid(LetterTile draggingTile, int len)
	{
		
		int[] pattern = patterns[len-1];
		GameObject zone = null;
		LetterTile g = null;
		int index;
		
		for(int i=0;i<pattern.Length;i++)
		{
			g = currentGrid[i];
			index = pattern[i];
			
			if(len % 2 == 0)
			{
				index +=9;
			}
			
			zone = dropZones[index-1];
			
			
			//g.transform.position  = ((GameObject)(GameObject.Find(newZone))).transform.position;
			g.snapTo = zone.transform.position;//g.transform.position;
			zone.GetComponent<DropZone>().currentResident = g;
			if(draggingTile && draggingTile.name .Equals(g.name))
			{
				g.animating = false;
			}
			else
			{
				g.animating = true;
			}
			
			g.dropZoneId = index;
		}
		
		CheckGuess();
	}
	
	
	public void AddLetter(LetterTile tile)
	{
		
		AddLetter(tile, false);
	}
	
	public void RemoveLetter(LetterTile tile)
	{
		if(currentGrid.Remove(tile))
		{
			
			
			////Debug.Log("looking up : " + currentZone);
			
			//GameObject dropZone = GameObject.Find(currentZone);
			GameObject dropZone = dropZones[ tile.dropZoneId -1];
			
			dropZone.GetComponent<DropZone>().currentResident = null;
			tile.dropZoneId = -1;
			
			
			int len = currentGrid.Count;
			if(len > 0)
			{
				DrawGrid(null,len);
			}
			
			
		}
		
		//CheckGuess();
		
	}
	
	public void ResetLetterTiles()
	{
		LetterTile tile;
		for(int i=0;i<currentGrid.Count;i++)
		{
			tile = currentGrid[i];
			tile.Reset();
		}
		
		for(int i=0;i<dropZones.Length;i++)
		{
			GameObject d = dropZones[i];
			d.GetComponent<DropZone>().currentResident = null;
		}
		
		currentGrid.Clear();
	}
	
	public bool Contains(LetterTile tile)
	{
		return currentGrid.Contains(tile);
	}
	
	
	
	public void HitOccupiedZone(LetterTile tile)
	{
		
		////Debug.Log("Calculating hit occupied");
		float xpos = tile.transform.position.x;
		int len = currentGrid.Count;
		
		int start = 0;
		int max = 9;
		
		//Dictionary<string,GameObject>.ValueCollection v = null;
		if(len % 2 == 0)
			
		{
			start = 9;
			max = 17;
		}
		
		
		//iterate through the drop zones and see if we fall into the bounds of one
		for(int i=start;i<max;i++)
		{
			DropZone dropZone = dropZones[i].GetComponent<DropZone>();
			////Debug.Log(string.Format("Checking {0} with xmin {1} and xmax {2} vs {3}", dropZoneobj.name, dropZone.xMin, dropZone.xMax, xpos));
			if(xpos > dropZone.xMin && xpos < dropZone.xMax)
			{
				////Debug.Log("I am hovering over dropZone: " + dropZoneobj.name);
				if(dropZone.currentResident == null)
				{
					////Debug.Log("this zone is unoccupied, no need for shift");
				}
				else if(!ignoreShift)
				{
					//make sure we don't shift things while we're reorganizing
					ignoreShift = true;
					
					//just return if we are hovering over the zone we already occupy
					if(dropZone.currentResident.name.Equals(tile.name))
					{
						ignoreShift = false;
						return;
					}
					
					int pos = currentGrid.IndexOf(dropZone.currentResident);
					if(pos >= 0)
					{
						currentGrid.Remove(tile);
						currentGrid.Insert(pos, tile);
						RedrawGrid(tile);
						
					}
					else
					{
						dropZone.currentResident =null;
					}
					ignoreShift = false;
					
				}
				break;
			}
		}
		
	}
	
	private void RedrawGrid(LetterTile draggingTile)
	{
		
		//Debug.Log("redrawing grid");
		int len = currentGrid.Count;
		DrawGrid(draggingTile, len);
		
		//CheckGuess();
	}
	
	
	


	public void Hide()
	{
		LetterTile tile;
		for(int i=0;i<tiles.Length;i++)
		{
			tile = tiles[i];
			//tiles[i].gameObject.SetActive(false);
			//tile.GetComponent<SpriteRenderer>().renderer.enabled  = false;
			tile.background.GetComponent<Renderer>().enabled = false;
			tile.display.GetComponent<Renderer>().enabled = false;
			tile.ticked.sprite = null;
			tile.ticked.enabled = false;
			//Debug.Log("triggering reset");
			tile.inteferenceAnimator.SetBool(GUIManager.GetInstance()._boolLockInput,true);
		}
	}
	
	public void Show()
	{
		LetterTile tile;
		for(int i=0;i<tiles.Length;i++)
		{
			//tiles[i].gameObject.SetActive(true);

			//tile.GetComponent<SpriteRenderer>().renderer.enabled  = true;

			tile = tiles[i];
			tile.background.GetComponent<Renderer>().enabled = true;
			tile.display.GetComponent<Renderer>().enabled = true;
			tile.ticked.enabled = true;
			tile.inteferenceAnimator.SetBool(GUIManager.GetInstance()._boolLockInput,false);
		}
	}
	
	//this should be cached
	public string GetCurrentWord()
	{
		StringBuilder sb = new StringBuilder();
		LetterTile tile;
		for(int i=0;i<currentGrid.Count;i++)
		{
			tile = currentGrid[i];
		
			string letter = tile.currentLetter;
			sb.Append(letter);
		}
		return sb.ToString();
	}
	
	public LetterTile DisableLetter(int spitType, float duration)
	{
		if(locked)
			return null;

		//LetterTile[] tiles = GetComponentsInChildren<LetterTile>();
		LetterTile disabled = null;
		LetterTile tile;
		for(int i=0;i<tiles.Length;i++)
		{
			tile = tiles[i];
			
			//try to disable a non-vowel first
			if(!string.IsNullOrEmpty(tile.currentLetter) && !tile.disabled && !GameConstants.VowelC(tile.currentLetter.ToCharArray()[0]) && 
			   tile.touchState == TileTouchState.INSELECTION)
			{
				
				if(tile.Disable(spitType, duration))
				{
					disabled = tile;
					return disabled;
				}
				
			}
		}
		
		//if none available, disable a vowel


		for(int j=0;j<tiles.Length;j++)
		{
			tile = tiles[j];

			
			
			if(!string.IsNullOrEmpty(tile.currentLetter) && !tile.disabled && 
			   tile.touchState == TileTouchState.INSELECTION)
			{
				if(tile.Disable(spitType,duration))
				{
					disabled = tile;
					return disabled;
				}
				
			}
		}
		//if we can't disable anything, we'll do damage
		return disabled;
	}
	
	public void RemoveRandomTile()
	{
		if(locked) 
			return;

		if(currentGrid.Count > 0)
		{
			int r = Random.Range(0,currentGrid.Count);
			currentGrid[r].MoveToSelection();
		}
	}
	
	public void ResetLetterPosition()
	{
		LetterTile tile;
		for(int i=0;i<tiles.Length;i++)
		{
			tile = tiles[i];
			tile.ResetPosition();
		}
		lettersShifted = false;
		
	}
	
	public void ShiftLetters()
	{
		if(lettersShifted)
			return;
		
		LetterTile tile;
		for(int i=0;i<tiles.Length;i++)
		{
			tile = tiles[i];
			//SpriteRenderer sr = tile.GetComponent<SpriteRenderer>();
			
			if(tile.background.sprite)
			{
				
				Vector2 tilePos = tile.SelectionOrigin;
				Vector2 newPos = new Vector2(tilePos.x+tile.background.sprite.bounds.extents.x,tile.SelectionOrigin.y);
				tile.gameObject.transform.position= newPos;
				
				tile.SelectionOrigin = newPos;
			}
		}
		lettersShifted = true;
	}
	
	

	
	public LetterTile[] GetTiles()
	{
		return tiles;
	}
	
	public void SetTutorialLevel()
	{
		//tutorialLevel = true;
		for(int i=0;i<tiles.Length;i++)
		{
			tiles[i].tutorialLevel = true;
		}
	}

	public ScrambleLetter[] GetActiveTiles()
	{
		ScrambleLetter[] activeTiles = new ScrambleLetter[WordManager.GetInstance().GetCurrentWord().Length];
		int count = 0;
		for(int i=0;i<tiles.Length;i++)
		{
			if(!string.IsNullOrEmpty(tiles[i].currentLetter))
			{
				activeTiles[count++] = new ScrambleLetter(tiles[i].currentLetter, tiles[i].used, i);
			}
		}
		return activeTiles;
	}

	public void ScrambleWord()
	{
		if(locked) 
			return;

		//get all active tiles
		ScrambleLetter[] activeTiles = GetActiveTiles();
		//generate a scrambled set of indeces
		int[] scrambled = new int[activeTiles.Length];
		for(int s=0;s<scrambled.Length;s++)
		{
			scrambled[s] = s;
		}

		int x = 0;
		//uses Fisher-Yates shuffle  
		int n = scrambled.Length;  
		
		while (n > x) {  
			n--;  
			int k = Random.Range(x,n + 1);  
			int value = scrambled[k];  
			scrambled[k] = scrambled[n];  
			scrambled[n] = value;  
		} 

		//int offset = 
		for(int j=0;j<activeTiles.Length;j++)
		{
			LetterTile currentTile = tiles[activeTiles[j].index];
			ScrambleLetter scrambledTile = activeTiles[scrambled[j]];

			currentTile.currentLetter = currentTile.display.text = scrambledTile.letter;
			currentTile.used = scrambledTile.used;
			if(!currentTile.used)
				currentTile.ticked.enabled = false;
			else
			{
				currentTile.ticked.enabled = true;
				if(currentTile.touchState == TileTouchState.INSELECTION)
					currentTile.ticked.sprite = SpriteCache.GetInstance().LetterTiles[8];
				else
					currentTile.ticked.sprite = SpriteCache.GetInstance().LetterTiles[9];
			}
		}


		CheckGuess();

	}

	public bool SetUsedTiles()
	{
		bool allUsed = true;
		
		for(int i=0;i<tiles.Length;i++)
		{
			if(!string.IsNullOrEmpty(tiles[i].currentLetter))
			{
				//if the tile is in the word, set it be used
				if(tiles[i].touchState == TileTouchState.INWORD)
					tiles[i].used = true;
				//if the tile is in the selection but not used yet, we haven't used all the letters
				else if(!tiles[i].used)
					allUsed = false;
			}
		}
		return allUsed;
	}


	public void TriggerInterference()
	{
		if(locked) 
			return;
		
		LetterTile tile;
		for(int i=0;i<tiles.Length;i++)
		{
			tile = tiles[i];
			if(!string.IsNullOrEmpty(tile.currentLetter) && !tile.disabled)
			{
				//tile.GetComponentInChildren<Animation>().Play ();
				tile.TwinInterference(1f);
			}
		}
	}
}
