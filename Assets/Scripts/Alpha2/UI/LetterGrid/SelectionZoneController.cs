using UnityEngine;
using System.Collections;

public class SelectionZoneController : BaseZoneController {




	public bool tutorialLevel;


	/// <summary>
	/// moves a letter tile from selection to word
	/// </summary>
	/// <param name="currentTouch">Current touch.</param>
	public override void TouchStart(Touch currentTouch)
	{
		Vector3 touchPosition = Camera.main.ScreenToWorldPoint(currentTouch.position);
		touchPosition = new Vector3(touchPosition.x, touchPosition.y, 0);
			
		GameObject touched = CheckTouchPoint(touchPosition,TileTouchState.INSELECTION);
		if(touched)
		{
			LetterTile currentTile = touched.GetComponent<LetterTile>();
			currentTile.MoveToWord();
			if(tutorialLevel)
			{
				GameEvent gameEvent = new GameEvent(MoldicideEvent.LETTER_ANIMATED,0, 0, null);
				GameEventManager.DispatchEvent(gameEvent);
			}

			//currentTile.SetActive(currentTouch.position);
		}
		else
		{
			//Debug.Log(string.Format("touch detected, but nothing hit @ position {0}", touchPosition));
		}
	}

	public override void FakeTouchStart (Vector2 v)
	{
		GameObject touched = CheckTouchPoint(v,TileTouchState.INSELECTION);
		if(touched)
		{
			LetterTile currentTile = touched.GetComponent<LetterTile>();
			currentTile.MoveToWord();

			if(tutorialLevel)
			{
				GameEvent gameEvent = new GameEvent(MoldicideEvent.LETTER_ANIMATED,0, 0, null);
				GameEventManager.DispatchEvent(gameEvent);
			}
			//currentTile.SetActive(currentTouch.position);
		}
		else
		{
			//Debug.Log(string.Format("touch detected, but nothing hit @ position {0}", v));
		}
	}


}
