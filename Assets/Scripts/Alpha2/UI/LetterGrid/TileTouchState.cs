﻿using UnityEngine;
using System.Collections;

public enum TileTouchState  {

	INSELECTION,
	DRAGGING,
	ANIMATING,
	INWORD
}
