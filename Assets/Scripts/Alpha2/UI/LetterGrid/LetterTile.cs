using UnityEngine;

using System.Collections;

public class LetterTile : MonoBehaviour {
	
	public bool tutorialLevel;
	
	public string currentLetter;

	public bool used;
	public int posInCurrentWord;
	public bool disabled;
	
	public TileTouchState touchState;
	public float animationRate = .4f;
	
	public Vector2 snapTo;
	public bool animating;
	public int dropZoneId;

	public SpriteRenderer ticked;

	public TextMesh display;

	private Vector2 selectionOrigin;
	private Vector2 resetPosition;
	private TileTouchState lastState;
	private TileTouchState targetState;

	private Timer disableTimer;

	
	//private Vector2 smallScale;
	//private Vector2 originalScale;
	//private float scale = .75f;
	
	//private float tapThreshold = .75f;
	private float startTouchTime;
	
	private LetterGrid grid;
	
	//private Timer disableTimer;
	//private float disableTime = 3f;
	//private SpriteRenderer spitRenderer;
	
	public SpriteRenderer background;

	private Vector3 screenPoint;
	//private Vector2 offsetPosition;
	private float offsetX;
	private float offsetY;
	//private bool haveDragged;
	public Animator inteferenceAnimator;
	
	public float xMin;
	public float xMax;

	//private Color selectionColor = Color.gray;
	//private Color wordColor = Color.gray;
	
	void Awake()
	{
		grid = GameObject.FindGameObjectWithTag("LetterGrid").GetComponent<LetterGrid>();
		inteferenceAnimator = GetComponentInChildren<Animator>();
		//spriteRenderer = GetComponent<SpriteRenderer>();
		background.sprite = null;
		ticked.enabled = false;
		
		//originalScale = transform.localScale;
		//smallScale = new Vector2(originalScale.x*scale, originalScale.y*scale);
		
		selectionOrigin = transform.position;
		resetPosition = transform.position;
		disableTimer = TimerFactory.GetInstance().CreateTimer(1f);

		animating = false;
		//haveDragged = false;
	}
	
	
	void Start()
	{
		//CalculateBounds();
	}
	
	private void CalculateBounds()
	{
		//float currentX = transform.position.x;
		Bounds b = GetComponent<Renderer>().bounds;
		
		xMin = b.min.x;
		xMax = b.max.x;
	}
	

	
	public bool IsPlayable()
	{
		return (currentLetter.Length > 0 && !disabled);
	}
	
	public void MoveToWord()
	{
		if(currentLetter.Length == 0)
			return;
		
		if(disabled)
			return;
		

		//transform.localScale = smallScale;
		if(!grid.Contains(this))
		{
			grid.AddLetter(this,true);
		}
		touchState = TileTouchState.INWORD;

		background.sortingOrder = 3;
		//CalculateBounds();
	}
	
	public void MoveToSelection()
	{
		if(currentLetter.Length == 0)
			return;
		
		if(disabled)
			return;
		
		snapTo = selectionOrigin;
		animating = true;

		//if we were previously in the word, we'll want to tell the grid to reduce the number of letters to track
		grid.RemoveLetter(this);
		
		//GetComponent<SpriteRenderer>().color = Color.white;
		//int index = System.Array.IndexOf(GameConstants.alpha,currentLetter[0]);
		//if(used)
			//index += 27;

		if(used) 
		{
			ticked.enabled = true;
			ticked.sprite = SpriteCache.GetInstance().LetterTiles[8];
		}


		display.color = GameConstants.COLOR_SELECTION;

		background.sprite = SpriteCache.GetInstance().LetterTiles[0];
		touchState = TileTouchState.INSELECTION;
		
		background.sortingOrder = 1;
		display.GetComponent<Renderer>().sortingOrder = 3;
	}
	
	public void ShowActive()
	{
		//transform.localScale = 1.25f;//originalScale;
		background.sortingOrder = 5;
		display.GetComponent<Renderer>().sortingOrder = 6;
		//display.transform.position = new Vector3(display.transform.position.x,display.transform.position.y,display.transform.position.z);

	}
	
	public void StopActive()
	{
		//transform.localScale = 1f;//smallScale;
		//display.renderer.sortingOrder = 1;
		background.sortingOrder = 1;
		display.GetComponent<Renderer>().sortingOrder = 3;
		//display.transform.position = new Vector3(display.transform.position.x,display.transform.position.y,display.transform.position.z);
	}


	public void SetSelectionOrigin()
	{
		selectionOrigin = transform.position;
	}

	
	void Update()
		
	{
		
		if(disabled)
		{
			if(disableTimer.Update())
			{
				disabled = false;
			}
		}		
		if(animating)
		{
			////Debug.Log("animating");
			transform.position = Vector2.Lerp(transform.position, snapTo, animationRate);

			//transform.position = Vector2.MoveTowards(transform.position, snapTo, animationRate);
			if((Vector2)transform.position == snapTo)
			{
				animating = false;
				////Debug.Log("animation done");
				snapTo = transform.position;
				

				
			}
		}
		
		
	}
	
	public void Reset()
	{
		transform.position = selectionOrigin;
		touchState = TileTouchState.INSELECTION;
		display.color = GameConstants.COLOR_SELECTION;
		//transform.localScale = originalScale;
		dropZoneId = -1;
		if(!string.IsNullOrEmpty(currentLetter))
		{
			//int index = System.Array.IndexOf(GameConstants.alpha,currentLetter[0]);
			if(used) 
			{
				ticked.enabled = true;
				ticked.sprite = SpriteCache.GetInstance().LetterTiles[8];
			}
			

			background.sprite = SpriteCache.GetInstance().LetterTiles[0];

		}
		
		animating = false;

		background.sortingOrder = 1;
	}
	
	public bool Disable(int spitType, float duration)
	{
		if(disabled)
			return false;
		
		disabled = true;
		disableTimer.UseNewTime(duration);
		disableTimer.StartTimer();
		SpitterInterference(spitType);
		return true;
		//disableTimer.StartTimer();
		//need to use pooling @ the grid level
		//spitRenderer = ((GameObject)GameObject.Instantiate(Resources.Load(GameConstants.GUI_PREFAB_PATH+"tileSpit_01"))).GetComponent<SpriteRenderer>();
		//spitRenderer.transform.position = transform.position;
	}
	
//	public void Enable()
//	{
//		disabled = false;
//		inteferenceAnimator.SetBool("SpitterInterference", false);
//	}
	
	public void SpitterInterference(float type)
	{
		if(inteferenceAnimator)
		{
			inteferenceAnimator.SetFloat(GUIManager.GetInstance()._floatSpitType, type);
			inteferenceAnimator.SetTrigger(GUIManager.GetInstance()._triggerSpitterInt);
		}

	}
	
	
	public void TwinInterference(float duration)
	{
		//inteferenceAnimator.speed = duration;
		if(inteferenceAnimator)
			inteferenceAnimator.SetTrigger(GUIManager.GetInstance()._triggerTwinInt);
	}
	
	
	
	
	public void ResetPosition()
	{
		selectionOrigin = resetPosition;
		transform.position = resetPosition;
	}
	
	public Vector2 SelectionOrigin {
		get {
			return selectionOrigin;
		}
		set {
			selectionOrigin = value;
		}
	}
}