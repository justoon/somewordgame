﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class LevelSelectController_alpha : MonoBehaviour {


	public Transform leftEdge;
	private GameObject firstElement;
	private GameObject currentElement;
	//private GameObject lastElement;


	private float setPosX = 0f;
	//private float lastExtents = 0f;

	//private Vector3 centerPoint;

	public GameObject CurrentElement {
		get {
			return currentElement;
		}
	}

	public GameObject FirstElement {
		get {
			return firstElement;
		}
	}

	void Start () 
	{

		//GUIManager.GetInstance().InitController();
		//not moving to GUI Manager since this is the only level that looks up this info
		//centerPoint = GameObject.Find("CenterPoint").transform.position;
		setPosX = leftEdge.position.x;


		PlayerProfile profile = PlayerProfile.GetInstance();
		List<LevelRecord> playerScores = profile.ScoreMatrix;
		//int currentLevel = profile.CurrentLevel;
		int maxLevel = profile.HighestLevel;

	

		StringBuilder sb = new StringBuilder(GameConstants.LEVEL_SELECT_PREFAB_PATH);

		//not moving to GUI Manager since this is the only level that looks up this info
		GameObject.Find("TotalScore_Story").GetComponent<GUIText>().text = profile.TotalScore.ToString("##,#");
		if(profile.Points < 39)
			GameObject.Find("TotalScore_Survival").GetComponent<GUIText>().text = "locked";
		else
			GameObject.Find("TotalScore_Survival").GetComponent<GUIText>().text = profile.SurvivalScore.ToString("##,#");



		//should make a prefab cache
		GameObject storyLevelPrefab = (GameObject) Resources.Load(sb.Append("StoryLevel").ToString());
		sb.Remove(31,10);
		GameObject currentLevelPrefab = (GameObject) Resources.Load(sb.Append("CurrentLevel").ToString());
		sb.Remove(31,12);
		GameObject completedLevelPrefab = (GameObject) Resources.Load(sb.Append("CompletedLevel").ToString());
		sb.Remove(31,14);
		GameObject upgradeLevelPrefab = (GameObject) Resources.Load(sb.Append("UpgradeLevel").ToString());
		sb.Remove(31,12);
		GameObject tutorialLevelPrefab = (GameObject) Resources.Load(sb.Append("TutorialLevel").ToString());

		GameObject currentPrefab;
		GameObject o = null;

		for(int i=0;i<=maxLevel;i++)
		{
			if(i <= GameMap.GetInstance().GetMaxLevel())
			{
				LevelRecord lr;
				if(i == maxLevel)
					lr = GameMap.GetInstance().GetLevel(i);
				else
					lr = playerScores[i];
				
				//create the associated prefab
				switch(lr.type)
				{
				case LevelRecord.LevelType.STORY: 
					currentPrefab = storyLevelPrefab;
						break;
				case LevelRecord.LevelType.STANDARD:
				case LevelRecord.LevelType.BOSS: 
						if(i == maxLevel)
							currentPrefab = currentLevelPrefab;
						else if(i == 1)
							currentPrefab = tutorialLevelPrefab;
						else
							currentPrefab = completedLevelPrefab;	
						break;
				case LevelRecord.LevelType.UPGRADE:
					currentPrefab = upgradeLevelPrefab;
					break;
				default: currentPrefab = null; break;
				}

				o = (GameObject) GameObject.Instantiate(currentPrefab);
				LevelSelectItem selectItem = o.GetComponent<LevelSelectItem>();
				selectItem.level = i;
				//selectItem.Init();
				//GameObject selectElement = (GameObject) GameObject.Instantiate(Resources.Load(sb.ToString()));


				setPosX += o.GetComponent<Renderer>().bounds.extents.x;


				if(lr.type == LevelRecord.LevelType.STANDARD || lr.type == LevelRecord.LevelType.BOSS)
				{
					if(o.transform.childCount > 0)
						o.transform.GetChild(0).GetComponent<GUIText>().text = lr.score.ToString();
					if(o.transform.childCount > 1 && lr.corrections == 0)
						o.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = SpriteCache.GetInstance().FlawlessBadge;
				}

				o.transform.position = new Vector2(setPosX, o.transform.position.y);
				o.transform.parent = transform;

				setPosX += o.GetComponent<Renderer>().bounds.extents.x+.2f;

				if(i==0)
					firstElement = o;
				else if(i == maxLevel)
					currentElement = o;
			}

		}

		if(currentElement == null)
			currentElement = o;
	}


}

