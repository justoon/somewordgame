﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteCache {

	private static SpriteCache instance;


	private Sprite[] barrierDamage;
	//private Sprite[] levelSelectComplete;
	//private Sprite[] levelSelectCurrent;

	private Sprite[] letterTiles;
	private Sprite[] levelFailTitles;
	private Sprite[] levelSucessTitles;
	private Sprite[] options;

	private Sprite[] levelCompleteIcons;
	private Sprite[] levelCurrentIcons;
	private Sprite[] levelCurrentIconsTap;
	private Sprite[] upgradeIcons;
	private Sprite[] storyIcons;

	private Sprite[] bossTiles;

	private Sprite[] triggers;

	private Sprite[] spitterTileGoo;


	private Sprite[] blobSprites;

	private Sprite flawlessBadge;
	private Sprite bossScrambleTile;

	private Sprite weaponQueueCannon;
	private Sprite weaponQueueFlamer;
	private Sprite weaponQueueRocket;


	private SpriteCache()
	{

		blobSprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_BLOB);

		flawlessBadge = Resources.Load<Sprite>(GameConstants.SPRITE_PATH_FLAWLESS_BADGE);

		spitterTileGoo = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_SPITTER_TILE_GOO);

		weaponQueueCannon  = Resources.Load<Sprite>(GameConstants.SPRITE_PATH_WEAPON_QUEUE_CANNON);
		weaponQueueFlamer  = Resources.Load<Sprite>(GameConstants.SPRITE_PATH_WEAPON_QUEUE_FLAMER);
		weaponQueueRocket  = Resources.Load<Sprite>(GameConstants.SPRITE_PATH_WEAPON_QUEUE_ROCKET);

		bossTiles = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_BOSS_TILES);
		bossScrambleTile = Resources.Load<Sprite>(GameConstants.SPRITE_PATH_BOSS_SCRAMBLE_TILE);

	
		//levelSelectComplete = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LEVEL_SELECT_COMPLETE);
		//levelSelectCurrent = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LEVEL_SELECT_CURRENT);
//		List<Sprite> tempList = new List<Sprite>();
//
//		Sprite[] sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//
//		sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES_CANNON);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//
//		sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES_CANNON2);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//
//		sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES_FLAMER);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//
//		sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES_FLAMER2);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//
//		sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES_ROCKET);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//
//		sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES_ROCKET2);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//
//		sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES_LASER);
//		foreach(Sprite s in sprites)
//			tempList.Add(s);
//	
//
//		letterTiles = new Sprite[tempList.Count];
//		letterTiles = tempList.ToArray();
		Sprite[] sprites = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_LETTER_TILES);
		letterTiles = new Sprite[11];
		letterTiles[0] = sprites[2];
		letterTiles[1] = sprites[0];
		letterTiles[2] = sprites[1];
		letterTiles[3] = sprites[3];
		letterTiles[4] = sprites[4];
		letterTiles[5] = sprites[6];
		letterTiles[6] = sprites[7];
		letterTiles[7] = sprites[5];
		letterTiles[8] = sprites[23];
		letterTiles[9] = sprites[24];
		letterTiles[10] = sprites[30];


	
		//options = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_OPTION);
		triggers = Resources.LoadAll<Sprite>(GameConstants.SPRITE_PATH_TRIGGER);


	
	}


	public static SpriteCache GetInstance()
	{
		if(instance == null)
		{
			instance = new SpriteCache();
		}
		
		return instance;
	}





	
	public Sprite[] Triggers {
		get {
			return triggers;
		}
	}
	

	
	public Sprite[] LetterTiles {
		get {
			return letterTiles;
		}
	}
	




	public Sprite FlawlessBadge {
		get {
			return flawlessBadge;
		}
	}

	public Sprite[] BossTiles {
		get {
			return bossTiles;
		}
	}

	public Sprite BossScrambleTile {
		get {
			return bossScrambleTile;
		}
	}

	public Sprite WeaponQueueCannon {
		get {
			return weaponQueueCannon;
		}
	}

	public Sprite WeaponQueueFlamer {
		get {
			return weaponQueueFlamer;
		}
	}

	public Sprite WeaponQueueRocket {
		get {
			return weaponQueueRocket;
		}
	}

	public Sprite[] SpitterTileGoo {
		get {
			return spitterTileGoo;
		}
	}



	public Sprite[] BlobSprites {
		get {
			return blobSprites;
		}
	}
}
