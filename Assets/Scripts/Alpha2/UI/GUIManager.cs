using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class GUIManager : MonoBehaviour  {

	public Animator fadeTransition;
	public TouchManager touchManager;
	public EventSystem unityUI;



    private GameObject weapon;
    //cache script refs
    private GameController gameController;
	private WeaponClickAction weaponButton;
	private LetterGrid letterGrid;
	private GameLevel gameLevel;
	
	private GameObject[] letterTiles;

	private Score score;
	private Text scoreDisplay;
	
	//gui texts
	private GUIText fps;
	private GUIText guiTimer;
	private GUIText nemesis;
	
	private GUIText[] gameLevelGUI;

	private Vector3 origin = GameConstants.OVERLAY_RESET_POSITION;
	private static GUIManager instance;

	private QuitLevel quitButton;
	
	private GameObject successScreen;
	private GameObject failScreen;
	private int levelOutcome = 0;

	private Text wordSolve;
	private Animator solveAnimator;
	

	public int _triggerWordSolve;
	public int _triggerDeath;
	public int _triggerReset;
	public int _triggerActivate;
	public int _triggerHit;
	public int _triggerStrike;
	public int _triggerFail;
	public int _triggerSuccess;
	public int _triggerCycle;
	public int _triggerSolve;
	public int _triggerType;
	public int _triggerMistake;
	public int _triggerCheer;
	public int _triggerGameOver;
	public int _triggerLevelComplete;
	public int _triggerFadeOut;
	public int _triggerFadeIn;
	public int _triggerTwinStatic;
	public int _triggerTwinElite;
	public int _triggerSpitterInt;
	public int _triggerTwinInt;
	public int _triggerBarrierBreak;
	public int _triggerChain;
	public int _triggerCorrection;
	public int _triggerChallenge;
	public int _triggerScore;
	public int _triggerUpdateScore;
	public int _triggerFoamLoad;
	public int _triggerFoamDeploy;
	public int _triggerFoamDeactivate;
	public int _boolHit;
	public int _boolAttack;
	public int _boolLoad;
	public int _boolLaserFire;
	public int _boolIsFiring;
	public int _boolLockInput;
	public int _floatDroneBlend;
	public int _floatBeastBlend;
	public int _floatSpitterBlend;
	public int _floatTwinBlend;
	public int _floatSpeed;
	public int _floatGlitchAttack;
	public int _floatWOPRCycle;
	public int _floatChaoAttitude;
	public int _floatChaoCheer;
	public int _floatSpitType;
	public int _floatPercent;



	void Awake() 
	{
		if (instance != null && instance != this) 
		{
			instance.FadeInLevel(Application.loadedLevel);
			Destroy(this.gameObject);
			return;
		} 
		else 
		{

			_triggerWordSolve = Animator.StringToHash("wordSolve");
			_triggerDeath= Animator.StringToHash("Death");
			_triggerReset= Animator.StringToHash("Reset");
			_triggerActivate= Animator.StringToHash("activate");
			_triggerHit= Animator.StringToHash("Hit");
			_triggerStrike= Animator.StringToHash("Strike");
			_triggerFail= Animator.StringToHash("goFail");
			_triggerSuccess= Animator.StringToHash("goSuccess");
			_triggerCycle= Animator.StringToHash("Cycle");
			_triggerSolve= Animator.StringToHash("Solve");
			_triggerType= Animator.StringToHash("Type");
			_triggerMistake= Animator.StringToHash("Mistake");
			_triggerCheer= Animator.StringToHash("Cheer");
			_triggerGameOver= Animator.StringToHash("GameOver");
			_triggerLevelComplete= Animator.StringToHash("LevelComplete");
			_triggerFadeOut= Animator.StringToHash("FadeOut");
			_triggerFadeIn= Animator.StringToHash("FadeIn");
			_triggerTwinStatic= Animator.StringToHash("TwinStatic");
			_triggerTwinElite= Animator.StringToHash("TwinElite");
			_triggerSpitterInt= Animator.StringToHash("SpitterInterference");
			_triggerTwinInt= Animator.StringToHash("TwinInterference");
			_triggerBarrierBreak= Animator.StringToHash("barrier_gameOver");
			_triggerChain= Animator.StringToHash("showChain");
			_triggerCorrection= Animator.StringToHash("showCorrection");
			_triggerChallenge= Animator.StringToHash("showChallenge");
			_triggerScore= Animator.StringToHash("showScore");
			_triggerUpdateScore= Animator.StringToHash("updateScore");
			_triggerFoamLoad= Animator.StringToHash("incrementFoamLoad");
			_triggerFoamDeploy= Animator.StringToHash("Deploy");
			_triggerFoamDeactivate= Animator.StringToHash("Deactivate");
			_boolHit= Animator.StringToHash("Hit");
			_boolAttack= Animator.StringToHash("Attack");
			_boolLoad= Animator.StringToHash("Load");
			_boolLaserFire= Animator.StringToHash("laserFire");
			_boolIsFiring= Animator.StringToHash("isFiring");
			_boolLockInput= Animator.StringToHash("LockInput");
			_floatDroneBlend= Animator.StringToHash("DroneType");
			_floatBeastBlend= Animator.StringToHash("BeastType");
			_floatSpitterBlend= Animator.StringToHash("SpitterType");
			_floatTwinBlend= Animator.StringToHash("TwinType");
			_floatSpeed= Animator.StringToHash("Speed");
			_floatGlitchAttack= Animator.StringToHash("AttackType");
			_floatWOPRCycle= Animator.StringToHash("cycleType");
			_floatChaoAttitude= Animator.StringToHash("Attitude");
			_floatChaoCheer= Animator.StringToHash("CheerType");
			_floatSpitType= Animator.StringToHash("SpitType");
			_floatPercent= Animator.StringToHash("percentOf");


			FadeInLevel(-1);
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}

	public static GUIManager GetInstance()
	{
		return instance;
	}


	public void TurnOffRenderers(GameObject x)
	{
		Renderer[] rs = x.GetComponentsInChildren<Renderer>();
		Renderer r;
		for(int i=0;i<rs.Length;i++)
		{
			r = rs[i];
			r.enabled = false;
		}
	}

	public void TurnOnRenderers(GameObject x)
	{
		Renderer[] rs = x.GetComponentsInChildren<Renderer>();
		Renderer r;
		for(int i=0;i<rs.Length;i++)
		{
			r = rs[i];
			r.enabled = true;
		}
	}





	void FadeInLevel(int level) {

		//instance = this;
		touchManager = GameObject.Find("GameLogic").GetComponent<TouchManager>();
		gameController = GameObject.Find("GameLogic").GetComponent<GameController>();
		unityUI = GameObject.Find ("EventSystem").GetComponent<EventSystem>();

		if(!Application.loadedLevelName.Equals("Transition"))
				FadeIn();
	}




	public void LoadGameLevel()
	{

		
			levelOutcome = 0;
            weapon = GameObject.FindGameObjectWithTag("weaponStrength");
            gameLevel = GameObject.Find("GameLevel").GetComponent<GameLevel>();
			letterTiles = GameObject.FindGameObjectsWithTag("LetterTile");
			
			letterGrid = GameObject.FindGameObjectWithTag("LetterGrid").GetComponent<LetterGrid>();
			//comboIndicator = GameObject.FindGameObjectWithTag("ComboIndicator").GetComponent<TextMesh>();
			
			GameObject scoreObj = GameObject.FindGameObjectWithTag("Score");
			if(scoreObj)
			{
				score = scoreObj.GetComponent<Score>();
				score.level = gameLevel;
				UpdateScore(Score.score);
			}


			GameObject quitButtonO = GameObject.Find("btn_quit");
			if(quitButtonO)
			{
				quitButton = quitButtonO.GetComponent<QuitLevel>();
				quitButton.Enable();
			}
		



		if(GameObject.Find("txt_solve") != null)
		{
			wordSolve = GameObject.Find("txt_solve").GetComponent<Text>();
			solveAnimator = GameObject.Find("ui_wordSolve_alert").GetComponent<Animator>();
		}


		gameLevel.StartLevel();
		fadeTransition.SetBool(_boolLoad, false);

	}


	public void ShowSuccess()
	{
		if(successScreen)
			successScreen.SetActive(true);
	}

	public void ShowFail()
	{
		if(failScreen)
			failScreen.SetActive(true);
	}




	public void DisableQuit()
	{
		quitButton.Disable();
	}

	public LetterGrid GetLetterGrid()
	{
		return letterGrid;
	}

	public void HideTrigger()
	{

		GetWeaponGUI().GetComponent<SpriteRenderer>().sprite = null;
		//weaponButton.acceptInput = false;
	}

	public void UpdateScore(int newScore)
	{
		score.ShowScore(newScore);
	}

	public void ResetScore()
	{
		score.Reset();
	}

	public string FormatTime(float elapsed)
	{
		int	minutes = (int)(elapsed/60f );
		int seconds = (int)(elapsed % 60f);
		
		//int fraction =  (int)((elapsed *10) %10);
		string sElapsed = string.Format("{0}:{1}", minutes, seconds);
		if(seconds < 10)
		{
			sElapsed = string.Format("{0}:0{1}", minutes, seconds);
		}
		return sElapsed;
	}

	public string FormatPercent(float num)
	{
		string percent = num.ToString("#0.##%");
		return percent;
	}

	public WeaponClickAction GetWeaponButton()
	{
		return weaponButton;
	}
	
		
	public GameObject GetWeaponGUI()
	{
		return weapon;
	}
	




	public void FadeOut()
	{
		//Debug.Log("Fading Out");
		LockGUI();
		fadeTransition.SetTrigger(_triggerFadeOut);
	}

	public void FadeIn()
	{
		//Debug.Log("Fading In");

		fadeTransition.SetTrigger(_triggerFadeIn);
	}

	public void FadeInComplete()
	{
		//Debug.Log("FadeInComplete");
		UnlockGUI();
	}

	//callback when the fade out is done
	public void FadeOutComplete()
	{

		//Debug.Log("Fade Out Complete");
		//if we're loading a level load the transition level
		if(fadeTransition.GetBool("Load"))
		{
			Application.LoadLevel("Transition");	
		}
		//otherwise just fade back in
		else
			fadeTransition.SetTrigger(_triggerReset);
	}

	public void LevelFadeOut()
	{
		//Debug.Log("Interlevel Fade Out");
		LockGUI();
		//sets the load flag to indicate we are loading a game level
		fadeTransition.SetBool(_boolLoad, true);
		fadeTransition.SetTrigger(_triggerFadeOut);
	}

	public void LevelLoaded()
	{
		//Debug.Log("Level has loaded");
		ResetOverlayPosition();
		fadeTransition.SetBool(_boolLoad, false);
	}


	public void LockGUI()
	{
		if(touchManager)
		{
			touchManager.acceptInput = false;
			#if UNITY_EDITOR 
			touchManager.useMouse = false;
			#endif
		}

		if(unityUI)
			unityUI.enabled = false;



	}

	public void UnlockGUI()
	{
		if(touchManager)
		{
			touchManager.acceptInput = true;
			#if UNITY_EDITOR 
			touchManager.useMouse = true;
			#endif
		}

		if(unityUI)
			unityUI.enabled = true;
		

	}

	public void MoveTransition(Vector3 position)
	{
		transform.position = position;
	}


	public void ResetOverlayPosition()
	{
		transform.position = origin;
	}

	public void TriggerTwinStatic()
	{
		fadeTransition.SetTrigger(_triggerTwinStatic);
	}

	public void TriggerTwinElite()
	{
		fadeTransition.SetTrigger(_triggerTwinElite);
	}

	public void EliteComplete()
	{
		Camera.main.transform.Rotate(new Vector3(0,0,-180));
		GetLetterGrid().TriggerInterference();
	}



	public void ResetLevelGUI()
	{
//		score.GetComponent<TextMesh>().text ="0";
//		score.GetComponent<MeshRenderer>().enabled = true;
//		score.GetComponent<Score>().Reset();
		
		//fps.enabled = true;
		//moldieCounter.GetComponent<MeshRenderer>().enabled = true;
		//guiTimer.enabled = true;
		UnLockGrid();
		letterGrid.ResetLetterTiles();
		letterGrid.ClearLetterTiles();
		letterGrid.ResetLetterPosition();
		HideTrigger();
	}
	
	public void HideGUIText()
	{
		//score.GetComponent<MeshRenderer>().enabled = false;
		//comboIndicator.renderer.enabled = false;
		//fps.enabled = false;
		//moldieCounter.GetComponent<MeshRenderer>().enabled = false;
		//barrierHealth.renderer.enabled = false;
		//guiTimer.enabled = false;
	}

	public void HideLetterGrid()
	{
		letterGrid.Hide();
		weaponButton.Reset();
	}

	public void ShowLetterGrid()
	{
		letterGrid.Show();
	}

	public void ResetLetterGrid()
	{
		letterGrid.ResetLetterTiles();

	}
	
	public void ClearLetterGrid()
	{
		letterGrid.ClearLetterTiles();
	}
	
	public void LockGrid()
	{
		letterGrid.LockTiles();
		weaponButton.acceptInput = false;
		


	}

	public void UnLockGrid(bool unlockTrigger)
	{
		letterGrid.UnLockTiles();
		if(unlockTrigger)
			weaponButton.acceptInput = true;
		//need to tell foam bomb fire button to re-enable if applicable
		
	}


	public void UnLockGrid()
	{
		UnLockGrid(true);
	}
	
	

	

	public int GetLevelOutcome()
	{
		return levelOutcome;
	}

	public void LevelComplete(int outcome)
	{
		levelOutcome = outcome;
		
	}


	public GameObject[] LetterTiles {
		get {
			return letterTiles;
		}
	}

	public GameLevel GameLevel {
		get {
			return gameLevel;
		}
	}

	

	public GameController GameController {
		get {
			return gameController;
		}
	}



	public Score Score {
		get {
			return score;
		}
	}

	

	public void AnimateSolution(string word)
	{
		wordSolve.text = word;
		solveAnimator.SetTrigger(_triggerWordSolve);
	}


}
