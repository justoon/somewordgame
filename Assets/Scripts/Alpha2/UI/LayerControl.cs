﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class LayerControl : MonoBehaviour {

	//public Renderer renderer;
	public string id;

	public string layerName;
	public int orderInLayer;

	void Start()
	{
		GetComponent<Renderer>().sortingLayerName = layerName;
		GetComponent<Renderer>().sortingOrder = orderInLayer;
	}


}
