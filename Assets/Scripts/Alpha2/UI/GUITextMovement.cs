﻿using UnityEngine;
using System.Collections;

public class GUITextMovement : MonoBehaviour {

	public float offset = -.03f;

	//GameObject levelSelect;

	// Use this for initialization
	void Start () {
		//levelSelect = GameObject.Find("LevelSelector");
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = new Vector2(Camera.main.WorldToViewportPoint(transform.parent.transform.position).x+offset, transform.position.y);

	}
}
