﻿using UnityEngine;
using System.Collections;

public class TouchZone : MonoBehaviour {

	public virtual void TouchStart(Touch t)
	{
		////Debug.Log("Touch started");
	}

	public virtual void TouchEnd(Touch t)
	{
		////Debug.Log("Touch ended");
	}

	public virtual void TouchMoved(Touch t)
	{
		////Debug.Log("Touch moved");
	}

	public virtual void FakeTouchStart(Vector2 v)
	{}

	public virtual void FakeTouchEnd(Vector2 v)
	{}

	public virtual void FakeTouchMoved(Vector2 v)
	{}


}
