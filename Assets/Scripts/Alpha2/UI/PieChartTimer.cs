﻿using UnityEngine;
using System.Collections;

public class PieChartTimer : MonoBehaviour {

	public float rotationSpeed = 20f;
	public float countDown = 10f;

	private Transform firstHalf;
	private Transform centerPt;
	private Transform secondHalf;
	private Transform fail;

	private Transform currentObject;

	//private Quaternion originFirst;
	//private Quaternion originSecond;

	private float accumulatedAngle;

	private bool showingFail;

	private Timer failTimer;

	private bool timerActive;
	private bool triggerSwap;

	// Use this for initialization
	void Start () {
	
		centerPt = transform.Find ("widget_timer_yellow_mask").transform;

		firstHalf = transform.Find("widget_timer_right_white").transform;
		//originFirst = firstHalf.rotation;
		//originRotation = firstHalf.rotation;
		secondHalf = transform.Find("widget_timer_right_yellow").transform;


		fail = transform.Find("widget_timer_fail_black").transform;
		//originRotation2 = secondHalf.rotation;
		//secondHalf.renderer.enabled = false;
		//originSecond = secondHalf.rotation;
		currentObject = firstHalf;

		accumulatedAngle = 0f;

		failTimer = TimerFactory.GetInstance().CreateTimer(.25f);
		timerActive = false;

		rotationSpeed = 360/countDown;
	}



	public void Reset()
	{
		firstHalf.RotateAround(centerPt.position, Vector3.forward, -accumulatedAngle);
		secondHalf.RotateAround(centerPt.position, Vector3.forward, -accumulatedAngle);
		accumulatedAngle = 0f;
		firstHalf.GetComponent<Renderer>().enabled = true;
		firstHalf.position = new Vector3(firstHalf.position.x, firstHalf.position.y,-1);
		secondHalf.GetComponent<Renderer>().enabled = false;
		//secondHalf.position = new Vector3(secondHalf.position.x, secondHalf.position.y,1);
		triggerSwap = false;
		currentObject = firstHalf;

	}
	

	// Update is called once per frame
	void Update () {
	
		if(timerActive)
			{
			if(failTimer.HasStarted())
			{
				if(failTimer.Update())
				{
					fail.position = new Vector3(fail.position.x, fail.position.y,1);
					failTimer.StopTimer();
					Reset();
				}
			}
			else
			{
				//float angle = Vector3.Angle(Vector3.up, currentObject.up);
				////Debug.Log(angle);
				//bug here where sometimes the sides are failing to swap - may be 2 frames in a row where angle = 180

				accumulatedAngle += -(rotationSpeed*Time.deltaTime);

				if(Mathf.RoundToInt(accumulatedAngle) == -180 && !triggerSwap)
				{
					if(currentObject == firstHalf)
					{
						currentObject = secondHalf;
						firstHalf.position = new Vector3(firstHalf.position.x, firstHalf.position.y,1);
						firstHalf.GetComponent<Renderer>().enabled = false;
						secondHalf.position = new Vector3(secondHalf.position.x, secondHalf.position.y,-1);
						secondHalf.GetComponent<Renderer>().enabled = true;
						triggerSwap = true;
						//firstHalf.rotation = originRotation;
						//firstHalf.renderer.enabled = false;
						//secondHalf.renderer.enabled = true;
					}
					/*else 
					{
						currentObject = firstHalf;
						firstHalf.position = new Vector3(firstHalf.position.x, firstHalf.position.y,-1);
						secondHalf.position = new Vector3(secondHalf.position.x, secondHalf.position.y,1);
						//secondHalf.rotation = originRotation2;
						//secondHalf.renderer.enabled = false;
						//firstHalf.renderer.enabled = true;
					}*/


				}



				if(accumulatedAngle <= -360)
				{
					ShowFail();
					GameEvent ge = new GameEvent(MoldicideEvent.TIMER_EXPIRED);		
					//implemented by subclasses
					GUIEventManager.DispatchEvent(ge);
				}
				else
				{
					firstHalf.RotateAround(centerPt.position, Vector3.forward, -(rotationSpeed*Time.deltaTime));
					secondHalf.RotateAround(centerPt.position, Vector3.forward, -(rotationSpeed*Time.deltaTime));
				}
			}
		}
	}

	public void ShowFail()
	{
		fail.position = new Vector3(fail.position.x, fail.position.y,-2);
		secondHalf.position = new Vector3(secondHalf.position.x, secondHalf.position.y,0);
		failTimer.StartTimer();
	}

	public void StopAll()
	{
		timerActive = false;
	}

	public void StartTimer()
	{
		timerActive = true;
	}
}
