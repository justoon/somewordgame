﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoryTemplateController : MonoBehaviour {

	public Image mainImage;
//	public Text mainTitle;
//	public Text subTitle;
	public Text newUpgrade;
	public Text newUpgradeDesc;
	public Image addedTile;
	public Text tile;

	public Text chapterText;
	public Text chapterTitle;
	public Text chapterSubTitle;

	public Sprite[] backgrounds;

	public bool[] addtiles;


	void Start()
	{
		GameObject.FindGameObjectWithTag("GameController").transform.position = GameConstants.OVERLAY_RESET_POSITION;
		int storyLevel = GameMap.GetInstance().GetThisLevel().level;

		if(storyLevel > 6)
		{
			GameObject.Find("tile").SetActive(false);
		}

		if(storyLevel == 12)
		{
			GameObject.Find("btn_confirm").SetActive(false);
		}

		mainImage.sprite = backgrounds[storyLevel-1];
		addedTile.enabled = addtiles[storyLevel-1];
		string fileName = "StoryContent/Story"+storyLevel;
		TextAsset storyContent = Resources.Load(fileName) as TextAsset;
		string[] content = storyContent.text.Split(new string[] { "|" }, System.StringSplitOptions.None);
//		mainTitle.text = content[0];
		chapterTitle.text = content[0];
//		subTitle.text = content[1];
		chapterSubTitle.text = content[1];
		tile.text = content[2];
		newUpgrade.text = content[3];
		newUpgradeDesc.text = content[4];
		chapterText.text = content[5];
		Resources.UnloadAsset(storyContent);

		int index = GameMap.GetInstance().GetThisLevel().index;
		PlayerProfile profile = PlayerProfile.GetInstance();

		if(profile.HighestLevel <= index)
		{
			//needs to accommodate 2 digits 
			//int levelId = GameMap.GetInstance().GetSceneIndex(level)-1;
			//int levelId = System.Convert.ToInt16(Application.loadedLevelName.Substring(Application.loadedLevelName.Length-1));
			//relative story #
			int levelId = GameMap.GetInstance().GetLevel(index).level;
			
			
			profile.SaveLevelRecord(LevelRecord.LevelType.STORY,levelId,0,0);
			profile.Save();
		}

		GameObject view = GameObject.Find("Scrollview");
		ScrollRect scroll = view.GetComponent<ScrollRect>();
		scroll.verticalNormalizedPosition = 1f;

	}
}
