﻿using UnityEngine;
using System.Collections;

public class SurvivalUIControl : MonoBehaviour {

	public void ShowSurvivalUnlock()
	{
		GameObject.FindGameObjectWithTag("GameController").transform.position = new Vector3(16.83f,0f,0f);
		Camera.main.transform.position = new Vector3(16.83f,0f,-10f);

	}

	public void ReturnToSelect()
	{
		Camera.main.transform.position = new Vector3(0f,0f,-10f);
		GameObject.FindGameObjectWithTag("GameController").transform.position = new Vector3(0f,0f,0f);

	}



}
