﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

public class UpgradeTemplateController : MonoBehaviour {

	//public InstallButton install;

	public SpriteRenderer upgradeWeapon;
	public SpriteRenderer upgradeIcon;
	public Animator upgradeAnimator;
	public Text weaponType;
	public Text upgradeType;
	public Text upgradeDescription;

	public Sprite[] weapons;
	public Sprite[] icons;

	public string[] upgradeDescriptions;

	//encoded = weapon,category,amount;next upgrade
	public string[] encoded;

	private string multiDelimiter = ";";
	//1,2,1
	//0,2,4
	//1,2,1;1,0,1
	//0,2,8

	void Start()
	{
		GameObject.FindGameObjectWithTag("GameController").transform.position = GameConstants.OVERLAY_RESET_POSITION;
		int upgradeLevel = GameMap.GetInstance().GetThisLevel().level;

		string encodedStr = encoded[upgradeLevel-1];
		string[] encodedUpgrades = encodedStr.Split(new string[] { multiDelimiter }, System.StringSplitOptions.None);
		UpgradeInstance[] upgrades = new UpgradeInstance[encodedUpgrades.Length];
		StringBuilder boostDesc = new StringBuilder();
		string upgradeTitle = null;
		int count = 0;

		bool increaseSpawns= false;
		string encodedUpgrade;
		for(int i=0;i<encodedUpgrades.Length;i++)
		{
			encodedUpgrade = encodedUpgrades[i];

			char[] attribs = encodedUpgrade.ToCharArray();
			UpgradeInstance.UpgradeCategory category = (UpgradeInstance.UpgradeCategory) char.GetNumericValue(attribs[0]);
			UpgradeInstance.UpgradeBoost boost = (UpgradeInstance.UpgradeBoost) char.GetNumericValue(attribs[2]);
			int val = (int) char.GetNumericValue(attribs[4]);

			UpgradeInstance instance = new UpgradeInstance();
			instance.category = category;
			instance.boost = boost;
			instance.value = val;

			upgrades[count] = instance;

			if(count == 0)
			{

				if(upgradeLevel == 1 || upgradeLevel == 3 || upgradeLevel == 7)
					upgradeAnimator.SetInteger("upgradeRing", 1);
				else if(upgradeLevel == 2 || upgradeLevel == 5 || upgradeLevel == 6 || upgradeLevel == 9 )
					upgradeAnimator.SetInteger("upgradeRing", 2);
				else if(upgradeLevel == 4 || upgradeLevel == 8 || upgradeLevel == 11 || upgradeLevel == 12)
					upgradeAnimator.SetInteger("upgradeRing", 3);
				else if(upgradeLevel == 10 || upgradeLevel == 13 || upgradeLevel == 14 || upgradeLevel == 15 || upgradeLevel == 16)
					upgradeAnimator.SetInteger("upgradeRing", 4);
				//edited by tor 16 feb

				if(upgradeLevel == 3 || upgradeLevel == 6 || upgradeLevel == 11)
					increaseSpawns = true;

				upgradeTitle = category.ToString();
				upgradeWeapon.sprite = weapons[(int)category-1];
				upgradeIcon.sprite = icons[(int)category-1];
			}

			if(count > 0)
				boostDesc.Append(" & ");

			if(category != UpgradeInstance.UpgradeCategory.Barrier)
			{
				//override the default description of these upgrades
				if(boost == UpgradeInstance.UpgradeBoost.Reflection)
				{
					boostDesc.Append("Damage Radius");
				}
				else if(boost == UpgradeInstance.UpgradeBoost.Strength)
				{
					boostDesc.Append("Burns Longer");
				}
				else
				{
					boostDesc.Append(boost.ToString());
				}

			}
			else
			{
				boostDesc.Append(boost.ToString());
			}
			count++;
		}

		//install.upgradesToApply = upgrades;
		weaponType.text = upgradeTitle;
		upgradeType.text = boostDesc.ToString();
		upgradeDescription.text = upgradeDescriptions[upgradeLevel-1];

		int index = GameMap.GetInstance().GetCurrentIndex()-1;
		int levelId = GameMap.GetInstance().GetLevel(index).level;
		PlayerProfile profile = PlayerProfile.GetInstance();


		if(profile.HighestLevel <= index) //need to get the index of this level in the gamemap hierarchy
		{
			//GetUpgradeSettings from Scene
			Hashtable playerUpgrades = profile.WeaponUpgrades;
			
			PlayerUpgrade playerUpgrade;
			
			UpgradeInstance upgrade;
			for(int i=0;i<upgrades.Length;i++)
			{
				upgrade = upgrades[i];
		
				if(playerUpgrades[upgrade.category] != null)
				{
					playerUpgrade = (PlayerUpgrade) playerUpgrades[upgrade.category];
				}
				else
				{
					playerUpgrade = new PlayerUpgrade();
				}
				
				switch(upgrade.boost)
				{
				case UpgradeInstance.UpgradeBoost.Ammo:
					playerUpgrade.ammo += upgrade.value;
					break;
				case UpgradeInstance.UpgradeBoost.Power:
					playerUpgrade.power += upgrade.value;
					break;
				case UpgradeInstance.UpgradeBoost.Speed:
					playerUpgrade.speed += upgrade.value;
					break;
				case UpgradeInstance.UpgradeBoost.Strength:
					playerUpgrade.strength += upgrade.value;
					break;
				case UpgradeInstance.UpgradeBoost.Targeting:
					playerUpgrade.targeting += upgrade.value;
					break;
				case UpgradeInstance.UpgradeBoost.Reflection:
					playerUpgrade.reflection += upgrade.value;
					break;
				default: break;
				}
				
				playerUpgrade.active = 1;
				playerUpgrade.type = upgrade.category;
				
				////Debug.Log(playerUpgrade.type);
				////Debug.Log(playerUpgrade.ToString());
				
				
				if(playerUpgrades[upgrade.category] != null)
					playerUpgrades[upgrade.category] = playerUpgrade;
				else
					playerUpgrades.Add(upgrade.category,playerUpgrade);
			}
			
			profile.WeaponUpgrades = playerUpgrades;
			//profile.SaveUpgrades();
			if(increaseSpawns)
				profile.IncreaseMaxSpawns();
			
			
			if(profile.Points <= index)
			{
				profile.WeaponUpgradesSurvival = playerUpgrades;
				//profile.SaveUpgradesSurvival();
				if(increaseSpawns)
					profile.IncreaseMaxSpawnsSurvival();
			}
			
			
		}

		profile.SaveLevelRecord(LevelRecord.LevelType.UPGRADE,levelId,0,0);
		profile.Save();
		
	}

}
