﻿using UnityEngine;
using System.Collections;

public enum GameDifficulty  {

	EASY=0,
	NORMAL=1,
	HARD=2,
	INSANE=3

}
