﻿using UnityEngine;
using System.Collections;

public class FlexibleAnimator : SimpleAnimator {

	public Vector3[] path;
	public int startOn;

	private int pathCounter;

	void Awake()
	{

		enabled = true;
	}

	public void MoveNext()
	{
		pathCounter++;
		if(pathCounter >= path.Length)
		{
			target.transform.localPosition = path[0];
			currentPosition =  path[0];
			//base.Reset();
			pathCounter = 1;
		}

		animating = true;
		enabled = true;
		destination = path[pathCounter];
	}

	public void Init()
	{
		start = currentPosition = path[startOn];
		target.transform.localPosition = path[startOn];
		pathCounter = startOn;
	}

	public override void Reset ()
	{
		pathCounter = startOn;
		start = path[startOn];
		base.Reset ();
	} 




}
