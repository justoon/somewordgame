﻿using UnityEngine;
using System.Collections;

public class ConfirmReset : MonoBehaviour {

	public SimpleAnimator confirmDialog;
	public GameObject carousel;

	public void ShowConfirm()
	{
		AudioDirector.Instance.PlayGUIClip(0);
		confirmDialog.MoveToActive();
		carousel.SetActive(false);
	}

	public void Cancel()
	{
		AudioDirector.Instance.PlayGUIClip(1);
		confirmDialog.MoveToEnd();
		carousel.SetActive(true);
	}


}
