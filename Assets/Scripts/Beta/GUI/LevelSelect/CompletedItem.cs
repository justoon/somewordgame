﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CompletedItem : CurrentLevelItem {

	public SpriteRenderer flawlessBadge;
	public SpriteRenderer insaneBadge;
	public Text score;

}
