﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LevelSelectController : MonoBehaviour {

	public ScrollRect scrollController;

	public HorizontalLayoutGroup scrollContents;
	
	public GameObject chapter;
	public GameObject tutorial;
	public GameObject completed;
	public GameObject current;
	public GameObject upgrade;
	public Sprite bossSprite;
	public Sprite timerSprite;

	public Sprite[] chapters;
	public string[] chapterTexts;

	public Sprite[] weaponIcons;
	public Sprite[] weaponLevels;
	public string[] weaponFluffs;
	public string[] weaponDescriptions;

	public Image survivalIcon;
	public Button survivalButton;
	public Text survivalText;
	public Text survivalMode;
	public Text survivalScore;
	public Text storyScore;

	public Sprite lockImage;
	public GameObject spawnPoint;


	private float currentPosition;
	private float scrollFactor;

	private bool inPosition;
	private Color TURQUOISE = new Color(0.41960784f, 0.81568627f, 0.93333333f);
	private Color GRAY = new Color(0.16078431f, 0.16078431f, 0.16078431f);

	private WaitForSeconds spawnTime = new WaitForSeconds(2f);
	private bool spawn;


	void Start () 
	{
		GameController.GetInstance ().ResetDone();
		PlayerProfile profile = PlayerProfile.GetInstance();
		List<LevelRecord> playerScores = profile.ScoreMatrix;
		int maxLevel = profile.HighestLevel;


		if(profile.Points < 39)
		{
			LockSurvival();
		}
		else
		{
			survivalScore.text = profile.SurvivalScore.ToString("#,##");
			survivalScore.color =   TURQUOISE;
		}

		storyScore.text = profile.TotalScore.ToString("#,##");

		GameObject o = null;
		//Image container = null;
		//float padding = 0;

		for(int i=0;i<=maxLevel;i++)
		{
			if(i <= GameMap.GetInstance().GetMaxLevel())
			{
				LevelRecord lr;
				if(i == maxLevel)
					lr = GameMap.GetInstance().GetLevel(i);
				else
					lr = playerScores[i];


				switch(lr.type)
				{
					case LevelRecord.LevelType.STORY: 
						o = (GameObject) GameObject.Instantiate(chapter);
						StoryItem storyItem = o.GetComponentInChildren<StoryItem>();
						storyItem.level = i;
						storyItem.chapterImage.sprite = chapters[lr.level-1];
						storyItem.chapterNo.text = "CHAPTER " + lr.level;
						storyItem.chapterText.text = chapterTexts[lr.level-1];
						//container = storyItem.container;
						//padding = 1f;
					break;

				    case LevelRecord.LevelType.STANDARD:

						
						if(i == maxLevel)
						{
							o = (GameObject) GameObject.Instantiate(current);
							CurrentLevelItem currentLevel = o.GetComponentInChildren<CurrentLevelItem>();
						if(lr.level == 13 || lr.level == 19 || lr.level == 31 || lr.level == 37 || lr.level == 46)
								currentLevel.specialBadge.sprite = timerSprite;
							else
								currentLevel.levelText.text = lr.level.ToString();
							currentLevel.level = i;
							//container = currentLevel.container;
						}
						else if(i == 1)
						{
							o = (GameObject) GameObject.Instantiate(tutorial);
							LevelSelectItem selectItem = o.GetComponentInChildren<LevelSelectItem>();
							selectItem.level = i;
							//container = selectItem.container;
						}
						else
						{
							o = (GameObject) GameObject.Instantiate(completed);
							CompletedItem completedLevel = o.GetComponentInChildren<CompletedItem>();
						if(lr.level == 13 || lr.level == 19 || lr.level == 31 || lr.level == 37 || lr.level == 46)
								completedLevel.specialBadge.sprite = timerSprite;
							else
								completedLevel.levelText.text = lr.level.ToString();
							completedLevel.score.text = lr.score.ToString();
							completedLevel.level = i;
							if(lr.corrections > 1)
								completedLevel.insaneBadge.GetComponent<Renderer>().enabled = true;
							else
								completedLevel.insaneBadge.GetComponent<Renderer>().enabled = false;
							if(lr.corrections % 2 > 0)
								completedLevel.flawlessBadge.GetComponent<Renderer>().enabled = true; 
							else
								completedLevel.flawlessBadge.GetComponent<Renderer>().enabled = false; 
						//container = completedLevel.container;
						}
						//padding = .75f;


						
					break;



					case LevelRecord.LevelType.BOSS: 
						if(i == maxLevel)
						{
							o = (GameObject) GameObject.Instantiate(current);
							CurrentLevelItem currentLevel = o.GetComponentInChildren<CurrentLevelItem>();
							currentLevel.specialBadge.sprite = bossSprite;
							currentLevel.level = i;
							//container = currentLevel.container;
						}
						else
						{
							o = (GameObject) GameObject.Instantiate(completed);
							CompletedItem completedLevel = o.GetComponentInChildren<CompletedItem>();
							completedLevel.specialBadge.sprite = bossSprite;
							completedLevel.score.text = lr.score.ToString();
							completedLevel.level = i;
							if(lr.corrections > 1)
								completedLevel.insaneBadge.GetComponent<Renderer>().enabled = true;
							else
								completedLevel.insaneBadge.GetComponent<Renderer>().enabled = false;
							if(lr.corrections % 2 > 0)
								completedLevel.flawlessBadge.GetComponent<Renderer>().enabled = true; 
							else
								completedLevel.flawlessBadge.GetComponent<Renderer>().enabled = false; 
						//container = completedLevel.container;
						}
						//padding = .75f;
					break;

					case LevelRecord.LevelType.UPGRADE:

						o = (GameObject) GameObject.Instantiate(upgrade);
						UpgradeItem upgradeItem = o.GetComponentInChildren<UpgradeItem>();
						upgradeItem.level = i;
						//container = upgradeItem.container;
						//set the appropriate u[grade icon depending on the level
					if(lr.level == 1 ||lr.level == 2 || lr.level == 4 || lr.level == 10)
							upgradeItem.weaponIcon.sprite = weaponIcons[0]; //cannon
					else if(lr.level == 3 || lr.level == 5 || lr.level == 8 || lr.level == 13)
							upgradeItem.weaponIcon.sprite = weaponIcons[1]; //flamer
					else if(lr.level == 7 || lr.level == 9 || lr.level == 11 || lr.level == 14)
							upgradeItem.weaponIcon.sprite = weaponIcons[2]; //rocket
					else if(lr.level == 6 || lr.level == 12 || lr.level == 15)
							upgradeItem.weaponIcon.sprite = weaponIcons[3]; //barrier
					else if(lr.level == 16)
							upgradeItem.weaponIcon.sprite = weaponIcons[4]; //laser
						
						//set the number of upgrades earned on the level
					if(lr.level == 1 || lr.level == 3 || lr.level == 7)
							upgradeItem.upgradeLevel.sprite = weaponLevels[0];
					else if(lr.level == 2 || lr.level == 5 || lr.level == 9 || lr.level == 6 )
							upgradeItem.upgradeLevel.sprite = weaponLevels[1];
					else if(lr.level == 4 || lr.level == 8 || lr.level == 11 || lr.level == 12)
							upgradeItem.upgradeLevel.sprite = weaponLevels[2];
					else if(lr.level == 10 || lr.level == 13 || lr.level == 14 || lr.level == 15 || lr.level == 16)
							upgradeItem.upgradeLevel.sprite = weaponLevels[3];
						
						upgradeItem.fluff.text = weaponFluffs[lr.level-1];
						upgradeItem.description.text = weaponDescriptions[lr.level-1];
					//padding = .75f;

					break;

					default:  break;
				}


				((RectTransform)o.transform).SetParent(scrollContents.transform);





			}

			
		}

		scrollController.horizontalNormalizedPosition = 0;
		if(profile.CurrentLevel <= 2)
			currentPosition = 0;
		else
			currentPosition = ((float)profile.CurrentLevel/maxLevel)- 0.01298701f;

	
		if(currentPosition > 1)
			currentPosition = 1;


		//Debug.Log(string.Format("current level {0} max level {1} currentPos {2}", profile.CurrentLevel,maxLevel, currentPosition));

		spawn = true;
		//StartCoroutine(MarchofMoldies());

	}

	

	void Update()
	{
		if(!inPosition)
		{
			if(scrollController.horizontalNormalizedPosition >= currentPosition)
				inPosition = true;
			else
				scrollController.horizontalNormalizedPosition += Time.deltaTime;

		}
	}

	void OnDestroy()
	{
		spawn = false;
	}

	private void LockSurvival()
	{
		survivalScore.text = "Locked";
		survivalScore.color = TURQUOISE;

		//survivalIcon.sprite = lockImage;
		survivalButton.interactable = false;
		survivalText.color = GRAY;
		survivalMode.color = GRAY; //TURQUOISE;
	}


}
