﻿using UnityEngine;
using System.Collections;

public struct ScrambleLetter {

	public string letter;
	public bool used;
	public int index;



	public ScrambleLetter(string letter, bool used, int index)
	{
		this.letter = letter;
		this.used = used;
		this.index = index;
	}

}
