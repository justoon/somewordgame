﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OneClickButton : MonoBehaviour {

	private Button button;


	void Start()
	{
		button = GetComponent<Button>();
	}
	public virtual void DoAction()
	{
		//set it so we can only fire this button once per scene to prevent double clicks/taps
		button.interactable = false;
	}


}
