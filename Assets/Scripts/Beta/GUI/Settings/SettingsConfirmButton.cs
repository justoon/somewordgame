﻿using UnityEngine;
using System.Collections;

public class SettingsConfirmButton : SettingsCancelButton {

	public bool reloadLevel = true;

	public void SaveSettings()
	{
		AudioDirector.Instance.PlayGUIClip(0);
		PlayerProfile profile = PlayerProfile.GetInstance();
		profile.MusicOn = AudioDirector.Instance.musicOn = music.toggle;
		profile.SfxOn = AudioDirector.Instance.sfxOn = sound.toggle;
		profile.UsDictionary = spelling.toggle;
		profile.Difficulty = (short)difficulty.difficulty;
		profile.SavePreferences();


		if(spelling.Changed())
		{
			WordManager.GetInstance().ChangeDictionary();
		}
		if(music.Changed())
		{
			AudioDirector.Instance.On();	
		}

		//if we've changed difficulty levels, reset the level

		if(difficulty.Changed())
		{
			GameController.GetInstance().UpdateDifficultySettings((GameDifficulty)profile.Difficulty);

			//if we're still in the middle of the game we need to reload
			if(!Application.loadedLevelName.Equals("LevelSelect") &&  Application.loadedLevel != 1 && !GameController.GetInstance().IsDone())
			{
				AudioDirector.Instance.StopGameTracks();
				GUIManager.GetInstance().MoveTransition(GameConstants.OVERLAY_SETTING_POSITION);
				//unpause
				GameController.GetInstance().ResumeGame();
				GUIManager.GetInstance().LevelFadeOut();
			}
			else
			{
				base.CloseSettings();
			}
		}

		//if music changed and we're turning it on, start music
		else
		{
			base.CloseSettings();
		}

		music.UpdateReset();
		sound.UpdateReset();
		difficulty.UpdateReset();
		spelling.UpdateReset();


	}

}
