﻿using UnityEngine;
using System.Collections;

public class SpellingSettingToggle : MonoBehaviour {

	public SimpleAnimator animator;
	
	public bool toggle;
	private bool reset;
	
	
	void Awake()
	{
		reset = toggle = PlayerProfile.GetInstance().UsDictionary;
		if(!toggle)
			animator.SetToActive();
	}
	
	public void Toggle()
	{
		toggle = !toggle;
		if(!toggle)
			animator.MoveToActive();
		else
			animator.MoveToEnd();
		

	}
	
	public void Reset()
	{
		toggle = reset;
		if(!toggle)
			animator.SetToActive();
		else
			animator.Reset();

	}

	public bool Changed()
	{
		return toggle != reset;
	}

	public void UpdateReset()
	{
		reset = toggle;
	}
}
