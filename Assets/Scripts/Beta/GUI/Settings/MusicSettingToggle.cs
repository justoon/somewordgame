﻿using UnityEngine;
using System.Collections;

public class MusicSettingToggle : MonoBehaviour {

	public SimpleAnimator animator;

	public bool toggle;
	private bool reset;



	/*
	 * Music setting logic
	 * if on->off, stop music immediately
	 * if off->on, only start music when confirming action
	 * if on->on or off->off do nothing
	 */ 

	void Awake()
	{
		//legacy bug with settings, 0 is on, 1 is off
		reset = toggle = PlayerProfile.GetInstance().MusicOn;
		if(!toggle)
			animator.SetToActive();
	}

	public void Toggle()
	{
		toggle = !toggle;
		if(!toggle)
			animator.MoveToActive();
		else
			animator.MoveToEnd();


		//if we've switched the music off and it was on originally, immediately stop the playing track
		if(!toggle && AudioDirector.Instance != null && reset)
		{
			AudioDirector.Instance.Off();
			//flip the reset flag in case we want to turn the music back on
			reset = false;
		}
	}

	public void Reset()
	{
		toggle = reset;
		if(!toggle)
			animator.SetToActive();
		else
			animator.Reset();	
	}

	public void UpdateReset()
	{
		reset = toggle;
	}

	//only report back to the confirm button if we were off and are now on, we need to start the music
	public bool Changed() 
	{
		return !reset && toggle;
	}

}
