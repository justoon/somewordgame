﻿using UnityEngine;
using System.Collections;

public class SettingsCancelButton : MonoBehaviour {

	public MusicSettingToggle music;
	public SoundSettingToggle sound;
	public DifficultyToggle difficulty;
	public SpellingSettingToggle spelling;

	public Vector3 resetPosition = GameConstants.CAMERA_GAME_POSITION;

	public void CancelSettings()
	{
		AudioDirector.Instance.PlayGUIClip(1);
		CloseSettings();
		music.Reset();
		sound.Reset();
		difficulty.Reset();
		spelling.Reset();
	}

	public void CloseSettings()
	{
//		if(!Application.loadedLevelName.Equals(GameConstants.LEVEL_MAIN))
//			GUIManager.GetInstance().ShowLevelGUI();
//		if(Application.loadedLevelName.Contains("Boss"))
//			GUIManager.GetInstance().ShowBossGUI();
		if(GameController.GetInstance ().IsDone())
		{
			if(GUIManager.GetInstance().GetLevelOutcome() == 1)
				resetPosition = GameConstants.CAMERA_SUCCESS_POSITION;
			else if(GUIManager.GetInstance().GetLevelOutcome() == 2)
				resetPosition = GameConstants.CAMERA_FAIL_POSITION;
		}
		else
		{
			resetPosition = GameConstants.CAMERA_GAME_POSITION;
		}

		
		//Vector3 cameraPos = Camera.main.transform.position;
		Camera.main.transform.position = resetPosition;
		GameController.GetInstance().ResumeGame();
	}
}
