﻿using UnityEngine;
using System.Collections;

public class DifficultyToggle : MonoBehaviour {

	public FlexibleAnimator animator;

	public int difficulty;

	private int reset;

	void Start()
	{
		difficulty = reset = animator.startOn = PlayerProfile.GetInstance().Difficulty;
		animator.Init();

	}


	public void ToggleDifficulty()
	{
		animator.MoveNext();
		difficulty++;
		if(difficulty > 3)
			difficulty = 0;
	}

	public void Reset()
	{
		animator.startOn = difficulty = reset;
		animator.Reset();
	}

	public bool Changed()
	{
		return difficulty != reset;
	}
	public void UpdateReset()
	{
		animator.startOn = reset = difficulty;
	}
}
