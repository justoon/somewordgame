﻿using UnityEngine;
using System.Collections;

public class SoundSettingToggle : MonoBehaviour {

	public SimpleAnimator animator;
	
	public bool toggle;
	private bool reset;
	
	
	void Awake()
	{
		reset = toggle = PlayerProfile.GetInstance().SfxOn;
		if(!toggle)
			animator.SetToActive();
	}
	
	public void Toggle()
	{
		toggle = !toggle;
		if(!toggle)
			animator.MoveToActive();
		else
			animator.MoveToEnd();
		
		if(!toggle && AudioDirector.Instance != null)
			AudioDirector.Instance.SfxOff();
		else if(AudioDirector.Instance != null)
			AudioDirector.Instance.SfxOn();
	}
	
	public void Reset()
	{
		toggle = reset;
		if(!toggle)
			animator.SetToActive();
		else
			animator.Reset();
		if(!toggle && AudioDirector.Instance != null)
			AudioDirector.Instance.SfxOff();
		else if(AudioDirector.Instance != null)
			AudioDirector.Instance.SfxOn();
	}

	public void UpdateReset()
	{
		reset = toggle;
	}
}
