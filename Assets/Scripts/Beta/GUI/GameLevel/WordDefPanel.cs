﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class WordDefPanel : MonoBehaviour {

	public static WordDefPanel Instance;

	public Text display;

	public static WordDefPanel GetInstance() {
		return Instance;
	}

	void Awake() {
		if(Instance == null) {
			Instance = this;
		}
	}
	// Use this for initialization
	void Start () {
		this.gameObject.SetActive(false);
	}

	public void Show() {
		this.gameObject.SetActive(true);
	}

	public void Close() {
		this.gameObject.SetActive(false);
	}

	public void SetText(string text) {
		display.text = text;
	}


}
