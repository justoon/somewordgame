﻿using UnityEngine;
using System.Collections;

public class ReturnToSpawn : MonoBehaviour {

	public Transform spawnPoint;

	public virtual void OnTriggerEnter2D(Collider2D hit)
	{
		GameObject moldie = hit.gameObject;
		moldie.transform.position = spawnPoint.position;
	}

}
