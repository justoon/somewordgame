﻿using UnityEngine;
using System.Collections;

public class QuitLevel : AdEnabledButton {

	public void DoQuitLevel()
	{
		base.DoAction();
	}

	protected override void DoLoadingAction ()
	{
		//Debug.Log("stopping game track");
		AudioDirector.Instance.StopGameTracks();
		//Debug.Log("getting main track");
		AudioDirector.Instance.SetTrackToMain();
		//Debug.Log("resuming game");
		GameController.GetInstance().ResumeGame();
		GameObject foamBombController = GameObject.FindGameObjectWithTag("ComboIndicator");
		
		
		
		GUIManager.GetInstance().ResetOverlayPosition();
		AudioDirector.Instance.StopGameTracks();
		AudioDirector.Instance.SetTrackToMain();
		GameController.GetInstance().LoadLevel("LevelSelect");
	}

	public void Enable()
	{
		this.gameObject.SetActive(true);
	}

	public void Disable()
	{
		this.gameObject.SetActive(false);
	}
}
