﻿using UnityEngine;
using System.Collections;

public class ResetLevel : OneClickButton {

	public override void DoAction ()
	{
		base.DoAction();


		GameObject foamBombController = GameObject.FindGameObjectWithTag("ComboIndicator");
		
		GUIManager.GetInstance().ResetOverlayPosition();
		GameController.GetInstance().ResumeGame();
		AudioDirector.Instance.StopGameTracks();
		PlayerProfile player = PlayerProfile.GetInstance();
		player.CurrentLevel = GameMap.GetInstance().GetThisLevel().index;
		GUIManager.GetInstance().LevelFadeOut();
	}
	
	public void Enable()
	{
		this.gameObject.SetActive(true);
	}
	
	public void Disable()
	{
		this.gameObject.SetActive(false);
	}

}

