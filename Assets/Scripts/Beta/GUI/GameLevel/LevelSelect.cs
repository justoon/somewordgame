﻿using UnityEngine;
using System.Collections;

public class LevelSelect : MonoBehaviour {

	public  void LoadLevelSelect ()
	{

		//base.DoAction();
		GUIManager.GetInstance().ResetOverlayPosition();
		AudioDirector.Instance.StopGameTracks();
		AudioDirector.Instance.SetTrackToMain();
		GameController.GetInstance().LoadLevel("LevelSelect");
	}

}
