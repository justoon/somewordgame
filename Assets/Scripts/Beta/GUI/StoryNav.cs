﻿using UnityEngine;
using System.Collections;

public class StoryNav : MonoBehaviour {

	public void ReadStory()
	{
		Camera.main.transform.position = GameConstants.CAMERA_SUCCESS_POSITION;
	}

	public void BackToTitle()
	{
		Camera.main.transform.position = GameConstants.CAMERA_GAME_POSITION;
	}

}
