﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class bGUIManager : MonoBehaviour {

	public GameObject failOverlay;


	// Use this for initialization
	void Start () {

		TurnOffRenderers(failOverlay);
		TurnOffUIText(failOverlay);
	


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TurnOnRenderers(GameObject x)
	{
		Renderer[] rs = x.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in rs)
			r.enabled = true;
		
	}
	
	public void TurnnUIText(GameObject x)
	{
		CanvasGroup[] cgs = x.GetComponentsInChildren<CanvasGroup>();
		foreach(CanvasGroup cg in cgs)
			cg.alpha = 1;
	}

	public void TurnOffRenderers(GameObject x)
	{
		Renderer[] rs = x.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in rs)
			r.enabled = false;

	}

	public void TurnOffUIText(GameObject x)
	{
		CanvasGroup[] cgs = x.GetComponentsInChildren<CanvasGroup>();
		foreach(CanvasGroup cg in cgs)
			cg.alpha = 0;
	}
}
