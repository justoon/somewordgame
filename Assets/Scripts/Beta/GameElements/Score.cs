﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class Score : MonoBehaviour  {

	public AudioSource sfxPlayer;
	public AudioClip chainAdd;
	public AudioClip chainBreak;

	public static int score;
	public Animator scoreAnimator;
	public Animator chainAnimator;
	public Animator flawlessAnimator;
	public Text chainDisplay;
	public Text correctionDisplay;
	public Text totalScore;
	public Text wordsRemaining;
	public GameLevel level;
	public bool showRemaining;

	private float timeTaken;
	
	//private float accuracy;
	private short longestChain;
	private short currentChain;
	private short numWords;
	
	
	private short corrections;
	
	private uint[] enemiesKilled;
	

	private GUIManager guiManager;
	
	
	void Start()
	{
		//Debug.Log("ScoreITeration2 start");
		
		enemiesKilled = new uint[5];
		
		//accuracy = 100f;
		longestChain = currentChain = numWords = corrections = 0;
		guiManager = GUIManager.GetInstance();


		if(GameMap.GetInstance().currentLevel.Equals(GameConstants.LEVEL_SURVIVAL1))
			corrections = 4;

		GUIEventManager.OnClicked += ExecuteGUIEvent;
		GameEventManager.OnEvent += ExecuteGameEvent;
	}
	
	void OnDestroy()
	{
		/////Debug.Log("ScoreITeration2 Destroy");
		GUIEventManager.OnClicked -= ExecuteGUIEvent;
		GameEventManager.OnEvent -= ExecuteGameEvent;
	}
	
	private void ExecuteGUIEvent(GameEvent gameEvent)
	{
		//string eventName = gameEvent.GetName();
		MoldicideEvent eventType = gameEvent.GetName();
		
		if(eventType == MoldicideEvent.GUESS_CORRECTION || eventType == MoldicideEvent.CANCEL_WORD)
		{

			//guiManager.Mistake();
			corrections++;
			if(corrections < 5)
				flawlessAnimator.SetInteger("numCorrections", corrections);
			if(corrections > 3 && currentChain > 1)
			{
				if(AudioDirector.Instance.sfxOn)
				{
					sfxPlayer.clip = chainBreak;
					sfxPlayer.Play();
				}

					
				if(!chainAnimator.GetCurrentAnimatorStateInfo(0).IsName("ui_broken_alerts"))
					chainAnimator.SetTrigger(GUIManager.GetInstance()._triggerChain);
			}
			else if( corrections < 4)
			{
				if(AudioDirector.Instance.sfxOn)
				{
					sfxPlayer.clip = chainBreak;
					sfxPlayer.Play();
				}
				correctionDisplay.text = corrections.ToString();
				chainAnimator.SetTrigger(GUIManager.GetInstance()._triggerCorrection);
//				if(!chainAnimator.GetCurrentAnimatorStateInfo(0).IsName("ui_correction_alerts"))
//				{
//
//				}
			}
			currentChain = 0;
			//guiManager.AnimateScore("0");
		}
		else if(eventType == MoldicideEvent.WEAPON_SPAWN)
		{
			
			WeaponClickAction weaponAction = (WeaponClickAction)gameEvent.GetAction();
			
			
			
			numWords++;
			currentChain++;
			if(currentChain > longestChain)
				longestChain = currentChain;
			
			//if(currentChain > 1)

			int baseScore = GetBaseScore(weaponAction.wordLength, weaponAction.fullLength);
			
			int score = baseScore * currentChain;

			//guiManager.AnimateScore(currentChain.ToString());
			if(showRemaining)
			{
				int wordsRemain = level.GetWordsRemaining()-1;
				if(wordsRemain > 0)
				{
					if(AudioDirector.Instance.sfxOn)
					{
						sfxPlayer.clip = chainAdd;
						sfxPlayer.Play();
					}
					wordsRemaining.text = wordsRemain.ToString();
					chainAnimator.SetTrigger(GUIManager.GetInstance()._triggerChallenge);
				}
			}
			else
			{
				if(currentChain > 1)
				{
					if(AudioDirector.Instance.sfxOn)
					{
						sfxPlayer.clip = chainAdd;
						sfxPlayer.Play();
					}
					chainDisplay.text = currentChain.ToString();
					chainAnimator.SetTrigger(GUIManager.GetInstance()._triggerScore);
				}
			}

			

			Add(score);
			ShowScore(Score.score);


			//scoreDisplay.text = score.ToString();
//			GameEvent scoreChange = new GameEvent(MoldicideEvent.SCORE,score,0,null);
//			
//			GameEventManager.DispatchEvent(scoreChange);


			

		}

		
		
		
		
	}
	
	private void ExecuteGameEvent(GameEvent gameEvent)
	{
		MoldicideEvent eventType = gameEvent.GetName();
		
		if(eventType == MoldicideEvent.GAME_OVER)
		{
			currentChain = 0;
			timeTaken = 0;
			chainDisplay.text = "0";
			//guiManager.AnimateScore("0");
			
		}
		else if(eventType == MoldicideEvent.CONFIRM_KILL)
		{
			enemiesKilled[gameEvent.GetValue()]++;
			//Debug.Log("kills: " + enemiesKilled[gameEvent.GetValue()]);
		}
		else if(eventType == MoldicideEvent.LEVEL_COMPLETE)
		{
			chainAnimator.SetTrigger(GUIManager.GetInstance()._triggerLevelComplete);
		}



//		else if(gameEvent.GetName() == MoldicideEvent.SCORE)
//		{
//
//		}
	}
	
	public void ResetTracker()
	{
		System.Array.Clear(enemiesKilled,0,5);
	}
	
	public int GetBaseScore(int wordLength, bool fullLength)
	{
		
		
		int calculatedScore = GameController.baseScore;
		
		if(wordLength > 3)
			calculatedScore = (int) (GameController.baseMultiplier *  Mathf.Pow(2, wordLength-GameController.factor));
		
		if(fullLength && wordLength > 4 && wordLength < 9)
			calculatedScore += calculatedScore/2;
		
		return calculatedScore;
	}
	
	
	public void ShowScore(int score)
	{

		totalScore.text = score.ToString();
		scoreAnimator.SetTrigger(GUIManager.GetInstance()._triggerUpdateScore);
	}

	
	public short Corrections {
		get {
			return corrections;
		}
	}
	
	public short LongestChain {
		get {
			return longestChain;
		}
	}
	
	public uint[] EnemiesKilled {
		get {
			return enemiesKilled;
		}
	}
	
	public short NumWords {
		get {
			return numWords;
		}
	}
	
	
	public float TimeTaken {
		get {
			return timeTaken;
		}
		set {
			timeTaken = value;
		}
	}
	




	public void Add(int score)
	{
		Score.score += score;
	}

	public void Reset()
	{
		Score.score = 0;
	}

	public int GetScore()
	{
		return Score.score;
	}





}
