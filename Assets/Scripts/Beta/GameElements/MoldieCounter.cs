﻿using UnityEngine;
using System.Collections;

public class MoldieCounter : MonoBehaviour {

	public SpriteRenderer counterOverlay;
	private int totalMoldies = -1;
	private float scalingFactor;


	public void RescaleCounter(int newCount)
	{
		totalMoldies = newCount;
		Init(newCount);
	}

	public void UpdateCounter(int newCount)
	{
		if(totalMoldies == -1)
			Init(newCount);
		else 
			counterOverlay.transform.localScale = new Vector2(1f, counterOverlay.transform.localScale.y-scalingFactor);

	}
	
	private void Init(int newCount)
	{
		totalMoldies = newCount;
		scalingFactor = 1f / totalMoldies;
		counterOverlay.transform.localScale = new Vector2(1f,1f);
	}



}
