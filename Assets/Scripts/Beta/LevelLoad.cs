﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelLoad : MonoBehaviour {

	public Animator loadingAnimator;
	public Camera transitionCamera;
	public SpriteRenderer difficultyIcon; 
	public Sprite[] difficultyIcons;
	public Text loadText;
	public SpriteRenderer loadSprite;
	public SpriteRenderer background;

	// Use this for initialization
	void Start () {
		GUIManager.GetInstance().LockGUI();
		difficultyIcon.sprite = difficultyIcons[PlayerProfile.GetInstance().Difficulty];
		Application.LoadLevelAdditive(GameMap.GetInstance().currentLevel);
		AdManager.OnLoadComplete += LoadCompleted;
		//loadingAnimator.SetBool("loading", true);
	}

	void OnDestroy()
	{
		AdManager.OnLoadComplete -= LoadCompleted;
	}

	private void LoadCompleted()
	{
		Debug.Log("Ad for level is ready");
	}

	// Update is called once per frame
	void Update () {
		if(!Application.isLoadingLevel)
		{
			//loadingAnimator.gameObject.SetActive(false);
			GUIManager.GetInstance().ResetOverlayPosition();
			GameController.GetInstance().ResetDone();
			//loadingAnimator.enabled = false;
			transitionCamera.enabled = false;
			loadText.enabled = false;
			difficultyIcon.enabled = false;
			loadSprite.enabled = false;
			background.enabled = false;
			enabled = false;
			string loadedLevel = GameMap.GetInstance().currentLevel;
			//only call to the GUIManager to load all the references to GameObjects for a gameLevel, ignore upgrades and story levels
			if(loadedLevel != null && (loadedLevel.Contains("Level") && !loadedLevel.Equals("LevelSelect")) || loadedLevel.Contains("Boss") || loadedLevel.Contains("Survival") || loadedLevel.Contains("Tutorial"))
			{
				GUIManager.GetInstance().LoadGameLevel();
				if(GameController.Instance.useAds)
				{
					AdManager.Instance.FetchVideoAd();
				}
			}
			else
			{
				GUIManager.GetInstance().LevelLoaded();
			}
		}

	}
}
