﻿using UnityEngine;
using System.Collections;

public class TextManager 
{
	private static TextManager instance;

	private string[] ernesto;
	private string[] successTips;
	private string[] failureTips;


	private TextManager()
	{
		TextAsset ernestoWords = Resources.Load("ernestoWords") as TextAsset;
		string text = ernestoWords.text;
		this.ernesto = text.Split(new string[] { GameConstants.DELIMITER }, System.StringSplitOptions.None);
		Resources.UnloadAsset(ernestoWords);


		TextAsset successWords = Resources.Load("successTips") as TextAsset;
		text = successWords.text;
		this.successTips = text.Split(new string[] { GameConstants.DELIMITER }, System.StringSplitOptions.None);
		Resources.UnloadAsset(successWords);

		TextAsset failureWords = Resources.Load("failureTips") as TextAsset;
		text = failureWords.text;
		this.failureTips = text.Split(new string[] { GameConstants.DELIMITER }, System.StringSplitOptions.None);
		Resources.UnloadAsset(failureWords);
	}
	
	public static TextManager GetInstance()
	{
		if(instance == null)
		{
			instance = new TextManager();
		}
		return instance;
	}

	public string GetErnestoWord()
	{
		int r = Random.Range(0, ernesto.Length);
		return ernesto[r];
	}

	public string GetSuccessTip()
	{
		int r = Random.Range(0, successTips.Length);
		return successTips[r];
	}

	public string GetFailTip()
	{
		int r = Random.Range(0, failureTips.Length);
		return failureTips[r];
	}




}
