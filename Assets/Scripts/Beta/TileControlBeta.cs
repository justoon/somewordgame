﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TileControlBeta : MonoBehaviour, IBeginDragHandler, IDragHandler, IDropHandler, IPointerDownHandler, IPointerClickHandler {


	private float offsetX;
	private float offsetY;

	private bool inSelection = true;
	private bool ignoreClickUp = false;

	public void OnBeginDrag (PointerEventData eventData)
	{

		RectTransform canvasRT = transform.parent.GetComponent<RectTransform>();
		RectTransform rt = GetComponent<RectTransform>();
		Vector3 globalMousePos;
		RectTransformUtility.ScreenPointToWorldPointInRectangle(canvasRT,eventData.position, eventData.pressEventCamera, out globalMousePos);

		offsetX = globalMousePos.x - rt.position.x;
		offsetY = globalMousePos.y - rt.position.y;

	}

	public void OnDrag (PointerEventData eventData)
	{
		RectTransform rt = GetComponent<RectTransform>();
		RectTransform canvasRT = transform.parent.GetComponent<RectTransform>();
		Vector3 globalMousePos;
		if (RectTransformUtility.ScreenPointToWorldPointInRectangle(canvasRT,eventData.position, eventData.pressEventCamera, out globalMousePos)) 
		{
			rt.position = new Vector2(globalMousePos.x - offsetX, globalMousePos.y - offsetY);
			//rt.rotation = m_DraggingPlane.rotation;
		}
	}

	public void OnDrop (PointerEventData eventData)
	{

		Debug.Log("dropped");
	}

	public void OnPointerDown (PointerEventData eventData)
	{

	}

	public void OnPointerClick (PointerEventData eventData)
	{
		if(!inSelection && !ignoreClickUp)
		{
			inSelection = true;
			Debug.Log("will move to selection");
		}
		ignoreClickUp = false;
	}

	void Update()
	{

	}


}
