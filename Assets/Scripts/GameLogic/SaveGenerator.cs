﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class SaveGenerator : MonoBehaviour {

	List<LevelRecord> gameLevels;
	public Text inputField;

	void Awake()
	{
		LoadGameMapData();
	}

	public void CreateSave()
	{
		int level = int.Parse(inputField.text);
		GenerateSave(level);
	}

	public void GenerateSave(int maxLevel)
	{
		
	int counter = 0;
	int maxSpawns = 3;
	int maxSpawnsSurvival = 6;
	//calculate the number of game map elements to copy
	foreach(LevelRecord record  in gameLevels)
	{
			if(record.type == LevelRecord.LevelType.STANDARD || record.type == LevelRecord.LevelType.BOSS)
		{
			if(record.level == maxLevel)
			{
				counter++;
				break;
			}
		}
			else if(record.type == LevelRecord.LevelType.UPGRADE)
		{
			if(record.level == 3 || record.level == 6 || record.level == 11)
			{
				maxSpawns++;
				maxSpawnsSurvival++;
			}
		}
		counter++;
	}
	
		StringBuilder encoded = new StringBuilder();
	
	for(int i=0;i<counter;i++)
	{
		LevelRecord levelRecord = gameLevels[i];
		encoded.Append(levelRecord.ToString());
		encoded.Append("|");
		
	}
		encoded.Remove(encoded.Length-1,1);
	
	int currentLevel = counter;
	int highestLevel = counter;
	int points = counter;
	
	string[] upgrades = GetEncodedUpgrades(maxLevel);
	
		StringBuilder saveOut = new StringBuilder();
	saveOut.Append(currentLevel)
		.Append("#")
			.Append(points)
			.Append("#")
			.Append(highestLevel)
			.Append("#")
			.Append(0)
			.Append("#")
			.Append(0)
			.Append("#")
			.Append(maxSpawns)
			.Append("#")
			.Append(maxSpawnsSurvival)
			.Append("#")
			.Append(encoded)
			.Append("#")
			.Append(upgrades[0])
			.Append("#")
			.Append(upgrades[1])
			.Append("#")
			.Append(0)
			.Append("#");
	
	
	
	string path = "d:/temp/moldicide";
	
		int d = 67854;
		StreamWriter fout = new StreamWriter(path);
		StringBuilder inSb = new StringBuilder(saveOut.ToString());
		StringBuilder outSb = new StringBuilder(saveOut.Length);
		char c;
		for (int i = 0; i < saveOut.Length; i++)
		{
			c = inSb[i];
			c = (char)(c ^ d);
			outSb.Append(c);
		}
		
		fout.Write(outSb.ToString());
		fout.Close();
	
	
	
}



public string[] GetEncodedUpgrades(int maxLevel)
{
		string[] upgrades = new string[2];
	
	if(maxLevel >= 4 && maxLevel < 7)
	{
		upgrades[0] = "[10001001";
		upgrades[1] = "[10001001";
	}
	else if(maxLevel >= 7 && maxLevel < 10)
	{
		upgrades[0] = "[11001001";
		upgrades[1] = "[11001001";
	}
	else if(maxLevel >= 10 && maxLevel < 13)
	{
		upgrades[0] = "[11001001[10001002";
		upgrades[1] = "[11001001[10001002";
	}
	else if(maxLevel >= 13 && maxLevel < 16)
	{
		upgrades[0] = "[21101001[10001002";
		upgrades[1] = "[21101001[10001002";
	}
	
	else if(maxLevel >= 16 && maxLevel < 19)
	{
		upgrades[0] = "[21101001[11001112";
		upgrades[1] = "[21101001[11001112";
	}
	else if(maxLevel >= 19 && maxLevel < 22)
	{
		upgrades[0] = "[22111001[11001112";
		upgrades[1] = "[22111001[11001112";
	}
	else if(maxLevel >= 22 && maxLevel < 25)
	{
		upgrades[0] = "[22111001[21101112";
		upgrades[1] = "[22111001[21101112";
	}
	else if(maxLevel >= 25 && maxLevel < 28)
	{
		upgrades[0] = "[22111001[21101112[10001003";
		upgrades[1] = "[22111001[21101112[10001003";
	}
	else if(maxLevel >= 28 && maxLevel < 31)
	{
		upgrades[0] = "[22111001[21101112[10001003[00001105";
		upgrades[1] = "[22111001[21101112[10001003[00001105";
	}
	else if(maxLevel >= 31 && maxLevel < 34)
	{
		upgrades[0] = "[22111001[21101112[11001013[00001105";
		upgrades[1] = "[22111001[21101112[11001013[00001105";
	}
	else if(maxLevel >= 34 && maxLevel < 37)
	{
		upgrades[0] = "[22111001[22101232[11001013[00001105";
		upgrades[1] = "[22111001[22101232[11001013[00001105";
	}
	else if(maxLevel >= 37 && maxLevel < 40)
	{
		upgrades[0] = "[22111001[22101232[11001013[00001205";
		upgrades[1] = "[22111001[22101232[11001013[00001205";
	}
	else if(maxLevel >= 40 && maxLevel < 43)
	{
		upgrades[0] = "[22111001[22101232[21001013[00001205";
		upgrades[1] = "[22111001[22101232[21001013[00001205";
	}
	else if(maxLevel >= 43 && maxLevel < 46)
	{
		upgrades[0] = "[22111001[22101232[22101033[00001205";
		upgrades[1] = "[22111001[22101232[22101033[00001205";
	}
	else if(maxLevel >= 46 && maxLevel < 49)
	{
		upgrades[0] = "[22111001[22101232[22101033[00001215";
		upgrades[1] = "[22111001[22101232[22101033[00001215";
	}
	else if(maxLevel > 49)
	{
		upgrades[0] = "[22111001[22101232[22101033[10000004[00001215";
		upgrades[1] = "[22111001[22101232[22101033[10000004[00001215";
	}
	
	return upgrades;
	
}

public void LoadGameMapData()
{
	gameLevels = new List<LevelRecord>();
	
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,1,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(1,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(2,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(3,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,1,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(4,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,2,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(5,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(6,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,2,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(7,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(8,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(9,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,3,0,0,gameLevels.Count)); //Add spawn
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,3,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(10,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(11,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(12,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,4,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(13,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(14,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,4,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(15,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,5,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(16,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(17,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(18,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,6,0,0,gameLevels.Count)); //Add spawn
	gameLevels.Add(new LevelRecord(19,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,5,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(20,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(21,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,7,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(22,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(23,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(24,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,8,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,6,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.BOSS,25,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(26,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(27,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,9,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(28,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(29,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,7,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(30,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,10,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(31,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(32,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(33,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,11,0,0,gameLevels.Count)); //Add spawn
	gameLevels.Add(new LevelRecord(34,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,8,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(35,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(36,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,12,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(37,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(38,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(39,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,13,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,9,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.BOSS,40,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(41,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(42,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,14,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(43,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(44,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,10,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(45,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,15,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(46,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(47,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(48,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.UPGRADE,16,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(49,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,11,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.BOSS,50,0,0,gameLevels.Count));
	gameLevels.Add(new LevelRecord(LevelRecord.LevelType.STORY,12,0,0,gameLevels.Count));
}
}
