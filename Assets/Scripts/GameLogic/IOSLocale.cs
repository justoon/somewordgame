﻿
using System.Collections;
using System.Runtime.InteropServices;

public class IOSLocale  {

	[DllImport("__Internal")]
	public static extern bool _GetLocale();

	public static bool GetLocale()
	{
		return _GetLocale ();
	}
}
