﻿using UnityEngine;
using System.Collections;

public class AskPrompt : MonoBehaviour {

	public void Rate()
	{
		#if UNITY_ANDROID
		if(LoadLeaderboardAction.GOOGLEPLAY)
			Application.OpenURL("market://details?id=com.sprouted.moldicide");
		else
			Application.OpenURL("amzn://apps/android?p=com.sprouted.moldicide");
		#else
		Application.OpenURL("https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=987151107&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8");
		#endif
		PlayerProfile.GetInstance().SetAsk();
		LoadNext();
	}

	public void Never()
	{
		PlayerProfile.GetInstance().SetAsk();
		LoadNext();
	}

	public void NotNow()
	{
		LoadNext();
	}

	private void LoadNext()
	{
		//GameController.GetInstance().LoadNextStoryLevel();
	}

}
