﻿using UnityEngine;
using System.Collections;
//using com.amazon.mas.cpt.ads;

public class AdManager : MonoBehaviour {


	public delegate void LoadingCallback();
	public static event LoadingCallback OnLoadComplete;

	public delegate void CompleteCallback();
	public static event CompleteCallback OnAdComplete;

	public bool interstitialAdReady;
	public bool videoAdReady;

	public bool testing;

	//private IAmazonMobileAds mobileAds;

	private static AdManager instance = null;
	public static AdManager Instance {
		get { return instance; }
	}

	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
			// Obtain object used to interact with the plugin
			//mobileAds = AmazonMobileAdsImpl.Instance;
		}
		DontDestroyOnLoad(this.gameObject);
	}


	// Use this for initialization
	public void InitAdStream () {


		
//		// Construct object passed to sync operation as input
//		ApplicationKey key = new ApplicationKey();
//		// Construct object passed to sync operation as input
//		ShouldEnable enable = new ShouldEnable();
//
//		
//		// Set input value
//		key.StringValue = "2b218ba9a4ad4aa982b88875ecbbd330";
//		
//		// Call method, passing in required input structure
//		// This method does not return a response
//		mobileAds.SetApplicationKey(key);
//
//
//		// Set input value
//		enable.BooleanValue = testing;
//		
//		// Call method, passing in required input structure
//		// This method does not return a response
//		mobileAds.EnableTesting(enable);
//		mobileAds.EnableLogging(enable);
//		
//		// Register for an event
//		mobileAds.AddAdLoadedListener(VideoAdReady);
//		mobileAds.AddAdDismissedListener(VideoAdComplete);
//
//
//		//mobileAds.CreateInterstitialAd();
//		Ad interstialAd = mobileAds.CreateInterstitialAd();
//		string adType = interstialAd.AdType.ToString();
//		long identifier = interstialAd.Identifier;
//		Debug.Log (identifier + "     " + adType);


		/*HeyzapAds.start("a2e21ac93b7341fa439156e03859afc7", HeyzapAds.FLAG_DISABLE_AUTOMATIC_FETCHING);
		HZInterstitialAd.AdDisplayListener listener = delegate(string adState, string adTag){
						if ( adState.Equals("show") ) {
							// Do something when the ad shows, like pause your game
							interstitialAdReady = false;
						}
						if ( adState.Equals("hide") ) {
							// Do something after the ad hides itself
							InterstitialAdComplete();
						}
						if ( adState.Equals("click") ) {
							// Do something when an ad is clicked on
							Debug.LogError("Ad was clicked");
						}
						if ( adState.Equals("failed") ) {
							// Do something when an ad fails to show
							Debug.LogError("Ad Failed");
						}
						if ( adState.Equals("available") ) {
							// Do something when an ad has successfully been fetched
							InterstitialAdReady();
						}
						if ( adState.Equals("fetch_failed") ) {
							// Do something when an ad did not fetch
							Debug.LogError("Failed to load ad");
						}
						if ( adState.Equals("audio_starting") ) {
							// The ad being shown will use audio. Mute any background music
							AudioDirector.Instance.Off();
						}
						if ( adState.Equals("audio_finished") ) {
							// The ad being shown has finished using audio.
							// You can resume any background music.
							if(PlayerProfile.GetInstance().MusicOn)
								AudioDirector.Instance.On();
						}
					};
					
					HZInterstitialAd.setDisplayListener(listener);
		HZVideoAd.AdDisplayListener vlistener = delegate(string adState, string adTag){
			if ( adState.Equals("show") ) {
				// Do something when the ad shows, like pause your game
				videoAdReady = false;
			}
			if ( adState.Equals("hide") ) {
				// Do something after the ad hides itself
				VideoAdComplete();
			}
			if ( adState.Equals("click") ) {
				// Do something when an ad is clicked on
				Debug.LogError("Ad was clicked");
			}
			if ( adState.Equals("failed") ) {
				// Do something when an ad fails to show
				Debug.LogError("Ad Failed");
			}
			if ( adState.Equals("available") ) {
				// Do something when an ad has successfully been fetched
				VideoAdReady();
			}
			if ( adState.Equals("fetch_failed") ) {
				// Do something when an ad did not fetch
				Debug.LogError("Failed to load ad");
			}
			if ( adState.Equals("audio_starting") ) {
				// The ad being shown will use audio. Mute any background music
				AudioDirector.Instance.Off();
			}
			if ( adState.Equals("audio_finished") ) {
				// The ad being shown has finished using audio.
				// You can resume any background music.
				if(PlayerProfile.GetInstance().MusicOn)
					AudioDirector.Instance.On();
			}
		};
		
		HZVideoAd.setDisplayListener(vlistener);*/
	}

	public void FetchInterstitialAd()
	{
		//HZInterstitialAd.fetch();
	}

	public void FetchVideoAd()
	{
		
		Debug.Log("Fetching video ad");

		//HZVideoAd.fetch();

//		LoadingStarted response = mobileAds.LoadInterstitialAd();
//		
//		// Get return value
//		bool loadingStarted = response.BooleanValue;
//
//		Debug.Log (loadingStarted);
	}

	public void ShowInterstitialAd()
	{
//		if(HZInterstitialAd.isAvailable())
//		{
//			HZInterstitialAd.show();
//		}
//		else
//		{
//			InterstitialAdComplete();
//		}
//
//		interstitialAdReady = false;

	}
	public void ShowVideoAd()
	{


//		if(HZVideoAd.isAvailable())
//		{
//			HZVideoAd.show();
//		}
//		else
//		{
//			VideoAdComplete();
//		}
//		videoAdReady = false;

//		else if(HZInterstitialAd.isAvailable())
//		{
//			HZInterstitialAd.show();
//		}
//		AdShown response = mobileAds.ShowInterstitialAd();
//		
//		// Get return value
//		bool adShown = response.BooleanValue;
//		if(adShown)
//			adReady = false;


	}

	private void InterstitialAdComplete()
	{
		if(OnAdComplete != null)
		{
			Debug.Log("Ad completed, callingback");
			OnAdComplete();
		}
	}
	
	private void InterstitialAdReady()
	{
		interstitialAdReady = true;
		if(OnLoadComplete != null)
		{
			Debug.Log("Ad loaded, callingback");
			OnLoadComplete();
			
		}
	}


	private void VideoAdComplete()
	{
		if(OnAdComplete != null)
		{
			Debug.Log("Ad completed, callingback");
			OnAdComplete();
		}
	}
	
	private void VideoAdReady()
	{
		videoAdReady = true;
		if(OnLoadComplete != null)
		{
			Debug.Log("Ad loaded, callingback");
			OnLoadComplete();

		}
	}
}
